package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.dialog

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product

class ProductSelectionItemKeyProvider(private val productItemAdapter: ProductSelectorAdapter) :
    ItemKeyProvider<Product>(SCOPE_CACHED) {

    override fun getKey(position: Int): Product? = productItemAdapter.getItemByPosition(position)

    override fun getPosition(key: Product): Int = productItemAdapter.getItemPosition(key.productId)

}

class ProductSelectionItemLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Product>() {

    override fun getItemDetails(e: MotionEvent): ItemDetails<Product>? {
        val viewSelected = recyclerView.findChildViewUnder(e.x, e.y)

        if (viewSelected != null) {
            val viewSelectedHolder =
                recyclerView.getChildViewHolder(viewSelected) as ProductSelectorAdapter.ProductSelectorViewHolder

            return viewSelectedHolder.getItemDetails()
        }

        return null
    }

}