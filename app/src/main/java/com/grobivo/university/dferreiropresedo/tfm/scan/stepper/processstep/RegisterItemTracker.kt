package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

class RegisterItemKeyProvider(private val registerItemTracker: ScanProcessAdapter) :
    ItemKeyProvider<Register>(SCOPE_CACHED) {

    override fun getKey(position: Int): Register? = registerItemTracker.getItemByPosition(position)

    override fun getPosition(key: Register): Int =
        registerItemTracker.getItemPosition(key.product.productId)

}

class RegisterItemLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Register>() {

    override fun getItemDetails(e: MotionEvent): ItemDetails<Register>? {
        val viewSelected = recyclerView.findChildViewUnder(e.x, e.y)

        if (viewSelected != null) {
            val viewSelectedHolder =
                recyclerView.getChildViewHolder(viewSelected) as ScanProcessAdapter.ScannedProductViewHolder

            return viewSelectedHolder.getItemDetails()
        }

        return null
    }

}