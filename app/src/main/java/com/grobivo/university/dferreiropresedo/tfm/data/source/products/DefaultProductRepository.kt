package com.grobivo.university.dferreiropresedo.tfm.data.source.products

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.local.ProductDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.remote.ProductRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultProductRepository @Inject internal constructor(
    private val localProductDataSource: ProductDatabaseSource,
    private val remoteProductDataSource: ProductRemoteSource,
    private val sessionManagement: SessionManagement
) : ProductRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getAllProductsToScan(): List<Product> = withContext(ioDispatcher) {
        return@withContext localProductDataSource.getAllProductsToScan()
    }

    override suspend fun getAllProductsSkipping(productsToSkip: List<Product>): List<Product> =
        withContext(ioDispatcher) {
            return@withContext localProductDataSource.getAllProductsSkipping(productsToSkip)
        }

    override suspend fun getAllDeviceProducts(deviceUUID: String): List<Register> =
        withContext(ioDispatcher) {
            safeApiCall(ioDispatcher) {
                remoteProductDataSource.getAllDeviceProducts(deviceUUID)
            }

            return@withContext localProductDataSource.getAllDeviceProducts(
                deviceUUID, sessionManagement.userInternalId()
            )
        }

    override suspend fun getStoredProductsByTypeId(
        deviceUUID: String, productTypeId: Long
    ): List<Register> = withContext(ioDispatcher) {
        return@withContext localProductDataSource.getStoredProductsByTypeId(
            deviceUUID, sessionManagement.userInternalId(), productTypeId
        )
    }

    override suspend fun getStoredProductsType(deviceUUID: String): LiveData<List<ProductType>> =
        withContext(ioDispatcher) {
            return@withContext localProductDataSource.getStoredProductsType(
                deviceUUID, sessionManagement.userInternalId()
            )
        }
}