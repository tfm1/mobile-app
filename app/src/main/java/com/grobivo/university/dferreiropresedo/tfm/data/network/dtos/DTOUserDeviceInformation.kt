package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.squareup.moshi.Json

data class DTOUserDeviceInformation(
    @Json(name = "isAdmin") val isAdmin: Boolean,
    @Json(name = "userInfo") val deviceInformation: DTODeviceInformation
)