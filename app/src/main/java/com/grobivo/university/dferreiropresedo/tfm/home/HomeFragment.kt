package com.grobivo.university.dferreiropresedo.tfm.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Inventory
import com.grobivo.university.dferreiropresedo.tfm.databinding.HomeFragmentBinding
import com.grobivo.university.dferreiropresedo.tfm.home.categories.HomeCategoryAdapter
import com.grobivo.university.dferreiropresedo.tfm.home.categories.HomeCategoryListener
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : Fragment() {

    private lateinit var viewDataBinding: HomeFragmentBinding

    private val viewModel: HomeViewModel by viewModels()

    private val activityViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var sessionManagement: SessionManagement

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)

        activityViewModel.cleanseCriteria()


        Timber.d("Calculating the data...")
        val deviceInventoryData = calculateData()

        Timber.d("Configuring the recycler view")
        configureRecyclerView()

        Timber.d("Retrieving the data...")
        viewModel.getDeviceProductsType(deviceInventoryData.deviceUUID)

        Timber.d("Configuring listeners...")
        configureListeners()

        return viewDataBinding.root
    }

    private fun calculateData(): DeviceInventoryMapping {

        val device = if (arguments != null) {
            HomeFragmentArgs.fromBundle(requireArguments()).device
        } else {
            Device(
                sessionManagement.getStoredDeviceUUID()!!,
                sessionManagement.getStoredDeviceName()!!
            )
        }

        val inventory: Inventory = if (sessionManagement.getStoredInventoryInternalId() == -1L) {
            val deviceInventory = viewModel.retrieveDeviceInventoryInfo(device.UUID)
            Inventory(
                deviceInventory.deviceUUID,
                deviceInventory.sharedInventoryId,
                deviceInventory.internalInventoryId
            )
        } else {
            Inventory(
                sessionManagement.getStoredDeviceUUID()!!,
                sessionManagement.getStoredInventorySharedId(),
                sessionManagement.getStoredInventoryInternalId()
            )
        }

        sessionManagement.storeDevice(device)
        sessionManagement.storeInventory(inventory)
        activityViewModel.flagCheckNavigationDrawerActivate()

        return DeviceInventoryMapping(
            device.UUID, device.name, sessionManagement.userInternalId(),
            inventory.inventoryId, inventory.sharedInventoryId
        )
    }

    private fun configureListeners() {

        viewDataBinding.homeFab.setOnClickListener {
            this.findNavController()
                .navigate(HomeFragmentDirections.actionHomeFragmentToProductsFragment(-1L))
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    private fun configureRecyclerView() {

        val homeCategoryAdapter = HomeCategoryAdapter(HomeCategoryListener {
            this.findNavController()
                .navigate(HomeFragmentDirections.actionHomeFragmentToProductsFragment(it.productTypeId))
        })

        // We process the categories of the products
        viewModel.categories.observe(viewLifecycleOwner) { categories ->

            if (categories.isEmpty()) {
                viewDataBinding.homeCategoriesList.visibility = View.GONE
                viewDataBinding.emptyInventory.visibility = View.VISIBLE
            } else {
                viewDataBinding.homeCategoriesList.visibility = View.VISIBLE
                viewDataBinding.emptyInventory.visibility = View.GONE
            }

            homeCategoryAdapter.submitList(categories.toList())
        }

        viewDataBinding.homeCategoriesList.adapter = homeCategoryAdapter

        val gridLayoutManager = GridLayoutManager(activity, 2)
        viewDataBinding.homeCategoriesList.layoutManager = gridLayoutManager

    }

}