package com.grobivo.university.dferreiropresedo.tfm.rules

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.custom.DoublePickerDialog
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule
import com.grobivo.university.dferreiropresedo.tfm.databinding.RuleItemviewBinding

class RulesAdapter(
    private val childFragmentManager: FragmentManager,
    private val viewLifecycleOwner: LifecycleOwner,
    private val ruleModifiedListener: RuleModifiedListener,
    private val adminUser: Boolean
) : ListAdapter<Rule, RulesAdapter.RuleViewHolder>(RulesDiffCallback()) {

    var selectionTracker: SelectionTracker<Rule>? = null

    interface RuleModifiedListener {
        fun ruleModified(rule: Rule)
    }

    class RuleViewHolder(
        private val binding: RuleItemviewBinding,
        private val childFragmentManager: FragmentManager,
        private val ruleModifiedListener: RuleModifiedListener
    ) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(
                parent: ViewGroup,
                childFragmentManager: FragmentManager,
                ruleModifiedListener: RuleModifiedListener
            ): RuleViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = RuleItemviewBinding.inflate(layoutInflater, parent, false)
                return RuleViewHolder(binding, childFragmentManager, ruleModifiedListener)
            }
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Rule> =
            object : ItemDetailsLookup.ItemDetails<Rule>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): Rule? = binding.rule
            }


        fun bind(
            rule: Rule,viewLifecycleOwner: LifecycleOwner,selected: Boolean,adminUser: Boolean
        ) {
            binding.rule = rule
            binding.ruleView.isChecked = selected

            if (adminUser) {
                binding.ruleQuantity.setOnClickListener {
                    val doublePicker =
                        DoublePickerDialog(rule.quantity, rule.product.productType.measure)
                    doublePicker
                        .show(childFragmentManager, DoublePickerDialog::class.qualifiedName)
                    doublePicker.valueSelected.observe(viewLifecycleOwner) {
                        binding.ruleQuantity.text = it.toString()
                        rule.quantity = it
                        ruleModifiedListener.ruleModified(rule)
                    }
                }
                binding.ruleQuantityMeasure.setOnClickListener {
                    val doublePicker =
                        DoublePickerDialog(rule.quantity, rule.product.productType.measure)
                    doublePicker
                        .show(childFragmentManager, DoublePickerDialog::class.qualifiedName)
                    doublePicker.valueSelected.observe(viewLifecycleOwner) {
                        binding.ruleQuantity.text = it.toString()
                        rule.quantity = it
                        ruleModifiedListener.ruleModified(rule)
                    }
                }
            }

            binding.executePendingBindings()
        }
    }


    fun getItemByPosition(position: Int): Rule = getItem(position)

    fun getItemPosition(ruleId: Long) = currentList.indexOfFirst { it.ruleId == ruleId }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RuleViewHolder {
        return RuleViewHolder.from(parent, childFragmentManager, ruleModifiedListener)
    }

    override fun onBindViewHolder(holder: RuleViewHolder, position: Int) {
        val rule = getItem(position)
        if (selectionTracker == null) {
            holder.bind(rule, viewLifecycleOwner, false, adminUser)
        } else {
            holder.bind(rule, viewLifecycleOwner, selectionTracker!!.isSelected(rule), adminUser)
        }
    }
}


class RulesDiffCallback : DiffUtil.ItemCallback<Rule>() {
    override fun areItemsTheSame(oldItem: Rule, newItem: Rule): Boolean {
        return oldItem.inventoryId == newItem.inventoryId && oldItem.product.productId == newItem.product.productId
    }

    override fun areContentsTheSame(oldItem: Rule, newItem: Rule): Boolean {
        return oldItem == newItem
    }

}