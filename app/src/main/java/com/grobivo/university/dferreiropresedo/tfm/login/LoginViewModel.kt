package com.grobivo.university.dferreiropresedo.tfm.login

import android.content.Intent
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.SyncRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.UserRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val userRepository: UserRepository,
    private val syncRepository: SyncRepository
) : ViewModel() {

    val INTENT_CODE_SIGN_IN = 1

    private lateinit var googleSignInClient: GoogleSignInClient
    private lateinit var googleSignInOptions: GoogleSignInOptions

    private val _triggerSignInActivity = MutableLiveData<Intent?>()
    val triggerSignInActivity: LiveData<Intent?>
        get() = _triggerSignInActivity

    private val _triggerNavigateAfterSignIn = MutableLiveData<Boolean>()
    val triggerNavigateAfterSignIn: LiveData<Boolean>
        get() = _triggerNavigateAfterSignIn

    private val _triggerErrorTryingToLogin = MutableLiveData<Boolean>()
    val triggerErrorTryingToLogin: LiveData<Boolean>
        get() = _triggerErrorTryingToLogin

    private val _triggerShowLoggingDialog = MutableLiveData<Boolean>()
    val triggerShowLoggingDialog: LiveData<Boolean>
        get() = _triggerShowLoggingDialog

    private val _loggingSteps = MutableLiveData<LoginStepsLoadingDialog.LOGGING_PROGRESS>()
    val loggingSteps: LiveData<LoginStepsLoadingDialog.LOGGING_PROGRESS>
        get() = _loggingSteps

    fun flagTriggerNavigateAfterSignInDeactivate() = _triggerNavigateAfterSignIn.postValue(false)
    fun flagTriggerSignInActivityDeactivate() = _triggerSignInActivity.postValue(null)
    fun flagTriggerErrorTryingToLoginDeactivate() = _triggerErrorTryingToLogin.postValue(false)
    fun flagTriggerShowLoggingDialogDeactivate() = _triggerShowLoggingDialog.postValue(false)

    fun configureGoogleSignIn(loginFragment: LoginFragment) {

        Timber.d("Configuring the GSI client")

        googleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(loginFragment.getString(R.string.server_client_id))
                .requestEmail()
                .build()

        googleSignInClient =
            GoogleSignIn.getClient(loginFragment.requireActivity(), googleSignInOptions)
    }

    fun googleSignInTrigger() {
        _triggerSignInActivity.value = googleSignInClient.signInIntent
    }

    fun googleSignInTriggered() {
        _triggerSignInActivity.value = null
    }

    fun handleSignInResult(data: Intent?) {

        try {
            val signedInAccountFromIntent = GoogleSignIn.getSignedInAccountFromIntent(data)
            val accountData = signedInAccountFromIntent.getResult(ApiException::class.java)
            handleSignInData(accountData)

        } catch (e: ApiException) {
            Timber.e(e, "There was an error trying to authenticate the user")
        }

    }

    fun handleSignInData(accountData: GoogleSignInAccount?) {
        accountData?.let {

            viewModelScope.launch {

                Timber.d("Starting to register the user...")
                _triggerShowLoggingDialog.postValue(true)
                _loggingSteps.postValue(LoginStepsLoadingDialog.LOGGING_PROGRESS.SIGN_IN)
                val saveUser = userRepository.saveUser(
                    accountData.photoUrl?.toString()!!, accountData.idToken!!
                )
                if (saveUser == null) {
                    Timber.w("There was an error trying to register the user.")
                    _triggerErrorTryingToLogin.postValue(true)
                } else {
                    _loggingSteps.postValue(LoginStepsLoadingDialog.LOGGING_PROGRESS.SYNC)
                    syncRepository.syncUserInformation()
                    _loggingSteps.postValue(LoginStepsLoadingDialog.LOGGING_PROGRESS.END)
                    Timber.d("User registered")
                    _triggerNavigateAfterSignIn.postValue(true)
                }
            }
        }
    }
}