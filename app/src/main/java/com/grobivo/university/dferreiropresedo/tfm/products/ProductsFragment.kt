package com.grobivo.university.dferreiropresedo.tfm.products

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentProductsBinding
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ProductsFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentProductsBinding

    private val viewModel: ProductsViewModel by viewModels()

    private val activityViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var sessionManagement: SessionManagement

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_products, container, false)

        activityViewModel.cleanseCriteria()

        val productTypeIdSelected =
            ProductsFragmentArgs.fromBundle(requireArguments()).productTypeId

        if (productTypeIdSelected == -1L) {
            Timber.d("Retrieving all the products information...")
            viewModel.findProducts(sessionManagement.getStoredDeviceUUID()!!)
        } else {
            Timber.d(
                "Retrieving the products information whose productTypeId is %s ...",
                productTypeIdSelected
            )
            viewModel.findProducts(sessionManagement.getStoredDeviceUUID()!!, productTypeIdSelected)
        }
        Timber.d("Configuring the recycler view...")
        configureRecyclerView()

        Timber.d("Configuring listeners")
        configureListeners()

        configureObservers()

        return viewDataBinding.root
    }

    private fun configureObservers() {

        viewModel.flagChangesCompleted.observe(viewLifecycleOwner) {
            if (it) {
                this.findNavController().navigate(
                    ProductsFragmentDirections.actionProductsFragmentToHomeFragment2(
                        Device(
                            sessionManagement.getStoredDeviceUUID()!!,
                            sessionManagement.getStoredDeviceName()!!
                        )
                    )
                )
            }
            viewModel.flagChangesCompletedDeactivate()
        }

    }

    private fun configureListeners() {

        viewDataBinding.productsAccept.setOnClickListener {
            viewModel.confirmChanges()
        }
    }

    private fun configureRecyclerView() {
        val productsAdapter = ProductsAdapter(childFragmentManager, viewLifecycleOwner)

        viewModel.productRegisters.observe(this.viewLifecycleOwner) { productsList ->
            productsAdapter.submitList(
                productsList.sortedWith(
                    compareBy({ it.product.productType.productTypeId }, { it.product.name })
                )
            )
        }
        viewDataBinding.productList.adapter = productsAdapter

        val gridLayoutManager = GridLayoutManager(activity, 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (productsAdapter.getItemViewType(position) == AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER) {
                    2
                } else {
                    1
                }
            }
        }

        viewDataBinding.productList.layoutManager = gridLayoutManager


        activityViewModel.searchCriteria.observe(viewLifecycleOwner) { searchCriteria ->

            val listToShow = if (!searchCriteria.isNullOrBlank()) {
                viewModel.productRegisters.value?.filter { register ->
                    register.product.name.contains(searchCriteria, true)
                }?.sortedBy { it.product.productType.productTypeId }
            } else {
                viewModel.productRegisters.value?.sortedBy { it.product.productType.productTypeId }
            }

            productsAdapter.submitList(listToShow)
            viewDataBinding.productList.smoothScrollToPosition(0)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

}