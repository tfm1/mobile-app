package com.grobivo.university.dferreiropresedo.tfm

import android.os.Bundle
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import androidx.appcompat.widget.SearchView
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import coil.load
import coil.transform.CircleCropTransformation
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.databinding.ActivityMainBinding
import com.grobivo.university.dferreiropresedo.tfm.login.LoginStepsLoadingDialog
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanCameraStep
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.navigation_drawer_header.*
import timber.log.Timber
import javax.inject.Inject


@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var viewDataBinding: ActivityMainBinding

    private lateinit var drawerLayout: DrawerLayout

    private lateinit var navController: NavController

    private val viewModel: MainViewModel by viewModels()

    @Inject
    lateinit var sessionManagement: SessionManagement

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewDataBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        /** We register the nav graph programatically due to a registered bug @see https://issuetracker.google.com/issues/143752103*/
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.main_nav_fragment) as NavHostFragment
        navController = navHostFragment.navController
        navController.setGraph(R.navigation.nav)

        setSupportActionBar(viewDataBinding.genericToolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)

        configureNavigationDrawer()
        configureObservers()

        navController.addOnDestinationChangedListener { _, destination, _ ->

            when (destination.id) {
                R.id.loginFragment -> {
                    viewDataBinding.genericToolbar.navigationIcon = null
                    drawerLayout.closeDrawers()
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
                    viewDataBinding.genericToolbarSearchview.visibility = View.GONE
                }
                else -> {
                    viewDataBinding.genericToolbar.navigationIcon =
                        AppCompatResources.getDrawable(this, R.drawable.ic_baseline_dehaze_24)
                    drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
                    viewDataBinding.genericToolbarSearchview.visibility = View.VISIBLE
                }
            }

        }

        viewDataBinding.root.requestFocus()
    }

    private fun configureObservers() {
        viewModel.flagCleanseSearchCriteria.observe(this) {
            if (it && viewDataBinding.genericToolbarSearchview.query.isNotBlank()) {
                viewDataBinding.genericToolbarSearchview.clearFocus()
                viewDataBinding.genericToolbarSearchview.setQuery("", false)
                closeKeyboardIfOpen()
                viewModel.criteriaCleansed()
            }
        }

        viewModel.flagCheckNavigationDrawer.observe(this) {
            if (it) {
                val deviceIsSelected = sessionManagement.getStoredDeviceUUID() != null
                viewDataBinding.navigationDrawerView.menu.setGroupVisible(
                    R.id.admin_drawer, sessionManagement.isAdminUser() && deviceIsSelected
                )

                viewDataBinding.navigationDrawerView.menu.findItem(R.id.homeFragment).isVisible =
                    deviceIsSelected
                viewDataBinding.navigationDrawerView.menu.findItem(R.id.rulesFragment).isVisible =
                    deviceIsSelected
                viewDataBinding.navigationDrawerView.menu.findItem(R.id.scanFragment).isVisible =
                    deviceIsSelected
                viewDataBinding.navigationDrawerView.menu.findItem(R.id.groceriesFragment).isVisible =
                    deviceIsSelected
                viewDataBinding.navigationDrawerView.menu.findItem(R.id.sync).isVisible =
                    deviceIsSelected
            }
            viewModel.flagCheckNavigationDrawerDeactivate()
        }
    }

    private fun configureNavigationDrawer() {

        drawerLayout = viewDataBinding.drawerLayout
        NavigationUI.setupWithNavController(viewDataBinding.navigationDrawerView, navController)

        viewDataBinding.genericToolbar.setNavigationOnClickListener {
            drawerLayout.openDrawer(GravityCompat.START)
        }

        drawerLayout.addDrawerListener(object : DrawerLayout.DrawerListener {
            override fun onDrawerSlide(drawerView: View, slideOffset: Float) {
            }

            override fun onDrawerOpened(drawerView: View) {
            }

            override fun onDrawerClosed(drawerView: View) {
            }

            override fun onDrawerStateChanged(newState: Int) {
                if (DrawerLayout.STATE_SETTLING == newState) {
                    drawerLayout.navigation_drawer_view.requestFocus()
                    closeKeyboardIfOpen()
                    fulfillDrawerFields()
                }
            }
        })

        viewDataBinding.navigationDrawerView.setNavigationItemSelectedListener {

            when (it.itemId) {
                R.id.devicesFragment, R.id.usersFragment,
                R.id.rulesFragment, R.id.scanFragment, R.id.groceriesFragment -> navController.navigate(
                    it.itemId
                )

                R.id.homeFragment -> {
                    val bundle = Bundle()
                    bundle.putParcelable(
                        "device",
                        Device(
                            sessionManagement.getStoredDeviceUUID()!!,
                            sessionManagement.getStoredDeviceName()!!
                        )
                    )
                    navController.navigate(it.itemId, bundle)
                }

                R.id.logout -> {
                    viewModel.logoutUser(this)
                    navController.navigate(NavDirections.actionLogout())
                }

                R.id.sync -> {
                    LoginStepsLoadingDialog(viewModel.syncSteps).show(
                        supportFragmentManager, "LoginStepsLoadingDialogMainActivity"
                    )
                    viewModel.syncInformation()
                }
                else -> {
                    Timber.d("Trying to navigate to the screen ${it.itemId}")
                    Toast.makeText(
                        this,
                        "This screen is not implemented yet. Stay tuned to check it in the future!",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

            drawerLayout.closeDrawer(GravityCompat.START)
            return@setNavigationItemSelectedListener true
        }
    }

    private fun closeKeyboardIfOpen() {
        val imm: InputMethodManager =
            getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        if (imm.isActive) {
            var currentFocus = currentFocus
            if (currentFocus == null) {
                currentFocus = View(applicationContext)
            }
            imm.hideSoftInputFromWindow(currentFocus.windowToken, 0)
        }
    }

    private fun fulfillDrawerFields() {
        this.nav_header_user_name?.text = sessionManagement.getUsernameLogged()
        this.header?.text = sessionManagement.getStoredDeviceName()

        sessionManagement.getUserPhotoUrl()?.let {
            this.nav_header_user_avatar?.load(it) {
                transformations(CircleCropTransformation())
                crossfade(true)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == ScanCameraStep.REQUEST_CODE_PERMISSIONS) {
            viewModel.flagRequestPermissionsActivated()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)

        viewDataBinding.genericToolbarSearchview.apply {

            setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    viewModel.modifyCriteria(query)
                    return false
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    viewModel.modifyCriteria(newText)
                    return false
                }
            })
        }

        return true
    }

    override fun onBackPressed() {

        if (drawerLayout.isDrawerVisible(drawerLayout.navigation_drawer_view)) {
            drawerLayout.closeDrawer(GravityCompat.START, true)
        } else {
            super.onBackPressed()
        }

    }

    override fun onResume() {
        super.onResume()


        val account = GoogleSignIn.getLastSignedInAccount(this)

        val googleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()

        val googleSignInClient =
            GoogleSignIn.getClient(this, googleSignInOptions)

        googleSignInClient.silentSignIn()
            .addOnSuccessListener(this) {
                if (it.idToken != null) {
                    sessionManagement.updateAccessToken(it.idToken!!)
                }

            }.addOnFailureListener {
                if (account != null) {
                    Toast.makeText(this, R.string.generic_silent_login_error, Toast.LENGTH_LONG)
                        .show()
                }
                sessionManagement.logoutUser()
                navController.navigate(
                    R.id.loginFragment, null,
                    NavOptions.Builder()
                        .setPopUpTo(R.id.loginFragment, true)
                        .build()
                )
            }
    }

}