package com.grobivo.university.dferreiropresedo.tfm.di

import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep.RuleStepperProduct
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.quantitystep.RuleStepperQuantity
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanCameraStep
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanPreviewStep
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep.ScanProcessStep
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.FragmentComponent

@InstallIn(FragmentComponent::class)
@Module
class FragmentDIModule {

    @Provides
    fun provideRuleStepperProduct(): RuleStepperProduct = RuleStepperProduct()

    @Provides
    fun provideRuleStepperQuantity(): RuleStepperQuantity = RuleStepperQuantity()

    @Provides
    fun provideScanCameraStep(): ScanCameraStep = ScanCameraStep()

    @Provides
    fun provideScanPreviewStep(): ScanPreviewStep = ScanPreviewStep()

    @Provides
    fun provideScanProcessStep(): ScanProcessStep = ScanProcessStep()
}