package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentScanProcessStepBinding
import com.grobivo.university.dferreiropresedo.tfm.scan.ScanContainerViewModel
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.dialog.ProductSelectorDialogFragment
import com.stepstone.stepper.Step
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import timber.log.Timber
import javax.inject.Inject

class ScanProcessStep @Inject constructor() :
    Fragment(), Step, ProductSelectorDialogFragment.ProductsToRegisterSelected,
    ActionMode.Callback {

    private lateinit var viewDataBinding: FragmentScanProcessStepBinding

    private val containerViewModel: ScanContainerViewModel by activityViewModels()

    lateinit var stepperLayout: StepperLayout

    private var actionMode: ActionMode? = null
    private lateinit var selectionTracker: SelectionTracker<Register>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("Initializing the process step...")

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_scan_process_step, container, false)

        configureRecyclerView()
        configureListeners()
        configureObservers()

        viewDataBinding.scannedProductsList.visibility = View.VISIBLE
        viewDataBinding.scanningProgressBarText.visibility = View.VISIBLE
        viewDataBinding.progressBarScanning.visibility = View.VISIBLE

        return viewDataBinding.root
    }

    private fun configureObservers() {
        containerViewModel.flagScannedProductsFinished.observe(viewLifecycleOwner) {
            if (it) {
                viewDataBinding.scanningProgressBarText.visibility = View.GONE
                viewDataBinding.progressBarScanning.visibility = View.GONE
                containerViewModel.flagScannedProductsFinishedDeactivate()
            }
        }
    }

    private fun configureRecyclerView() {
        val scanProcessAdapter = ScanProcessAdapter(childFragmentManager, viewLifecycleOwner)
        viewDataBinding.scannedProductsList.adapter = scanProcessAdapter
        viewDataBinding.scannedProductsList.layoutManager = GridLayoutManager(activity, 2)

        val selectionTracker = generateSelectionTracker(scanProcessAdapter)
        scanProcessAdapter.selectionTracker = selectionTracker

        containerViewModel.scannedProducts.observe(viewLifecycleOwner) {
            scanProcessAdapter.submitList(it)
        }
    }

    private fun generateSelectionTracker(scanProcessAdapter: ScanProcessAdapter): SelectionTracker<Register> {

        selectionTracker = SelectionTracker.Builder(
            "registerItemTracker",
            viewDataBinding.scannedProductsList,
            RegisterItemKeyProvider(scanProcessAdapter),
            RegisterItemLookup(viewDataBinding.scannedProductsList),
            StorageStrategy.createParcelableStorage(Register::class.java)
        )
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        selectionTracker.addObserver(object : SelectionTracker.SelectionObserver<Register>() {

            override fun onSelectionChanged() {
                super.onSelectionChanged()

                selectionTracker.let {
                    val selectedItems = it.selection.toList()

                    if (selectedItems.isEmpty()) {
                        actionMode?.finish()
                    } else {

                        if (actionMode == null) {
                            val requireActivity: AppCompatActivity =
                                requireActivity() as AppCompatActivity

                            actionMode =
                                requireActivity.startSupportActionMode(this@ScanProcessStep)
                        }

                        actionMode?.title = "${selectedItems.size}"
                    }
                }
            }
        })
        return selectionTracker
    }

    private fun configureListeners() {
        viewDataBinding.addProductToScan.setOnClickListener {

            val alreadyCreatedRegisters = containerViewModel.scannedProducts.value ?: emptyList()
            val alreadyRegisteredProducts = alreadyCreatedRegisters.map { it.product }

            containerViewModel.retrievePendingProductsToBeScanned(alreadyRegisteredProducts)

            ProductSelectorDialogFragment(containerViewModel.pendingProductsToBeScanned, this).show(
                childFragmentManager,
                ProductSelectorDialogFragment::class.java.toString()
            )
        }
    }

    override fun verifyStep(): VerificationError? {
        return if (containerViewModel.photoResult == null) {
            VerificationError("A photo must have been taken")
        } else {
            null
        }
    }

    override fun onSelected() {
        viewDataBinding.scanningProgressBarText.visibility = View.VISIBLE
        viewDataBinding.progressBarScanning.visibility = View.VISIBLE
        containerViewModel.retrieveProductsToScan()
        containerViewModel.scanProducts(requireContext())
    }

    override fun onError(error: VerificationError) {
        TODO("Not yet implemented")
    }

    override fun onProductsSelected(products: List<Product>?) {
        products?.let {
            containerViewModel.addProductsManuallyScanned(products)
        }
    }

    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        mode?.let {
            it.menuInflater.inflate(R.menu.action_mode_menu, menu)
            return true
        }

        return false
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        return true
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.action_mode_delete_view -> {
                Timber.d("Action mode - Delete clicked")

                val confirmDialog = AlertDialog.Builder(requireContext())

                confirmDialog.apply {
                    setTitle(R.string.generic_text_confirmation_question)
                    setMessage(R.string.scan_process_delete_confirmation_body)
                    setPositiveButton(R.string.generic_text_yes) { _, _ ->
                        Timber.d("Action mode - Removing selected registers")
                        val itemsSelected = selectionTracker.selection.toList()

                        containerViewModel.manuallyRemoveProducts(itemsSelected)

                        Toast.makeText(
                            requireContext(),
                            resources.getQuantityString(
                                R.plurals.scan_process_dialog_confirm_delete_reply,
                                itemsSelected.size, itemsSelected.size
                            ),
                            Toast.LENGTH_SHORT
                        ).show()

                        actionMode?.finish()
                    }
                    setNegativeButton(R.string.generic_text_no) { _, _ ->
                        Timber.d("Action mode - Reverted delete action")
                        // We just dismiss the alert and do nothing
                    }
                    create()
                }.show()
            }
        }

        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        Timber.d("Action mode - Destroy")
        val registerItemAdapter = viewDataBinding.scannedProductsList.adapter as ScanProcessAdapter
        registerItemAdapter.selectionTracker?.clearSelection()

        actionMode = null
        (requireActivity() as AppCompatActivity).supportActionBar?.show()
        registerItemAdapter.notifyDataSetChanged()
    }
}