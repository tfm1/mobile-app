package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device


@Entity(tableName = "device")
data class EntityDevice constructor(
    @PrimaryKey @ColumnInfo(name = "UUID") var UUID: String = "",
    @ColumnInfo(name = "name") var name: String = ""
)

fun EntityDevice.asDomainModel() = Device(UUID, name)
fun List<EntityDevice>.asDomainModel(): List<Device> = map { it.asDomainModel() }


@Entity(
    primaryKeys = ["user_internal_id", "device_UUID"],
    tableName = "device_users"
)
data class DeviceUsersRelation(
    @ColumnInfo(name = "user_internal_id") val userInternalId: String,
    @ColumnInfo(name = "device_UUID") val deviceUUID: String
)