package com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentRuleProductStepBinding
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.RuleStepperViewModel
import com.stepstone.stepper.Step
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import javax.inject.Inject


class RuleStepperProduct @Inject constructor() : Fragment(), Step {

    private lateinit var viewDataBinding: FragmentRuleProductStepBinding
    private lateinit var recyclerViewAdapter: RuleStepperProductAdapter

    private val stepperViewModel: RuleStepperViewModel by activityViewModels()

    lateinit var stepperLayout: StepperLayout

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_rule_product_step, container, false)

        configureRecyclerView()
        configureListeners()

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    override fun verifyStep(): VerificationError? = null

    override fun onSelected() = stepperViewModel.retrieveProductsInfo()

    private fun configureRecyclerView() {
        recyclerViewAdapter = RuleStepperProductAdapter()
        viewDataBinding.productsList.adapter = recyclerViewAdapter
        recyclerViewAdapter.selectionTracker = generateSelectionTracker(recyclerViewAdapter)

        val gridLayoutManager = GridLayoutManager(requireContext(), 2)
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (recyclerViewAdapter.getItemViewType(position) == AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER) {
                    2
                } else {
                    1
                }
            }
        }

        viewDataBinding.productsList.layoutManager = gridLayoutManager
    }

    private fun generateSelectionTracker(ruleStepperProductAdapter: RuleStepperProductAdapter): SelectionTracker<Register> {

        val selectionTracker = SelectionTracker.Builder(
            "ruleStepperProductItemTracker",
            viewDataBinding.productsList,
            RuleStepperProductItemKeyProvider(ruleStepperProductAdapter),
            RuleStepperProductItemLookup(viewDataBinding.productsList),
            StorageStrategy.createParcelableStorage(Register::class.java)
        )
            .withSelectionPredicate(SelectionPredicates.createSelectSingleAnything())
            .build()

        selectionTracker.addObserver(object : SelectionTracker.SelectionObserver<Register>() {

            override fun onSelectionChanged() {
                super.onSelectionChanged()
                selectionTracker.let {
                    stepperViewModel.productSelected = it.selection.elementAtOrNull(0)
                    stepperLayout.setNextButtonEnabled(it.hasSelection())
                    stepperLayout.setNextButtonVerificationFailed(!it.hasSelection())
                }
            }
        })

        return selectionTracker
    }

    private fun configureListeners() {

        stepperViewModel.products.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                viewDataBinding.emptyProductsToCreateRule.visibility = View.VISIBLE
                viewDataBinding.productsList.visibility = View.GONE
            } else {
                viewDataBinding.emptyProductsToCreateRule.visibility = View.GONE
                viewDataBinding.productsList.visibility = View.VISIBLE
                recyclerViewAdapter.submitList(it)
            }
            stepperLayout.setNextButtonEnabled(false)
            stepperLayout.setNextButtonVerificationFailed(true)
            recyclerViewAdapter.selectionTracker?.let { tracker ->
                stepperLayout.setNextButtonEnabled(tracker.hasSelection())
                stepperLayout.setNextButtonVerificationFailed(!tracker.hasSelection())
            }
        }
    }

    override fun onError(error: VerificationError) {
        TODO("Not yet implemented")
    }
}