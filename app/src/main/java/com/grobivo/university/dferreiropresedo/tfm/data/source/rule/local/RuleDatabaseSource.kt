package com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule

interface RuleDatabaseSource {

    suspend fun getAmountOfProductsWithoutRule(inventoryId: Long): LiveData<Int>

    suspend fun getAllProductsWithoutRule(inventoryId: Long): List<Register>

    suspend fun createRule(inventoryId: Long, product: Product, quantity: Float): Long

    suspend fun createRules(rules: List<Rule>)

    suspend fun retrieveInventoryRules(inventoryId: Long): LiveData<List<Rule>>

    suspend fun storeRuleChanges(rules: List<Rule>)

    suspend fun removeRules(rulesToDelete: Set<Rule>)
}