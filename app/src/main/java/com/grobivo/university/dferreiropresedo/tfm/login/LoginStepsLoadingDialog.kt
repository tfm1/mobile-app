package com.grobivo.university.dferreiropresedo.tfm.login

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDialogLoginLoadingStepsBinding

class LoginStepsLoadingDialog constructor(
    private val steps: LiveData<LOGGING_PROGRESS>
) : DialogFragment() {

    private lateinit var binding: CustomDialogLoginLoadingStepsBinding

    enum class LOGGING_PROGRESS { BEGINNING, SIGN_IN, SYNC, END }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.custom_dialog_login_loading_steps, null, false
        )

        steps.observe(this) { step ->
            when (step) {
                LOGGING_PROGRESS.BEGINNING -> {}
                LOGGING_PROGRESS.SIGN_IN -> {
                    binding.loggingCurrentStep.text =
                        getString(R.string.login_logging_loading_step_1)
                    binding.loggingStepsProgressBar.progress = 40
                }
                LOGGING_PROGRESS.SYNC -> {
                    binding.loggingCurrentStep.text =
                        getString(R.string.login_logging_loading_step_2)
                    binding.loggingStepsProgressBar.progress = 60
                }
                LOGGING_PROGRESS.END -> {
                    binding.loggingStepsProgressBar.progress = 100
                    this.dismiss()
                }
                else -> {
                    this.dismiss()
                }
            }

        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            return AlertDialog.Builder(context).apply {
                setView(binding.root)
                setCancelable(false)
            }.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }

    override fun onDetach() {
        steps.removeObservers(this)
        super.onDetach()
    }

}