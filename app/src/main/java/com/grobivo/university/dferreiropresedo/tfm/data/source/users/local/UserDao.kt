package com.grobivo.university.dferreiropresedo.tfm.data.source.users.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityDevice
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityUser

@Dao
interface UserDao {

    @Insert
    fun insertSync(user: EntityUser)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(user: EntityUser)

    @Update
    suspend fun update(user: EntityUser)

    @Query("""SELECT * FROM user WHERE internal_id LIKE :userInternalId""")
    suspend fun findUserInfo(userInternalId: String): EntityUser?

    @Query(
        """SELECT d.*
        FROM device_users ud JOIN device d ON ud.device_UUID = d.UUID AND ud.user_internal_id LIKE :userInternalId
        """
    )
    fun getUserDevices(userInternalId: String): LiveData<List<EntityDevice>>

    @Query(
        """SELECT d.*
        FROM device_users ud JOIN device d ON ud.device_UUID = d.UUID AND ud.user_internal_id LIKE :userInternalId
        """
    )
    fun getUserConnectedDevices(userInternalId: String): List<EntityDevice>
}