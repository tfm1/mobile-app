package com.grobivo.university.dferreiropresedo.tfm.di

import android.content.Context
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import timber.log.Timber
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class SessionModule {

    @Singleton
    @Provides
    fun provideSessionManagement(@ApplicationContext context: Context): SessionManagement {
        Timber.d("Providing the Session Management")

        return SessionManagement(context)
    }
}