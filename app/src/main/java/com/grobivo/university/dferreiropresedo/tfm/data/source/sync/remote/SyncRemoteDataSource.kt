package com.grobivo.university.dferreiropresedo.tfm.data.source.sync.remote

import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTOUserDeviceInformation
import javax.inject.Inject

class SyncRemoteDataSource @Inject constructor(
    private val endpointService: EndpointService
) : SyncRemoteSource {

    override suspend fun syncUserInformation(): List<DTOUserDeviceInformation> {
        return endpointService.syncUserInformation()
    }
}