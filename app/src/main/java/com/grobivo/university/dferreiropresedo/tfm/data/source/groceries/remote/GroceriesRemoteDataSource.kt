package com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import javax.inject.Inject

class GroceriesRemoteDataSource @Inject constructor(
    private val endpointService: EndpointService,
    private val sessionManagement: SessionManagement
) : GroceriesRemoteSource {

    override suspend fun requestGroceriesList(): List<Register> {

        val requestGroceriesList = endpointService.requestGroceriesList(
            sessionManagement.getStoredDeviceUUID()!!,
            sessionManagement.getStoredDeviceName()!!
        )

        return requestGroceriesList.asDomainModel()
    }
}