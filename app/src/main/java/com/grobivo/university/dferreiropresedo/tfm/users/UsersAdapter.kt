package com.grobivo.university.dferreiropresedo.tfm.users

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.databinding.UsersManagementItemlistViewBinding

class UsersAdapter(private val listener: UsersDataManagementListener) :
    ListAdapter<User, UsersAdapter.UserManagementHolder>(UsersManagementDiffCallback()) {

    class UserManagementHolder private constructor(val binding: UsersManagementItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): UserManagementHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    UsersManagementItemlistViewBinding.inflate(layoutInflater, parent, false)
                return UserManagementHolder(binding)
            }
        }

        fun bind(user: User, listener: UsersDataManagementListener) {
            binding.userInfo = user
            binding.listener = UsersViewManagementListener(listener, binding)

            if (user.isAdmin) {
                configurePrivilegedUser(binding)
            } else {
                configureUnprivilegedUser(binding)
            }

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserManagementHolder {
        return UserManagementHolder.from(parent)
    }

    override fun onBindViewHolder(holder: UserManagementHolder, position: Int) {
        holder.bind(getItem(position), listener)
    }
}

class UsersManagementDiffCallback : DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.internalId == newItem.internalId
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }
}


class UsersViewManagementListener(
    private val dataManagementListener: UsersDataManagementListener,
    private val binding: UsersManagementItemlistViewBinding
) {

    fun onDeleteUser(user: User) {
        dataManagementListener.onDeleteUser(user)
    }

    fun onUpgradeUser(user: User) {
        user.isAdmin = true
        dataManagementListener.onModifyUser(user)
        configurePrivilegedUser(binding)
    }

    fun onDowngradeUser(user: User) {
        user.isAdmin = false
        dataManagementListener.onModifyUser(user)
        configureUnprivilegedUser(binding)
    }
}

interface UsersDataManagementListener {
    fun onDeleteUser(user: User)
    fun onModifyUser(user: User)
}


private fun configurePrivilegedUser(binding: UsersManagementItemlistViewBinding) {
    binding.setAdminUser.visibility = View.GONE
    binding.downgradeUser.visibility = View.VISIBLE
    binding.userManagedName.setCompoundDrawablesWithIntrinsicBounds(
        R.drawable.ic_baseline_star_24, 0, 0, 0
    )
}

private fun configureUnprivilegedUser(binding: UsersManagementItemlistViewBinding) {
    binding.setAdminUser.visibility = View.VISIBLE
    binding.downgradeUser.visibility = View.GONE
    binding.userManagedName.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0)
}