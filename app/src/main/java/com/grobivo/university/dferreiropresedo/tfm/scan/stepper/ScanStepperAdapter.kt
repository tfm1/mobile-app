package com.grobivo.university.dferreiropresedo.tfm.scan.stepper

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep.ScanProcessStep
import com.stepstone.stepper.Step
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel

class ScanStepperAdapter constructor(
    fm: FragmentManager,
    context: Context,
    private val scanCameraStep: ScanCameraStep,
    private val scanPreviewStep: ScanPreviewStep,
    private val scanProcessStep: ScanProcessStep
) :
    AbstractFragmentStepAdapter(fm, context) {

    override fun getCount(): Int = 3

    override fun createStep(position: Int): Step {

        return when (position) {
            0 -> scanCameraStep
            1 -> scanPreviewStep
            2 -> scanProcessStep
            else -> TODO("Something went wrong")
        }
    }

    override fun getViewModel(position: Int): StepViewModel {

        return when (position) {
            0 -> StepViewModel.Builder(context)
                .setBackButtonVisible(false)
                .setEndButtonVisible(false)
                .create()
            1 -> StepViewModel.Builder(context)
                .setBackButtonVisible(true)
                .setBackButtonLabel(R.string.generic_text_back)
                .setEndButtonLabel(R.string.generic_text_next)
                .create()
            2 -> StepViewModel.Builder(context)
                .setBackButtonVisible(true)
                .setBackButtonLabel(R.string.generic_text_back)
                .setEndButtonLabel(R.string.generic_text_complete)
                .create()
            else -> TODO("Another thing wrong")
        }
    }
}