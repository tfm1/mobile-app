package com.grobivo.university.dferreiropresedo.tfm.data.source.groceries

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.remote.GroceriesRemoteSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


class DefaultGroceriesRepository @Inject constructor(
    private val groceriesRemoteDataSource: GroceriesRemoteSource
) : GroceriesRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun requestGroceriesList(): ResultWrapper<List<Register>> =
        safeApiCall(ioDispatcher) {
            groceriesRemoteDataSource.requestGroceriesList()
        }
}