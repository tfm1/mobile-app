package com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityRule
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductRuleMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.RegisterMapping

@Dao
interface RuleDao {

    @Insert
    suspend fun createRule(rule: EntityRule): Long

    @Insert
    suspend fun createRules(rule: List<EntityRule>)

    @Query(
        """
        SELECT COUNT(p.product_id)
        FROM product p
        WHERE NOT EXISTS(
            SELECT 1
            FROM rule r
            where r.product_id = p.product_id AND r.inventory_id == :inventoryId
        )
    """
    )
    fun getAmountOfProductsWithoutRule(inventoryId: Long): LiveData<Int>

    @Query(
        """
        SELECT 0 as registerId, 0 as sharedRegisterId, 0 as inventoryId, p.product_id as productId, p.name, p.ticket_code as ticketCode,
            pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure, 0 as quantity
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id
        WHERE NOT EXISTS(
            SELECT 1
            FROM rule r
            WHERE r.product_id = p.product_id AND r.inventory_id == :inventoryId
        )
        """
    )
    suspend fun getAllProductsWithoutRule(inventoryId: Long): List<RegisterMapping>

    @Query(
        """
        SELECT r.rule_id as ruleId, r.shared_rule_id as sharedRuleId, r.inventory_id as inventoryId, r.product_id as productId, r.quantity as quantity,
            p.name as name, p.ticket_code as ticketCode, pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure
        FROM rule r JOIN product p ON r.product_id = p.product_id 
            JOIN product_type pt ON pt.product_type_id = p.product_type_id 
        WHERE r.inventory_id = :inventoryId 
    """
    )
    fun retrieveInventoryRules(inventoryId: Long): LiveData<List<ProductRuleMapping>>

    @Query(
        """
        SELECT r.rule_id as ruleId, r.shared_rule_id as sharedRuleId, r.inventory_id as inventoryId, r.product_id as productId, r.quantity as quantity,
            p.name as name, p.ticket_code as ticketCode, pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure
        FROM rule r JOIN product p ON r.product_id = p.product_id 
            JOIN product_type pt ON pt.product_type_id = p.product_type_id 
        WHERE r.inventory_id = :inventoryId 
    """
    )
    suspend fun retrieveInventoryRulesSync(inventoryId: Long): List<ProductRuleMapping>


    @Query("""DELETE FROM rule WHERE inventory_id IN (:inventoryIds)""")
    suspend fun cleanseDevicesInventoryRules(inventoryIds: List<Long>)

    @Update
    suspend fun updateRules(rules: List<EntityRule>)

    @Delete
    suspend fun removeRules(rules: List<EntityRule>)
}