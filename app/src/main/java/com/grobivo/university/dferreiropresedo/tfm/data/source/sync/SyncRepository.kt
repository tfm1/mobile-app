package com.grobivo.university.dferreiropresedo.tfm.data.source.sync

interface SyncRepository {

    suspend fun syncUserInformation()

}