package com.grobivo.university.dferreiropresedo.tfm.groceries

import androidx.lifecycle.*
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.GroceriesRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GroceriesViewModel @Inject constructor(
    private val groceriesRepository: GroceriesRepository
) : ViewModel() {

    private val _flagErrorRequestingGroceries = MutableLiveData(false)
    val flagErrorRequestingGroceries: LiveData<Boolean>
        get() = _flagErrorRequestingGroceries

    fun flagErrorRequestingGroceriesDeactivate() = _flagErrorRequestingGroceries.postValue(false)

    private val _productsToBuy = MutableLiveData<List<Register>>()
    val productsToBuy: LiveData<List<Register>>
        get() = Transformations.map(_productsToBuy) { groceries ->
            groceries.sortedWith(
                compareBy(
                    { it.product.name },
                    { it.quantity }
                )
            )
        }

    fun requestGroceriesList() {

        viewModelScope.launch {
            val requestGroceriesListWrapper = groceriesRepository.requestGroceriesList()

            when (requestGroceriesListWrapper) {
                is ResultWrapper.Success -> {
                    _productsToBuy.postValue(requestGroceriesListWrapper.value!!)
                }
                else -> {
                    _flagErrorRequestingGroceries.postValue(true)
                    _productsToBuy.postValue(emptyList())
                }
            }
        }

    }

}