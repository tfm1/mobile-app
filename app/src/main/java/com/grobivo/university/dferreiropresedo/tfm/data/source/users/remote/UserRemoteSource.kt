package com.grobivo.university.dferreiropresedo.tfm.data.source.users.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User

interface UserRemoteSource {

    suspend fun registerUser(idToken: String, userPhotoUrl: String?): User

    suspend fun upgradeUsers(users: List<User>)
}