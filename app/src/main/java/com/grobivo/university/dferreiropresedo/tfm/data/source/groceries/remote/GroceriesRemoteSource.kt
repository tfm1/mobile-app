package com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface GroceriesRemoteSource {

    suspend fun requestGroceriesList(): List<Register>

}