package com.grobivo.university.dferreiropresedo.tfm.devices

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import androidx.lifecycle.*
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.asDomainDevice
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.DevicesRepository
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class DevicesViewModel @Inject constructor(
    private val devicesRepository: DevicesRepository,
    private val sessionManagement: SessionManagement,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    private val GOOGLE_DEVICE_PREFIX = "7C:D9:5C"

    /** Devices retrieved from database */
    private val _devicesRetrieved = MediatorLiveData<List<Device>>()
    val devicesRetrieved: LiveData<List<Device>>
        get() = _devicesRetrieved

    /** Devices selected by the user in the list */
    private val userSelectedDevices: HashSet<Device> = HashSet()

    /** Possible devices to connect via Bluetooth */
    private val _chooseBluetoothDevice = MutableLiveData<List<BluetoothDevice>?>()
    val chooseBluetoothDevice: LiveData<List<BluetoothDevice>?>
        get() = _chooseBluetoothDevice

    private val _flagSelectedDeviceWasRemoved = MutableLiveData<Boolean>(false)
    val flagSelectedDeviceWasRemoved: LiveData<Boolean>
        get() = _flagSelectedDeviceWasRemoved

    fun retrieveStoredDevices() {
        Timber.d("Retrieving stored devices.")
        viewModelScope.launch {
            _devicesRetrieved.addSource(devicesRepository.getAllDevicesFromUser()) {
                _devicesRetrieved.postValue(it)
            }
        }
    }

    fun connectDeviceBluetooth() {
        val bluetoothAdapter: BluetoothAdapter? = BluetoothAdapter.getDefaultAdapter()

        // we retrieve the Google devices which has not already been stored
        val bondedGoogleDevices = bluetoothAdapter?.bondedDevices?.filter {
            it.address.startsWith(
                GOOGLE_DEVICE_PREFIX,
                true
            ) && _devicesRetrieved.value?.contains(it.asDomainDevice())?.not() ?: true
        }
        _chooseBluetoothDevice.postValue(bondedGoogleDevices!!)
    }


    fun connectSelectedDevice(connectedGoogleDevice: BluetoothDevice) {
        viewModelScope.launch {
            devicesRepository.connectDeviceToUser(connectedGoogleDevice.asDomainDevice())
        }
    }

    fun disconnectUserSelectedDevices(): Int {
        val numberOfElements = userSelectedDevices.size
        val devicesToDisconnect = userSelectedDevices.map { it.UUID }
        viewModelScope.launch {
            devicesRepository.disconnectDevices(ArrayList<Device>(userSelectedDevices))
            if (devicesToDisconnect.contains(sessionManagement.getStoredDeviceUUID())) {
                sessionManagement.cleanDevice()
                _flagSelectedDeviceWasRemoved.postValue(true)
            }
        }
        userSelectedDevices.clear()

        return numberOfElements
    }

    fun addUserSelectedDevices(selectedItems: List<Device>) {
        userSelectedDevices.addAll(selectedItems)
    }

    fun clearUserSelectedDevices() {
        userSelectedDevices.clear()
    }

    fun closeModalUserChooseDevice() {
        _chooseBluetoothDevice.postValue(null)
    }

    fun flagSelectedDeviceWasRemovedDeactivate() {
        _flagSelectedDeviceWasRemoved.postValue(false)
    }
}