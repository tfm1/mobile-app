package com.grobivo.university.dferreiropresedo.tfm.data.source.users.local

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User

interface UserDatabaseSource {

    suspend fun insert(user: User)
}