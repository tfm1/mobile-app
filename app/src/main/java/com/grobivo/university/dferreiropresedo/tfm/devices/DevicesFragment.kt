package com.grobivo.university.dferreiropresedo.tfm.devices

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.*
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.databinding.DevicesFragmentBinding
import com.grobivo.university.dferreiropresedo.tfm.util.ENABLE_BLUETOOTH_REQUEST
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.devices_fragment.*
import kotlinx.android.synthetic.main.navigation_drawer_header.*
import timber.log.Timber


@AndroidEntryPoint
class DevicesFragment : Fragment(), ActionMode.Callback,
    ConnectDevicesDialogFragment.ConnectDevicesDialogListener {

    private lateinit var viewDataBinding: DevicesFragmentBinding

    private val viewModel: DevicesViewModel by viewModels()

    private val activityViewModel: MainViewModel by activityViewModels()

    private var actionMode: ActionMode? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        findNavController().popBackStack(R.id.devicesFragment, false)

        activityViewModel.cleanseCriteria()
        Timber.d("onCreateView")

        viewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.devices_fragment, container, false
        )

        Timber.d("onCreateView - Recycler View")
        insertRecyclerView()
        configureListeners()
        viewModel.retrieveStoredDevices()
        Timber.d("onCreateView finished")

        return viewDataBinding.root
    }


    private fun configureListeners() {

        viewDataBinding.devicesBluetoothButton.setOnClickListener {

            if (BluetoothAdapter.getDefaultAdapter()?.isEnabled == false) {
                Timber.d("Bluetooth disabled, trying to enable it...")
                startActivityForResult(
                    Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE),
                    ENABLE_BLUETOOTH_REQUEST
                )

            } else {
                viewModel.connectDeviceBluetooth()
            }

        }

        viewModel.chooseBluetoothDevice.observe(viewLifecycleOwner) {
            it?.let {
                ConnectDevicesDialogFragment(it, this).show(
                    childFragmentManager,
                    ConnectDevicesDialogFragment::class.java.toString()
                )
                viewModel.closeModalUserChooseDevice()
            }
        }

        viewModel.flagSelectedDeviceWasRemoved.observe(viewLifecycleOwner) {
            if (it) {
                activityViewModel.flagCheckNavigationDrawerActivate()
            }
            viewModel.flagSelectedDeviceWasRemovedDeactivate()
        }


        activityViewModel.searchCriteria.observe(viewLifecycleOwner, { criteria ->

            val deviceAdapter = viewDataBinding.devicesContainerView.adapter as DeviceItemAdapter
            val allDevicesShown =
                deviceAdapter.currentList.size == viewModel.devicesRetrieved.value?.size

            if (!criteria.isNullOrBlank() || !allDevicesShown) {
                val listToShow = if (!criteria.isNullOrBlank()) {
                    viewModel.devicesRetrieved.value?.filter { deviceToFilter ->
                        deviceToFilter.name.contains(criteria, true)
                    }
                } else {
                    viewModel.devicesRetrieved.value
                }?.sortedBy { deviceToSort -> deviceToSort.name }

                deviceAdapter.submitList(listToShow)
                viewDataBinding.devicesContainerView.smoothScrollToPosition(0)
            }
        })

    }

    private fun insertRecyclerView() {

        val adapter = DeviceItemAdapter(DeviceItemListener { device ->
            this.findNavController()
                .navigate(DevicesFragmentDirections.actionDevicesFragmentToHomeFragment(device))
        })

        viewDataBinding.devicesContainerView.adapter = adapter

        viewModel.devicesRetrieved.observe(viewLifecycleOwner, { devicesToShow ->
            devicesToShow?.let { devices ->
                // in case the devices list is empty, we have to show the corresponding message
                if (devices.isEmpty()) {
                    viewDataBinding.devicesEmpty.visibility = View.VISIBLE
                } else {
                    viewDataBinding.devicesEmpty.visibility = View.GONE
                }
                adapter.submitList(devices.sortedBy { device -> device.name })
            }
        })

        Timber.d("onCreateView - SelectionTracker")
        val selectionTracker = generateSelectionTracker(adapter)
        Timber.d("onCreateView - SelectionTracker finished")

        adapter.selectionTracker = selectionTracker

        val gridLayoutManager = GridLayoutManager(activity, 1)
        viewDataBinding.devicesContainerView.layoutManager = gridLayoutManager

    }


    private fun generateSelectionTracker(deviceItemAdapter: DeviceItemAdapter): SelectionTracker<Device> {

        val selectionTracker = SelectionTracker.Builder(
            "deviceItemTracker",
            viewDataBinding.devicesContainerView,
            DeviceItemKeyProvider(deviceItemAdapter),
            DeviceItemLookup(viewDataBinding.devicesContainerView),
            StorageStrategy.createParcelableStorage(Device::class.java)
        )
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        selectionTracker.addObserver(object : SelectionTracker.SelectionObserver<Device>() {

            override fun onSelectionChanged() {
                super.onSelectionChanged()

                selectionTracker.let {
                    val selectedItems = it.selection.toList()
                    viewModel.addUserSelectedDevices(selectedItems)

                    if (selectedItems.isEmpty()) {
                        actionMode?.finish()
                    } else {

                        if (actionMode == null) {
                            val requireActivity: AppCompatActivity =
                                requireActivity() as AppCompatActivity

                            actionMode =
                                requireActivity.startSupportActionMode(this@DevicesFragment)
                        }

                        actionMode?.title = "${selectedItems.size}"
                    }
                }
            }
        })


        return selectionTracker
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        Timber.d("onActivityCreated initialized")
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
        Timber.d("onActivityCreated finished")
    }


    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {

        mode?.let {
            it.menuInflater.inflate(R.menu.action_mode_menu, menu)
            return true
        }

        return false
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        Timber.d("Action mode - Destroy")
        val deviceItemAdapter = viewDataBinding.devicesContainerView.adapter as DeviceItemAdapter
        deviceItemAdapter.selectionTracker?.clearSelection()

        actionMode = null

        (requireActivity() as AppCompatActivity).supportActionBar?.show()

        viewModel.clearUserSelectedDevices()

        deviceItemAdapter.notifyDataSetChanged()
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.action_mode_delete_view -> {

                Timber.d("Action mode - Delete clicked")

                val confirmDialog = AlertDialog.Builder(requireContext())

                confirmDialog.apply {
                    setTitle(R.string.generic_text_confirmation_question)
                    setMessage(R.string.devices_dialog_confirm_delete_body)

                    setPositiveButton(R.string.generic_text_yes) { _, _ ->
                        Timber.d("Action mode - Removing selected devices")
                        val numberOfDevices = viewModel.disconnectUserSelectedDevices()

                        Toast.makeText(
                            requireContext(),
                            resources.getQuantityString(
                                R.plurals.devices_dialog_confirm_delete_reply,
                                numberOfDevices, numberOfDevices
                            ),
                            Toast.LENGTH_SHORT
                        ).show()

                        actionMode?.finish()
                    }

                    setNegativeButton(R.string.generic_text_no) { _, _ ->
                        Timber.d("Action mode - Reverted delete action")
                        // We just dismiss the alert and do nothing
                    }

                    create()
                }.show()
            }
        }

        return true
    }

    override fun onConnectDevicesDialogConfirm(
        dialog: DialogFragment,
        bluetoothDevice: BluetoothDevice
    ) {
        viewModel.connectSelectedDevice(bluetoothDevice)
        viewDataBinding.devicesContainerView.smoothScrollToPosition(0)
    }

}