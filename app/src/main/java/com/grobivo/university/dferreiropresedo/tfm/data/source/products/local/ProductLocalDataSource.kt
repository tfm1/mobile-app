package com.grobivo.university.dferreiropresedo.tfm.data.source.products.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ProductLocalDataSource @Inject internal constructor(
    private val productDao: ProductDao
) : ProductDatabaseSource {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO


    override suspend fun getAllProductsToScan(): List<Product> = withContext(ioDispatcher) {
        return@withContext productDao.getProductsToScan().parseEntityToProduct()
    }

    override suspend fun getAllProductsSkipping(productsToSkip: List<Product>): List<Product> =
        withContext(ioDispatcher) {
            val productsIds = productsToSkip.map { it.productId }
            return@withContext productDao.getProductsSkipping(productsIds).parseEntityToProduct()
        }


    override suspend fun getAllDeviceProducts(
        deviceUUID: String, userId: String
    ): List<Register> =
        withContext(ioDispatcher) {
            return@withContext productDao.getAllDeviceProductsInfo(userId, deviceUUID)
                .parseEntityToRegister()
        }

    override suspend fun getStoredProductsByTypeId(
        deviceUUID: String, userInternalId: String, productTypeId: Long
    ): List<Register> = withContext(ioDispatcher) {
        return@withContext productDao.getStoredProductsByTypeId(
            userInternalId, deviceUUID, productTypeId
        ).parseEntityToRegister()
    }

    override suspend fun getStoredProductsType(
        deviceUUID: String, userInternalId: String
    ): LiveData<List<ProductType>> = withContext(ioDispatcher) {
        return@withContext Transformations.map(
            productDao.getStoredProductsType(deviceUUID, userInternalId)
        ) { it.asDomainModel() }
    }
}