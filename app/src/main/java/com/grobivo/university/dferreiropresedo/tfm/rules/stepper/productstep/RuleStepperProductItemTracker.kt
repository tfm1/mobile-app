package com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

class RuleStepperProductItemKeyProvider(private val ruleStepperProductAdapter: RuleStepperProductAdapter) :
    ItemKeyProvider<Register>(
        SCOPE_CACHED
    ) {

    override fun getKey(position: Int): Register? =
        ruleStepperProductAdapter.getItemByPosition(position)

    override fun getPosition(key: Register): Int =
        ruleStepperProductAdapter.getItemPosition(key.product.productId)


}

class RuleStepperProductItemLookup(private val recyclerView: RecyclerView) :
    ItemDetailsLookup<Register>() {

    override fun getItemDetails(e: MotionEvent): ItemDetails<Register>? {
        val viewSelected = recyclerView.findChildViewUnder(e.x, e.y)

        if (viewSelected != null && recyclerView.getChildViewHolder(viewSelected) is RuleStepperProductAdapter.RuleStepperProductViewHolder) {
            val viewSelectedHolder =
                recyclerView.getChildViewHolder(viewSelected) as RuleStepperProductAdapter.RuleStepperProductViewHolder

            return viewSelectedHolder.getItemDetails()
        }

        return null
    }


}