package com.grobivo.university.dferreiropresedo.tfm.data.source.devices

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.remote.DeviceRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class DefaultDevicesRepository @Inject constructor(
    private val deviceRemoteDataSource: DeviceRemoteSource,
    private val deviceLocalDataSource: DeviceDatabaseSource,
    private val sessionManagement: SessionManagement
) : DevicesRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO


    override suspend fun getAllUsersFromDevice(deviceUUID: String): ResultWrapper<List<User>> =
        safeApiCall(ioDispatcher) {
            Timber.d("Getting all the connected users of a device")
            deviceRemoteDataSource.retrieveAllUsersOfDevice(deviceUUID)
        }

    override suspend fun getAllPossibleUsers(usersAlreadyAdded: List<User>): ResultWrapper<List<User>> =
        safeApiCall(ioDispatcher) {
            Timber.d("Getting all the possible users to add to a device")
            val ids = usersAlreadyAdded.map { it.internalId }
            deviceRemoteDataSource.retrievePossibleUsersToAddToDevice(ids)
        }

    override suspend fun addUsersToSelectedDevice(
        users: List<User>, deviceUUID: String
    ): ResultWrapper<Unit> =
        safeApiCall(ioDispatcher) {
            Timber.d("Adding the selected users to the device")
            val usersEmail = users.map { it.email }
            deviceRemoteDataSource.addUsersToADevice(usersEmail, deviceUUID)
        }

    override suspend fun removeUsersToSelectedDevice(
        users: List<User>, deviceUUID: String
    ): ResultWrapper<Unit> = safeApiCall(ioDispatcher) {
        Timber.d("Removing users from the selected device")
        deviceRemoteDataSource.removeUsersFromDevice(users, deviceUUID)
    }

    override suspend fun getAllDevicesFromUser(): LiveData<List<Device>> =
        withContext(ioDispatcher) {
            Timber.d("Getting all devices from user.")
            deviceLocalDataSource.getAllDevicesFromUser(sessionManagement.userInternalId())
        }

    override suspend fun connectDeviceToUser(device: Device) = withContext(ioDispatcher) {
        Timber.d("Connecting the device %s to the user.", device)

        val resultWrapper = safeApiCall(ioDispatcher) {
            deviceRemoteDataSource.connectDevice(device, sessionManagement.userInternalId())
        }
        when (resultWrapper) {
            is ResultWrapper.Success -> {
                // it is already inserted on the remote call
            }
            else -> deviceLocalDataSource.connectDevice(device, sessionManagement.userInternalId())
        }
    }

    override suspend fun disconnectDevices(devices: List<Device>) = withContext(ioDispatcher) {
        Timber.d("Disconnecting the devices %s to the user.", devices)
        deviceLocalDataSource.disconnectDevices(devices, sessionManagement.userInternalId())
        deviceRemoteDataSource.disconnectDevices(devices, sessionManagement.userInternalId())
    }

    override suspend fun cleanUserLoggedOutInformation(userId: String) = withContext(ioDispatcher) {
        deviceLocalDataSource.cleanUserLoggedOutInformation(userId)
    }

}