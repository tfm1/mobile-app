package com.grobivo.university.dferreiropresedo.tfm.users.addusersdialog

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.domain.UserSelected
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDialogAddUsersToDeviceBinding

class AddUsersToDeviceDialog(
    private val usersRetrieved: LiveData<List<User>>,
    private val listener: AddUsersToDeviceListener
) : DialogFragment() {

    private lateinit var addUsersAdapter: AddUsersAdapter

    interface AddUsersToDeviceListener {
        fun onUsersSelectedToAdd(usersSelected: List<UserSelected>)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val binding: CustomDialogAddUsersToDeviceBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.custom_dialog_add_users_to_device, null, false
        )

        addUsersAdapter = AddUsersAdapter()
        binding.userListSelect.adapter = addUsersAdapter

        val gridLayoutManager = GridLayoutManager(activity, 1)
        binding.userListSelect.layoutManager = gridLayoutManager

        usersRetrieved.observe(this) {
            binding.progressBarSearchingUsers.visibility = View.GONE
            val usersList = it.map { user -> UserSelected(user, false) }.toList()
            if (usersList.isNullOrEmpty()) {
                binding.noUsersFoundMessage.visibility = View.VISIBLE
            } else {
                binding.noUsersFoundMessage.visibility = View.GONE
            }

            addUsersAdapter.submitList(usersList)
        }


        return AlertDialog.Builder(activity)
            .setTitle(R.string.user_management_dialog_possible_users_title)
            .setMessage(R.string.user_management_dialog_possible_users_message)
            .setPositiveButton(R.string.generic_text_ok) { _, _ ->
                listener.onUsersSelectedToAdd(addUsersAdapter.currentList)
            }
            .setNegativeButton(R.string.generic_text_cancel) { _, _ ->
                // Do nothing
            }
            .setView(binding.root)
            .create()
    }

    override fun onDismiss(dialog: DialogInterface) {
        usersRetrieved.removeObservers(this)
        super.onDismiss(dialog)
    }

}