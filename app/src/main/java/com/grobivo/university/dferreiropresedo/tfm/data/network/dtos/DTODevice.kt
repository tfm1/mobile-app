package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.squareup.moshi.Json

data class DTODevice(
    @Json(name = "uuid") val UUID: String,
    @Json(name = "name") val name: String
)

fun DTODevice.asDomainModel(): Device = Device(UUID, name)

