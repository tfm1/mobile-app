package com.grobivo.university.dferreiropresedo.tfm.rules.stepper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentRuleStepperBinding
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep.RuleStepperProduct
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.quantitystep.RuleStepperQuantity
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.WithFragmentBindings
import javax.inject.Inject

@AndroidEntryPoint
@WithFragmentBindings
class RuleStepperFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentRuleStepperBinding

    private val ruleStepperViewModel: RuleStepperViewModel by activityViewModels()

    @Inject
    lateinit var ruleStepperProduct: RuleStepperProduct

    @Inject
    lateinit var ruleStepperQuantity: RuleStepperQuantity

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_rule_stepper, container, false)

        val ruleStepperAdapter = RuleStepperAdapter(
            childFragmentManager, requireContext(), ruleStepperProduct, ruleStepperQuantity
        )

        viewDataBinding.ruleStepperLayout.setAdapter(ruleStepperAdapter, 0)
        viewDataBinding.ruleStepperLayout.setListener(configureStepperListener())

        ruleStepperProduct.stepperLayout = viewDataBinding.ruleStepperLayout

        return viewDataBinding.root
    }

    private fun configureStepperListener(): StepperLayout.StepperListener =
        object : StepperLayout.StepperListener {
            override fun onCompleted(completeButton: View?) {
                ruleStepperViewModel.createRule(
                    ruleStepperViewModel.productSelected!!.product,
                    ruleStepperViewModel.quantitySelected
                )
                ruleStepperViewModel.cleanData()
                findNavController().navigate(RuleStepperFragmentDirections.actionRuleStepperFragmentToRulesFragment())
            }

            override fun onError(verificationError: VerificationError?) {
                // Do nothing
            }

            override fun onStepSelected(newStepPosition: Int) {
                // Do nothing
            }

            override fun onReturn() {
                // Do nothing
            }

        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }
}