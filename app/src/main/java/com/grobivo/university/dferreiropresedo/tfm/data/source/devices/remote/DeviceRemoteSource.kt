package com.grobivo.university.dferreiropresedo.tfm.data.source.devices.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User

interface DeviceRemoteSource {

    suspend fun connectDevice(deviceToConnect: Device, userId: String)

    suspend fun disconnectDevices(devices: List<Device>, userInternalId: String)

    suspend fun retrievePossibleUsersToAddToDevice(usersAlreadyAdded: List<String>): List<User>

    suspend fun retrieveAllUsersOfDevice(deviceUUID: String): List<User>

    suspend fun addUsersToADevice(users: List<String>, deviceUUID: String)

    suspend fun removeUsersFromDevice(users: List<User>, deviceUUID: String)

}