package com.grobivo.university.dferreiropresedo.tfm.rules.stepper.quantitystep

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.custom.DoublePickerDialog
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentRuleQuantityStepBinding
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.RuleStepperViewModel
import com.grobivo.university.dferreiropresedo.tfm.util.productQuantity
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import javax.inject.Inject

class RuleStepperQuantity @Inject constructor() : Fragment(), Step {

    private lateinit var viewDataBinding: FragmentRuleQuantityStepBinding

    private val ruleStepperViewModel: RuleStepperViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_rule_quantity_step, container, false
        )
        configureListeners()

        return viewDataBinding.root
    }

    override fun verifyStep(): VerificationError? = null

    override fun onSelected() {
        productQuantity(viewDataBinding.ruleStepperQuantity, ruleStepperViewModel.quantitySelected)
        viewDataBinding.ruleStepperQuantityMeasure.text =
            ruleStepperViewModel.productSelected?.product?.productType?.measure
    }

    private fun configureListeners() {
        viewDataBinding.ruleStepperQuantity.setOnClickListener {
            ruleStepperViewModel.productSelected?.product?.productType?.measure?.let { measure ->
                val doublePicker =
                    DoublePickerDialog(ruleStepperViewModel.quantitySelected, measure)
                doublePicker
                    .show(childFragmentManager, DoublePickerDialog::class.qualifiedName)
                doublePicker.valueSelected.observe(viewLifecycleOwner) { amount ->
                    viewDataBinding.ruleStepperQuantity.text = amount.toString()
                    ruleStepperViewModel.quantitySelected = amount
                }
            }
        }
    }

    override fun onError(error: VerificationError) {
        TODO("Not yet implemented")
    }
}