package com.grobivo.university.dferreiropresedo.tfm.session

import android.content.Context
import androidx.core.content.edit
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Inventory
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import timber.log.Timber
import javax.inject.Inject

class SessionManagement @Inject constructor(context: Context) {

    private val sharedPreferences = context.getSharedPreferences("Grobivo", Context.MODE_PRIVATE)

    /** Set of keys used to manage user session in SharedPreferences*/
    private val isLoggedIn = Pair("isLoggedIn", false)
    private val userInternalIdKey = Pair("userInternalId", "")
    private val userAccessTokenKey = Pair("accessToken", "")
    private val usernameKey = Pair("username", "")
    private val emailKey = Pair("email", "")
    private val isAdminKey = Pair("isAdmin", false)
    private val userPhoto = Pair("userPhoto", null)

    /** Set of keys used to manage device selected*/
    private val deviceUUIDSelected = Pair("deviceUUIDSelected", null)
    private val deviceNameSelected = Pair("deviceNameSelected", null)
    private val inventorySelectedInternalId = Pair("inventorySelectedInternalId", -1L)
    private val inventorySelectedSharedId = Pair("inventorySelectedSharedId", -1L)

    fun loginUser(userData: User, accessToken: String) {

        Timber.d("Creating the session for the user...")

        sharedPreferences.edit {
            putBoolean(isLoggedIn.first, true)
            putString(userInternalIdKey.first, userData.internalId)
            putString(userAccessTokenKey.first, accessToken)
            putString(usernameKey.first, userData.name)
            putString(emailKey.first, userData.email)
            putBoolean(isAdminKey.first, userData.isAdmin)
            putString(userPhoto.first, userData.imageUrl)
            apply()
        }

        Timber.d("Session created")
    }


    fun logoutUser() {
        sharedPreferences.edit {
            clear()
            apply()
        }
        Timber.d("User session cleared")
    }


    fun isUserLoggedIn(): Boolean {
        return sharedPreferences.getBoolean(isLoggedIn.first, isLoggedIn.second)
    }

    fun userInternalId(): String {
        return sharedPreferences.getString(userInternalIdKey.first, userInternalIdKey.second)!!
    }

    fun userAccessToken(): String {
        Timber.d("Retrieving the user access token")
        return sharedPreferences.getString(userAccessTokenKey.first, userAccessTokenKey.second)!!
    }

    fun getUsernameLogged(): String {
        return sharedPreferences.getString(usernameKey.first, usernameKey.second)!!
    }

    fun getUserPhotoUrl(): String? {
        return sharedPreferences.getString(userPhoto.first, userPhoto.second)
    }

    fun isAdminUser(): Boolean {
        return sharedPreferences.getBoolean(isAdminKey.first, isAdminKey.second)
    }

    fun updateUserAdmin(isAdmin: Boolean) {
        sharedPreferences.edit {
            putBoolean(isAdminKey.first, isAdmin)
        }
    }

    fun updateAccessToken(accessToken: String) {
        Timber.d("Updating the user access token")
        sharedPreferences.edit {
            putString(userAccessTokenKey.first, accessToken)
            apply()
        }
    }

    fun storeDevice(device: Device) {

        sharedPreferences.edit {
            putString(deviceUUIDSelected.first, device.UUID)
            putString(deviceNameSelected.first, device.name)
            apply()
        }

        Timber.d("Stored the device %s in the session", device.name)
    }

    fun storeInventory(inventory: Inventory) {

        sharedPreferences.edit {
            putLong(inventorySelectedInternalId.first, inventory.inventoryId)
            inventory.sharedInventoryId?.let { putLong(inventorySelectedSharedId.first, it) }
            apply()
        }

        Timber.d("Stored the inventory of the device %s in the session", inventory.deviceUUID)
    }

    fun getStoredDeviceUUID(): String? {
        return sharedPreferences.getString(deviceUUIDSelected.first, deviceUUIDSelected.second)
    }

    fun getStoredDeviceName(): String? {
        return sharedPreferences.getString(deviceNameSelected.first, deviceNameSelected.second)
    }

    fun getStoredInventoryInternalId(): Long {
        return sharedPreferences.getLong(
            inventorySelectedInternalId.first,
            inventorySelectedInternalId.second
        )
    }

    fun getStoredInventorySharedId(): Long {
        return sharedPreferences.getLong(
            inventorySelectedSharedId.first,
            inventorySelectedSharedId.second
        )
    }

    fun cleanDevice() {
        sharedPreferences.edit {
            putString(deviceUUIDSelected.first, null)
            putString(deviceNameSelected.first, null)
            putLong(inventorySelectedInternalId.first, inventorySelectedInternalId.second)
            putLong(inventorySelectedSharedId.first, inventorySelectedSharedId.second)
            apply()
        }
    }


}