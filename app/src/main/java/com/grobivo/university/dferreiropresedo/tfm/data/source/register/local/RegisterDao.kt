package com.grobivo.university.dferreiropresedo.tfm.data.source.register.local

import androidx.room.*
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityRegister
import com.grobivo.university.dferreiropresedo.tfm.data.domain.RegisterMapping

@Dao
interface RegisterDao {

    @Insert
    suspend fun insertRegister(register: EntityRegister): Long

    @Insert
    suspend fun insertRegister(registers: List<EntityRegister>)

    @Delete
    suspend fun deleteRegister(registersToDelete: List<EntityRegister>)

    @Update(onConflict = OnConflictStrategy.IGNORE)
    suspend fun updateRegister(registersToUpdate: List<EntityRegister>)

    @Query("""SELECT r.* FROM register r WHERE r.inventory_id = :inventoryId""")
    suspend fun getAllInventoryRegisters(inventoryId: Long): List<EntityRegister>

    @Query("""SELECT r.* FROM register r WHERE r.inventory_id = :inventoryId AND r.product_id IN (:productIds)""")
    suspend fun getAllInventoryRegistersByProductIds(
        inventoryId: Long,
        productIds: List<Long>
    ): List<EntityRegister>

    @Query(
        """SELECT r.register_id as registerId, r.shared_register_id as  sharedRegisterId, r.inventory_id as inventoryId,
             r.product_id as productId, p.name as name, p.ticket_code as ticketCode, r.quantity as quantity,
             pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure
        FROM register r JOIN product p ON r.product_id = p.product_id
            JOIN product_type pt ON p.product_type_id = pt.product_type_id
        WHERE r.inventory_id = :inventoryId AND r.product_id IN (:productIds)"""
    )
    suspend fun getAllInventoryRegistersInfoByProductIds(
        inventoryId: Long,
        productIds: List<Long>
    ): List<RegisterMapping>

    @Query("""DELETE FROM register WHERE inventory_id IN (:inventoryIds)""")
    suspend fun cleanseDevicesInventoryRegisters(inventoryIds: List<Long>)

    @Transaction
    suspend fun updateUserInventoryStock(
        registersToInsert: List<EntityRegister>,
        registersToDelete: List<EntityRegister>,
        registersToUpdate: List<EntityRegister>
    ) {
        if (registersToDelete.isNotEmpty()) {
            deleteRegister(registersToDelete)
        }
        if (registersToUpdate.isNotEmpty()) {
            updateRegister(registersToUpdate)
        }
        if (registersToInsert.isNotEmpty()) {
            insertRegister(registersToInsert)
        }
    }

}