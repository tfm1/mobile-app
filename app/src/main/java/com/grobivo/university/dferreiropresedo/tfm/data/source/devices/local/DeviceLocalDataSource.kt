package com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDao
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class DeviceLocalDataSource @Inject internal constructor(
    private val deviceDao: DeviceDao,
    private val userDao: UserDao,
    private val inventoryDao: InventoryDao,
    private val registerDao: RegisterDao,
    private val ruleDao: RuleDao
) : DeviceDatabaseSource {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getAllDevicesFromUser(userId: String): LiveData<List<Device>> =
        withContext(ioDispatcher) {
            return@withContext Transformations.map(userDao.getUserDevices(userId)) { it.asDomainModel() }
        }

    override suspend fun connectDevice(device: Device, userId: String) = withContext(ioDispatcher) {
        val userUUIDs = userDao.getUserConnectedDevices(userId).map { it.UUID }
        if (!userUUIDs.contains(device.UUID)) {
            Timber.d("Connecting to the device %s with a local inventory", device.UUID)
            deviceDao.connectDeviceToUser(device.asDeviceDatabaseModel(), userId)
            inventoryDao.createInventory(device.UUID, null)
        }

    }

    override suspend fun disconnectDevices(devices: List<Device>, userInternalId: String) =
        withContext(ioDispatcher) {
            Timber.d("Local devices disconnection from user.")
            val UUIDs = devices.map { it.UUID }
            val inventoryIds = inventoryDao.getDevicesInventoryInfo(userInternalId, UUIDs)
                .map { it.internalInventoryId }
            registerDao.cleanseDevicesInventoryRegisters(inventoryIds)
            inventoryDao.removeInventoryData(UUIDs)
            deviceDao.disconnectDevicesToUser(
                devices.asDeviceDatabaseModel(), UUIDs, userInternalId
            )
        }

    override suspend fun cleanUserLoggedOutInformation(userId: String) = withContext(ioDispatcher) {
        val devicesUUID = userDao.getUserConnectedDevices(userId).map { it.UUID }
        val inventoryIds =
            inventoryDao.getDevicesInventoryInfo(userId, devicesUUID).map { it.internalInventoryId }
        registerDao.cleanseDevicesInventoryRegisters(inventoryIds)
        ruleDao.cleanseDevicesInventoryRules(inventoryIds)
        inventoryDao.removeInventoryData(devicesUUID)
        deviceDao.deleteUserDevices(devicesUUID, userId)
        deviceDao.deleteDevices(devicesUUID)
    }
}