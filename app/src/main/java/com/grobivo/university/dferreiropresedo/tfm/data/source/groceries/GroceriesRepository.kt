package com.grobivo.university.dferreiropresedo.tfm.data.source.groceries

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper

interface GroceriesRepository {

    suspend fun requestGroceriesList(): ResultWrapper<List<Register>>

}