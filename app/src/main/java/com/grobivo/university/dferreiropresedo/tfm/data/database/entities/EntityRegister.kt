package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

@Entity(
    tableName = "register",
    indices = [
        Index(value = ["inventory_id", "product_id"], unique = true)
    ]
)
class EntityRegister constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "register_id") val registerId: Long,
    @ColumnInfo(name = "shared_register_id") val sharedRegisterId: Long?,
    @ColumnInfo(name = "quantity") val quantity: Float,
    @ColumnInfo(name = "inventory_id") val inventoryId: Long,
    @ColumnInfo(name = "product_id") val productId: Long
)

fun EntityRegister.asDomainModel(product: Product) =
    Register(registerId, sharedRegisterId, quantity, inventoryId, product)