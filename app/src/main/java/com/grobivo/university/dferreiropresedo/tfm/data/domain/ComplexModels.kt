package com.grobivo.university.dferreiropresedo.tfm.data.domain

data class RegisterMapping(
    val registerId: Long,
    val sharedRegisterId: Long?,
    val inventoryId: Long,
    val productId: Long,
    val name: String,
    val ticketCode: String,
    val quantity: Float,
    val productTypeId: Long,
    val productTypeName: String,
    val productTypeMeasure: String
)

fun RegisterMapping.parseEntityToRegister(): Register = Register(
    registerId, sharedRegisterId, quantity, inventoryId,
    Product(
        productId, name, ticketCode, ProductType(productTypeId, productTypeName, productTypeMeasure)
    )
)

fun List<RegisterMapping>.parseEntityToRegister(): List<Register> =
    map { it.parseEntityToRegister() }

data class ProductMapping(
    val productId: Long,
    val name: String,
    val ticketCode: String,
    val productTypeId: Long,
    val productTypeName: String,
    val productTypeMeasure: String
)

fun ProductMapping.parseEntityToProduct(): Product = Product(
    productId,
    name,
    ticketCode,
    ProductType(productTypeId, productTypeName, productTypeMeasure)
)

fun List<ProductMapping>.parseEntityToProduct(): List<Product> = map { it.parseEntityToProduct() }


data class DeviceInventoryMapping(
    val deviceUUID: String,
    val deviceName: String,
    val userInternalId: String,
    val internalInventoryId: Long,
    val sharedInventoryId: Long?
)

fun DeviceInventoryMapping.asInventoryInfo(): Inventory =
    Inventory(deviceUUID, sharedInventoryId, internalInventoryId)


data class InventoryInfo(
    val device: Device,
    val inventory: Inventory,
    val registers: List<Register>,
    val rules: List<Rule>
)


data class UserSelected(
    val user: User,
    var selected: Boolean = false
)

data class ProductRuleMapping(
    val ruleId: Long,
    val sharedRuleId: Long,
    val inventoryId: Long,
    val productId: Long,
    val quantity: Float,
    val name: String,
    val ticketCode: String,
    val productTypeId: Long,
    val productTypeName: String,
    val productTypeMeasure: String
)

fun ProductRuleMapping.parseProductRuleMapping(): Rule = Rule(
    ruleId, sharedRuleId, quantity, inventoryId, Product(
        productId, name, ticketCode, ProductType(productTypeId, productTypeName, productTypeMeasure)
    )
)

fun List<ProductRuleMapping>.parseProductRuleMapping(): List<Rule> =
    map { it.parseProductRuleMapping() }