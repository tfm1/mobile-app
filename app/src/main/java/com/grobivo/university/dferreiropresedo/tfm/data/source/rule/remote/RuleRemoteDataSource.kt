package com.grobivo.university.dferreiropresedo.tfm.data.source.rule.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTODevice
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTODeviceRules
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import javax.inject.Inject


class RuleRemoteDataSource @Inject constructor(
    private val appService: EndpointService,
    private val sessionManagement: SessionManagement,
    private val ruleDao: RuleDao
) : RuleRemoteSource {

    override suspend fun createRule(rule: List<Rule>): List<Rule> {
        val ruleDTO = rule.map {
            Rule(
                it.ruleId, it.sharedRuleId, it.quantity,
                sessionManagement.getStoredInventorySharedId(), it.product
            )
        }.asRuleDTO()
        val device = Device(
            sessionManagement.getStoredDeviceUUID()!!,
            sessionManagement.getStoredDeviceName()!!
        )
        val deviceRulesDTO = DTODeviceRules(device.asDeviceDTO(), ruleDTO)
        return appService.createRules(deviceRulesDTO).asDomainModel()
    }

    override suspend fun removeRule(rule: List<Rule>) {
        appService.removeRules(rule.filter { it.sharedRuleId != null }.map { it.sharedRuleId!! })
    }

    override suspend fun retrieveInventoryRules(storedInventorySharedId: Long) {
        val ruleDTOList = appService.retrieveInventoryRules(storedInventorySharedId)
        val rulesList = ruleDTOList.asDomainModel()
        val rulesMappingList =
            ruleDao.retrieveInventoryRules(sessionManagement.getStoredInventoryInternalId()).value
                ?: emptyList()
        val inventoryRulesList = rulesMappingList.parseProductRuleMapping()

        val remoteProductsIds = rulesList.map { it.product.productId }
        val localProductsIds = inventoryRulesList.map { it.product.productId }
        val rulesIdToBeCreated = remoteProductsIds.minus(localProductsIds)
        val rulesToBeUpdated =
            inventoryRulesList.filter { remoteProductsIds.contains(it.product.productId) }

        val rulesToUpdate = rulesToBeUpdated.map { ruleToUpdate ->
            val remoteRule =
                rulesList.find { it.product.productId == ruleToUpdate.product.productId }!!
            return@map Rule(
                ruleToUpdate.ruleId, remoteRule.sharedRuleId, remoteRule.quantity,
                ruleToUpdate.inventoryId, ruleToUpdate.product
            )
        }

        val ruleToCreate =
            rulesList.filter { rulesIdToBeCreated.contains(it.product.productId) }.map {
                Rule(
                    0L, it.sharedRuleId, it.quantity,
                    sessionManagement.getStoredInventoryInternalId(), it.product
                )
            }

        ruleDao.createRules(ruleToCreate.asRuleDatabaseModel())
        ruleDao.updateRules(rulesToUpdate.asRuleDatabaseModel())
    }

    override suspend fun saveChanges(rules: List<Rule>) {

        val rulesWithRemoteId = rules.map {
            Rule(
                it.ruleId, it.sharedRuleId, it.quantity,
                sessionManagement.getStoredInventorySharedId(), it.product
            )
        }

        appService.saveRuleChanges(
            DTODeviceRules(
                DTODevice(
                    sessionManagement.getStoredDeviceUUID()!!,
                    sessionManagement.getStoredDeviceName()!!
                ), rulesWithRemoteId.asRuleDTO()
            )
        )
    }


}