package com.grobivo.university.dferreiropresedo.tfm.data.source.users.local

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.domain.asUserDatabaseModel
import javax.inject.Inject


class UserLocalDataSource @Inject internal constructor(private val userDao: UserDao) :
    UserDatabaseSource {

    override suspend fun insert(user: User) {
        val userInfo = user.asUserDatabaseModel()
        val findUserInfo = userDao.findUserInfo(userInfo.internalId)
        if (findUserInfo == null) {
            userDao.insert(userInfo)
        }
    }

}