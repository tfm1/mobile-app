package com.grobivo.university.dferreiropresedo.tfm.data.source.devices

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper

interface DevicesRepository {

    /**
     * Retrieve all the users of a device
     */
    suspend fun getAllUsersFromDevice(deviceUUID: String): ResultWrapper<List<User>>

    /**
     * Retrieve all the possible users to connect to a device
     */
    suspend fun getAllPossibleUsers(usersAlreadyAdded: List<User>): ResultWrapper<List<User>>


    /**
     * Given a device UUID, it adds the specified users to it.
     */
    suspend fun addUsersToSelectedDevice(users: List<User>, deviceUUID: String): ResultWrapper<Unit>

    /**
     * Given a device UUID, it removes the specified users from it
     */
    suspend fun removeUsersToSelectedDevice(
        users: List<User>, deviceUUID: String
    ): ResultWrapper<Unit>

    /**
     * Retrieve the locally stored devices of the current user.
     */
    suspend fun getAllDevicesFromUser(): LiveData<List<Device>>

    /**
     * Connect the device locally to the user and updates its inventory from the remote.
     * @param device The device to be connected.
     */
    suspend fun connectDeviceToUser(device: Device)

    /**
     * Given a bunch of devices, they are locally disconnected from the user.
     *
     * @param devices Devices to be disconnected
     */
    suspend fun disconnectDevices(devices: List<Device>)

    /**
     * Given a user, it eliminates all the device information related to it
     *
     */
    suspend fun cleanUserLoggedOutInformation(userId: String)

}