package com.grobivo.university.dferreiropresedo.tfm.devices

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device

class DeviceItemKeyProvider(private val deviceItemAdapter: DeviceItemAdapter) :
    ItemKeyProvider<Device>(
        SCOPE_CACHED
    ) {

    override fun getKey(position: Int): Device? = deviceItemAdapter.getItemByPosition(position)

    override fun getPosition(key: Device): Int = deviceItemAdapter.getItemPosition(key.UUID)

}

class DeviceItemLookup(private val recyclerView: RecyclerView) : ItemDetailsLookup<Device>() {

    override fun getItemDetails(e: MotionEvent): ItemDetails<Device>? {
        val viewSelected = recyclerView.findChildViewUnder(e.x, e.y)

        if (viewSelected != null) {
            val viewSelectedHolder =
                recyclerView.getChildViewHolder(viewSelected) as DeviceItemAdapter.ViewHolder

            return viewSelectedHolder.getItemDetails()
        }

        return null
    }

}