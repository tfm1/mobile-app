package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Inventory

@Entity(
    tableName = "inventory",
    indices = [
        Index(value = ["device_UUID"], unique = true)
    ]
)
data class EntityInventory constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "inventory_id") var inventoryId: Long,
    @ColumnInfo(name = "shared_inventory_id") var sharedInventoryId: Long?,
    @ColumnInfo(name = "device_UUID") var deviceUUID: String
)

fun EntityInventory.asDomainModel() = Inventory(deviceUUID, sharedInventoryId, inventoryId)
fun List<EntityInventory>.asDomainModel(): List<Inventory> = map { it.asDomainModel() }