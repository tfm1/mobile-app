package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityInventory
import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping

@Dao
interface InventoryDao {

    @Query("INSERT INTO inventory(device_UUID, shared_inventory_id) VALUES (:deviceUUID, :sharedInventoryId)")
    suspend fun createInventory(deviceUUID: String, sharedInventoryId: Long?): Long

    @Insert
    suspend fun insertInventory(inventory: EntityInventory): Long

    @Query(
        """
        SELECT d.UUID as deviceUUID, d.name as deviceName, du.user_internal_id as userInternalId, i.inventory_id as internalInventoryId, i.shared_inventory_id as sharedInventoryId
        FROM device d JOIN inventory i ON d.UUID LIKE i.device_UUID AND d.UUID LIKE :deviceUUID
            JOIN device_users du ON du.device_UUID LIKE i.device_UUID AND du.user_internal_id LIKE :userInternalId
    """
    )
    suspend fun getDeviceInventoryInfo(
        userInternalId: String, deviceUUID: String
    ): DeviceInventoryMapping?

    @Query(
        """
        SELECT d.UUID as deviceUUID, d.name as deviceName, du.user_internal_id as userInternalId, i.inventory_id as internalInventoryId, i.shared_inventory_id as sharedInventoryId
        FROM device d JOIN inventory i ON d.UUID LIKE i.device_UUID
            JOIN device_users du ON du.device_UUID LIKE i.device_UUID AND du.user_internal_id LIKE :userInternalId
        WHERE d.UUID IN (:deviceUUIDs)
    """
    )
    suspend fun getDevicesInventoryInfo(
        userInternalId: String,
        deviceUUIDs: List<String>
    ): List<DeviceInventoryMapping>

    @Query("""DELETE FROM inventory WHERE device_UUID IN (:deviceUUIDs)""")
    suspend fun removeInventoryData(deviceUUIDs: List<String>)
}