package com.grobivo.university.dferreiropresedo.tfm.data.source.register

import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDatabaseSource
import javax.inject.Inject

class DefaultRegisterRepository @Inject constructor(
    private val localRegisterDataSource: RegisterDatabaseSource
) : RegisterRepository {

}