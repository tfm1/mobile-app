package com.grobivo.university.dferreiropresedo.tfm.data.source.devices.remote

import android.content.Context
import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import dagger.hilt.android.qualifiers.ApplicationContext
import timber.log.Timber
import javax.inject.Inject

class DeviceRemoteDataSource @Inject constructor(
    @ApplicationContext val context: Context,
    private val endpointService: EndpointService,
    private val deviceDao: DeviceDao,
    private val registerDao: RegisterDao,
    private val ruleDao: RuleDao,
    private val inventoryDao: InventoryDao
) : DeviceRemoteSource {

    override suspend fun connectDevice(deviceToConnect: Device, userId: String) {
        Timber.d("Connecting the device %s on the remote server", deviceToConnect)
        val (device, inventory, registers, rules) = endpointService.connectUserDevice(
            deviceToConnect.asDeviceDTO()
        ).asDomainModel()

        Timber.d("Storing the data retrieved from the remote source.")
        deviceDao.connectDeviceToUser(device.asDeviceDatabaseModel(), userId)
        val inventoryCreated = inventoryDao.insertInventory(inventory.asInventoryDatabaseModel())
        val registersInventory = registers.map { remoteRegister: Register ->
            Register(
                remoteRegister.registerId, remoteRegister.sharedRegisterId,
                remoteRegister.quantity, inventoryCreated, remoteRegister.product
            )
        }.asRegisterDatabaseModel()
        val rulesInventory = rules.map { remoteRule: Rule ->
            Rule(
                remoteRule.ruleId, remoteRule.sharedRuleId, remoteRule.quantity,
                inventoryCreated, remoteRule.product
            )
        }.asRuleDatabaseModel()
        registerDao.insertRegister(registersInventory)
        ruleDao.createRules(rulesInventory)
    }

    override suspend fun disconnectDevices(devices: List<Device>, userInternalId: String) {
        Timber.d("Remote devices disconnection from user.")
        endpointService.disconnectUserDevice(devices.asDeviceDTO())
    }

    override suspend fun retrievePossibleUsersToAddToDevice(usersAlreadyAdded: List<String>): List<User> {
        Timber.d("Retrieving all the possible users to add from remote")
        return endpointService.retrieveAllPossibleUsers(usersAlreadyAdded).asDomainModel()
    }

    override suspend fun retrieveAllUsersOfDevice(deviceUUID: String): List<User> {
        Timber.d("Retrieving all the users of a device")
        return endpointService.getAllDeviceUsers(deviceUUID).asDomainModel()
    }

    override suspend fun addUsersToADevice(users: List<String>, deviceUUID: String) {
        endpointService.addUsersToDevice(deviceUUID, users)
    }

    override suspend fun removeUsersFromDevice(users: List<User>, deviceUUID: String) {
        endpointService.removeUsersFromDevice(deviceUUID, users.asUserDTO())
    }
}