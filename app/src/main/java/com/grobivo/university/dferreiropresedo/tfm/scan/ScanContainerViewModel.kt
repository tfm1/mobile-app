package com.grobivo.university.dferreiropresedo.tfm.scan

import android.content.Context
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.InventoryRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.ProductRepository
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.IOException
import javax.inject.Inject

@HiltViewModel
class ScanContainerViewModel @Inject constructor(
    private val sessionManagement: SessionManagement,
    private val productRepository: ProductRepository,
    private val inventoryRepository: InventoryRepository
) : ViewModel() {

    var photoResult: File? = null
    private val recognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)

    /** The products shown as scanned and that will be saved */
    private val _scannedProducts = MutableLiveData<List<Register>>(ArrayList())
    val scannedProducts: LiveData<List<Register>>
        get() = _scannedProducts

    /** Indicates when the scanning process of the image is finished */
    private val _flagScannedProductsFinished = MutableLiveData(false)
    val flagScannedProductsFinished: LiveData<Boolean>
        get() = _flagScannedProductsFinished

    /** The products info so as to recognize the info on the ticket */
    private val _productsToScan = ArrayList<Product>()

    /** Products that can be manually registered */
    private val _pendingProductsToBeScanned = MutableLiveData<List<Product>>()
    val pendingProductsToBeScanned: LiveData<List<Product>>
        get() = _pendingProductsToBeScanned


    fun flagScannedProductsFinishedActivate() = _flagScannedProductsFinished.postValue(true)
    fun flagScannedProductsFinishedDeactivate() = _flagScannedProductsFinished.postValue(false)

    fun cleanData() {
        _scannedProducts.postValue(emptyList())
        _flagScannedProductsFinished.value = false
        _productsToScan.clear()
        _pendingProductsToBeScanned.value = emptyList()
    }

    fun manuallyRemoveProducts(registers: List<Register>) {
        val productIds = registers.map { it.product.productId }
        val listOfRegisters = _scannedProducts.value ?: emptyList()

        val filteredList = listOfRegisters.filter { !productIds.contains(it.product.productId) }

        _scannedProducts.postValue(filteredList)
    }

    fun addProductsManuallyScanned(productsSelected: List<Product>?) {

        val currentList = _scannedProducts.value ?: emptyList()
        val modifiableList = ArrayList(currentList)

        val newRegisters = productsSelected?.map { product ->
            Register(
                0L, 0L, 1F,
                sessionManagement.getStoredInventoryInternalId(),
                product
            )
        } ?: emptyList()

        modifiableList.addAll(newRegisters)
        _scannedProducts.postValue(modifiableList)
    }

    fun completeScanningProcess() {
        viewModelScope.launch {
            val registersToBeAdded = _scannedProducts.value ?: emptyList()
            inventoryRepository.addScannedRegisters(registersToBeAdded)
        }
    }

    fun retrieveProductsToScan() {
        viewModelScope.launch {
            _productsToScan.addAll(productRepository.getAllProductsToScan())
        }
    }

    fun retrievePendingProductsToBeScanned(productsToSkip: List<Product>) {
        viewModelScope.launch {
            _pendingProductsToBeScanned.postValue(
                productRepository.getAllProductsSkipping(
                    productsToSkip
                )
            )
        }
    }

    fun scanProducts(context: Context) {
        Timber.d("Starting to scan the products from the ticket image...")

        if (photoResult == null) {
            Timber.d("There is no image to scan.")
            return
        }

        val image: InputImage
        try {
            image = InputImage.fromFilePath(context, photoResult!!.toUri())
        } catch (e: IOException) {
            Timber.w(e, "There was a problem trying to use the photo captured.")
            return
        }

        recognizer.process(image)
            .addOnSuccessListener { visionText ->
                extractProductsFromTicket(visionText)
            }
            .addOnFailureListener { e ->
                Timber.w(e, "There was an error processing the photo captured.")
            }
    }

    private fun extractProductsFromTicket(visionText: Text) {
        for (block in visionText.textBlocks) {
            for (line in block.lines) {
                val text = line.text

                val productFound = _productsToScan.find { text.contains(it.ticketCode) }
                productFound?.let {
                    addNewProductRegistered(
                        Register(
                            0L, 0L, 1F,
                            sessionManagement.getStoredInventoryInternalId(),
                            it
                        )
                    )
                }
            }
        }
        flagScannedProductsFinishedActivate()
    }

    private fun addNewProductRegistered(newRegister: Register) {

        val previousValue = _scannedProducts.value
        if (previousValue != null) {
            val alreadyRegistered =
                previousValue.find { r -> r.product.productId == newRegister.product.productId }

            if (alreadyRegistered != null) {
                alreadyRegistered.quantity += 1
            }

            val arrayList = ArrayList(previousValue)
            arrayList.add(newRegister)
            _scannedProducts.value = arrayList
        }
    }
}