package com.grobivo.university.dferreiropresedo.tfm.custom

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDoublePickerBinding

class DoublePickerDialog constructor(
    private val previousValue: Float,
    private val measure: String
) : DialogFragment() {

    private lateinit var binding: CustomDoublePickerBinding

    private val _valueSelected = MutableLiveData<Float>()
    val valueSelected: LiveData<Float>
        get() = _valueSelected

    override fun onAttach(context: Context) {
        super.onAttach(context)

        binding = DataBindingUtil.inflate(
            LayoutInflater.from(context),
            R.layout.custom_double_picker,
            null,
            false
        )
        binding.measure.text = measure
        binding.separator.text = ","

        val unity = previousValue.toInt()
        val decimal = ((previousValue - unity) * 100).toInt()

        binding.unityNumberPicker.apply {
            minValue = 0
            maxValue = 99
            value = unity
        }
        binding.decimalsNumberPicker.apply {
            minValue = 0
            maxValue = 99
            value = decimal
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            return AlertDialog.Builder(context).apply {
                setTitle(R.string.product_select_amount)
                setView(binding.root)
                setPositiveButton(R.string.generic_text_ok) { _, _ ->
                    val unity = binding.unityNumberPicker.value
                    val decimals = binding.decimalsNumberPicker.value
                    val value: Float = 0F + unity + (decimals.toFloat() / 100F)
                    _valueSelected.postValue(value)
                }
                setNegativeButton(R.string.generic_text_cancel) { _, _ ->

                }

            }.create()
        } ?: throw IllegalStateException("Activity cannot be null")
    }
}