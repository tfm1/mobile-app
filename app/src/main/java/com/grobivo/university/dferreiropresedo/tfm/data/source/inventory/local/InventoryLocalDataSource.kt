package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local

import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityRegister
import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.domain.asRegisterDatabaseModel
import com.grobivo.university.dferreiropresedo.tfm.data.domain.parseEntityToRegister
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import javax.inject.Inject

class InventoryLocalDataSource @Inject internal constructor(
    private val inventoryDao: InventoryDao,
    private val registerDao: RegisterDao
) : InventoryDatabaseSource {

    override suspend fun updateInventoryInfo(newData: List<Register>, storedInventory: Long) {
        val registersToInsert = ArrayList<EntityRegister>()
        val registersToDelete = ArrayList<EntityRegister>()
        val registersToUpdate = ArrayList<EntityRegister>()
        val productsIdsToUpdate: List<Long> = newData.map { it.product.productId }
        val productsRegistered =
            registerDao.getAllInventoryRegistersByProductIds(storedInventory, productsIdsToUpdate)

        newData.map { productStocked ->
            // check if the product is already registered
            val productRegistered: EntityRegister? =
                productsRegistered.find { productRegistered ->
                    productRegistered.productId == productStocked.product.productId
                }

            // if it's not, then we create a brand new row
            if (productRegistered == null && !productStocked.quantity.equals(0F)) {
                registersToInsert.add(
                    EntityRegister(
                        0L,
                        productStocked.sharedRegisterId,
                        productStocked.quantity, storedInventory, productStocked.product.productId
                    )
                )
            } else {
                if (productStocked.quantity.equals(0F)) {
                    // if the amount selected is 0, the register is eliminated
                    productRegistered?.let { registersToDelete.add(it) }
                } else {
                    // otherwise, we update the existent one with the new amount
                    productRegistered?.let {
                        registersToUpdate.add(
                            EntityRegister(
                                it.registerId, it.sharedRegisterId, productStocked.quantity,
                                it.inventoryId, it.productId
                            )
                        )
                    }
                }
            }
        }

        registerDao.updateUserInventoryStock(
            registersToInsert, registersToDelete, registersToUpdate
        )
    }

    override suspend fun addScannedRegisters(
        newData: List<Register>,
        storedInventory: Long
    ): List<Register> {

        val entityRegisters = newData.asRegisterDatabaseModel()
        val productsIds = entityRegisters.map { it.productId }
        val productsRegistered =
            registerDao.getAllInventoryRegistersByProductIds(storedInventory, productsIds)
        val productsIdsAlreadyRegistered = productsRegistered.map { it.productId }

        // calculate which products need to be updated and which created
        val productsIdsToUpdate = productsIds.intersect(productsIdsAlreadyRegistered)
        val productIdsToAdd = productsIds.minus(productsIdsAlreadyRegistered)
        val registersToUpdate =
            productsRegistered.filter { productsIdsToUpdate.contains(it.productId) }

        val registersToAdd = entityRegisters.filter { productIdsToAdd.contains(it.productId) }
        val registersUpdated = registersToUpdate.map { register ->
            val registerFound = newData.find { it.product.productId == register.productId }!!
            EntityRegister(
                register.registerId, register.sharedRegisterId,
                register.quantity + registerFound.quantity,
                register.inventoryId, register.productId
            )
        }

        registerDao.updateUserInventoryStock(registersToAdd, emptyList(), registersUpdated)
        return registerDao.getAllInventoryRegistersInfoByProductIds(storedInventory, productsIds)
            .parseEntityToRegister()
    }

    override suspend fun getDeviceInventoryInfo(
        userId: String, deviceUUID: String
    ): DeviceInventoryMapping = inventoryDao.getDeviceInventoryInfo(userId, deviceUUID)!!
}

