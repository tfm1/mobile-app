package com.grobivo.university.dferreiropresedo.tfm.util

import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.network.AppService
import java.net.URLEncoder

@BindingAdapter("productQuantity")
fun productQuantity(view: TextView, amount: Float) {

    val formatter = if (amount % 1.0 <= 0.001) {
        "%.0f"
    } else {
        "%.2f"
    }
    view.text = formatter.format(amount)
}


@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    if (imgUrl.isNullOrEmpty()) {
        Glide.with(imgView.context)
            .load(R.drawable.ic_broken_image)
            .apply(RequestOptions().override(imgView.width, imgView.height))
            .into(imgView)
    } else {
        val uri = imgUrl.toUri()
        Glide.with(imgView.context)
            .load(uri)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
                    .override(imgView.width, imgView.height)
            )
            .into(imgView)
    }
}

@BindingAdapter("staticImageUrl")
fun bindStaticImage(imgView: ImageView, name: String) {
    val staticImageUrl =
        AppService.SERVER_URL + "images/" + URLEncoder.encode(
            name.lowercase().replace(" ", "_"),
            "UTF-8"
        ) + ".jpg"
    bindImage(imgView, staticImageUrl)
}
