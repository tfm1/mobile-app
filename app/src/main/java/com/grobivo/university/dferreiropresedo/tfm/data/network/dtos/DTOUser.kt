package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.squareup.moshi.Json

data class DTOUser(
    @Json(name = "user_id") val id: String,
    @Json(name = "name") val name: String,
    @Json(name = "email") val email: String,
    @Json(name = "admin") val admin: Boolean,
    @Json(name = "image_url") val imageUrl: String?
)

fun DTOUser.asDomainModel(imageUrl: String?): User =
    User(id, name, email, admin, imageUrl ?: this.imageUrl)

fun List<DTOUser>.asDomainModel(): List<User> = map { it.asDomainModel(null) }