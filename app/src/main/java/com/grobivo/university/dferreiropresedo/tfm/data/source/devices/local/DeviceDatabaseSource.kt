package com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device

interface DeviceDatabaseSource {

    suspend fun getAllDevicesFromUser(userId: String): LiveData<List<Device>>

    suspend fun connectDevice(device: Device, userId: String)

    suspend fun disconnectDevices(devices: List<Device>, userInternalId: String)

    suspend fun cleanUserLoggedOutInformation(userId: String)

}