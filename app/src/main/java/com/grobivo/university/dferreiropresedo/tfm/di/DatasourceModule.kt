package com.grobivo.university.dferreiropresedo.tfm.di

import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.DefaultDevicesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.DevicesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.remote.DeviceRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.remote.DeviceRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.DefaultGroceriesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.GroceriesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.remote.GroceriesRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.groceries.remote.GroceriesRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.DefaultInventoryRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.InventoryRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.remote.InventoryRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.remote.InventoryRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.DefaultProductTypeRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.ProductTypeRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local.ProductTypeDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local.ProductTypeLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.remote.ProductTypeRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.remote.ProductTypeRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.DefaultProductRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.ProductRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.local.ProductDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.local.ProductLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.remote.ProductRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.remote.ProductRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.DefaultRuleRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.RuleRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.remote.RuleRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.remote.RuleRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.DefaultSyncRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.SyncRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.local.SyncLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.local.SyncLocalSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.remote.SyncRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.remote.SyncRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.DefaultUserRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.UserRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.remote.UserRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.remote.UserRemoteSource
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
abstract class DatabaseSourceModule {

    @Singleton
    @Binds
    abstract fun bindUserDatabaseSource(local: UserLocalDataSource): UserDatabaseSource

    @Singleton
    @Binds
    abstract fun bindDevicesDatabaseSource(local: DeviceLocalDataSource): DeviceDatabaseSource

    @Singleton
    @Binds
    abstract fun bindInventoryDatabaseSource(local: InventoryLocalDataSource): InventoryDatabaseSource

    @Singleton
    @Binds
    abstract fun bindProductDatabaseSource(local: ProductLocalDataSource): ProductDatabaseSource

    @Singleton
    @Binds
    abstract fun bindProductTypeDatabaseSource(local: ProductTypeLocalDataSource): ProductTypeDatabaseSource

    @Singleton
    @Binds
    abstract fun bindRegisterDatabaseSource(local: RegisterLocalDataSource): RegisterDatabaseSource

    @Singleton
    @Binds
    abstract fun bindRuleDatabaseSource(local: RuleLocalDataSource): RuleDatabaseSource

    @Singleton
    @Binds
    abstract fun bindSyncDatabaseSource(local: SyncLocalDataSource): SyncLocalSource
}

@InstallIn(SingletonComponent::class)
@Module
abstract class RemoteSourceModule {

    @Singleton
    @Binds
    abstract fun bindUserRemoteSource(local: UserRemoteDataSource): UserRemoteSource

    @Singleton
    @Binds
    abstract fun bindDevicesRemoteSource(local: DeviceRemoteDataSource): DeviceRemoteSource

    @Singleton
    @Binds
    abstract fun bindInventoryRemoteSource(local: InventoryRemoteDataSource): InventoryRemoteSource

    @Singleton
    @Binds
    abstract fun bindProductRemoteSource(local: ProductRemoteDataSource): ProductRemoteSource

    @Singleton
    @Binds
    abstract fun bindProductTypeRemoteSource(local: ProductTypeRemoteDataSource): ProductTypeRemoteSource

    @Singleton
    @Binds
    abstract fun bindRuleRemoteSource(local: RuleRemoteDataSource): RuleRemoteSource

    @Singleton
    @Binds
    abstract fun bindSyncRemoteSource(local: SyncRemoteDataSource): SyncRemoteSource

    @Singleton
    @Binds
    abstract fun bindGroceriesRemoteSource(local: GroceriesRemoteDataSource): GroceriesRemoteSource
}

@InstallIn(SingletonComponent::class)
@Module
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun bindDeviceRepository(repository: DefaultDevicesRepository): DevicesRepository

    @Singleton
    @Binds
    abstract fun bindUserRepository(repository: DefaultUserRepository): UserRepository

    @Singleton
    @Binds
    abstract fun bindInventoryRepository(repository: DefaultInventoryRepository): InventoryRepository

    @Singleton
    @Binds
    abstract fun bindProductRepository(repository: DefaultProductRepository): ProductRepository

    @Singleton
    @Binds
    abstract fun bindProductTypeRepository(repository: DefaultProductTypeRepository): ProductTypeRepository

    @Singleton
    @Binds
    abstract fun bindRuleRepository(repository: DefaultRuleRepository): RuleRepository

    @Singleton
    @Binds
    abstract fun bindSyncRepository(repository: DefaultSyncRepository): SyncRepository

    @Singleton
    @Binds
    abstract fun bindGroceriesRepository(repository: DefaultGroceriesRepository): GroceriesRepository
}