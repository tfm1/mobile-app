package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory

import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.remote.InventoryRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultInventoryRepository @Inject constructor(
    private val localInventoryDataSource: InventoryDatabaseSource,
    private val remoteInventoryDataSource: InventoryRemoteSource,
    private val sessionManagement: SessionManagement
) : InventoryRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun updateInventoryInfo(newData: List<Register>) =
        withContext(ioDispatcher) {

            val resultWrapper = safeApiCall(ioDispatcher) {
                remoteInventoryDataSource.updateInventoryInfo(
                    newData, sessionManagement.getStoredInventorySharedId()
                )
            }

            when (resultWrapper) {
                is ResultWrapper.Success -> {
                    // if it is successful, we have already inserted the data with the remote ids
                }
                else -> localInventoryDataSource.updateInventoryInfo(
                    newData, sessionManagement.getStoredInventoryInternalId()
                )
            }
        }

    override suspend fun addScannedRegisters(newData: List<Register>) = withContext(ioDispatcher) {

        val modifiedRegisters = localInventoryDataSource.addScannedRegisters(
            newData, sessionManagement.getStoredInventoryInternalId()
        )

        safeApiCall(ioDispatcher) {
            remoteInventoryDataSource.updateInventoryInfo(
                modifiedRegisters, sessionManagement.getStoredInventorySharedId()
            )
        }

        return@withContext
    }

    override suspend fun getDeviceInventoryInfo(deviceUUID: String): DeviceInventoryMapping =
        withContext(ioDispatcher) {
            return@withContext localInventoryDataSource.getDeviceInventoryInfo(
                sessionManagement.userInternalId(), deviceUUID
            )
        }


}