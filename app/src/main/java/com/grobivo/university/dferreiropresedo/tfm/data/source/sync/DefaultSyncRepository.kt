package com.grobivo.university.dferreiropresedo.tfm.data.source.sync

import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.local.SyncLocalSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.remote.SyncRemoteSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class DefaultSyncRepository @Inject constructor(
    private val syncRemoteDataSource: SyncRemoteSource,
    private val syncLocalSource: SyncLocalSource
) : SyncRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun syncUserInformation() =
        withContext(ioDispatcher) {
            val safeApiCall = safeApiCall(ioDispatcher) {
                Timber.d("Calling the remote endpoint...")
                syncRemoteDataSource.syncUserInformation()
            }
            when (safeApiCall) {
                is ResultWrapper.Success -> {
                    Timber.d("Storing the data into local database...")
                    val devicesInformation = safeApiCall.value
                    syncLocalSource.syncUserInformation(devicesInformation)
                }
                else -> {

                }
            }
        }
}