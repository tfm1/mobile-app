package com.grobivo.university.dferreiropresedo.tfm.data.network

import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor: Interceptor {

    private val authHeader = "Authorization"

    var sessionToken: String = ""

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        val requestBuilder = request.newBuilder()


        if (request.header(authHeader) == null) {
            requestBuilder.addHeader(authHeader, sessionToken)
        }

        return chain.proceed(requestBuilder.build())
    }
}