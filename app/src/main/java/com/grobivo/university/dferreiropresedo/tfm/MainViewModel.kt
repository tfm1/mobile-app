package com.grobivo.university.dferreiropresedo.tfm

import androidx.lifecycle.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.DevicesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.sync.SyncRepository
import com.grobivo.university.dferreiropresedo.tfm.login.LoginStepsLoadingDialog
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val sessionManagement: SessionManagement,
    private val devicesRepository: DevicesRepository,
    private val syncRepository: SyncRepository
) : ViewModel() {

    private val _searchCriteria = MutableLiveData<String>()
    val searchCriteria: LiveData<String>
        get() = _searchCriteria

    private val _flagCleanseSearchCriteria = MutableLiveData<Boolean>()
    val flagCleanseSearchCriteria: LiveData<Boolean>
        get() = _flagCleanseSearchCriteria

    private val _flagCheckNavigationDrawer = MutableLiveData<Boolean>()
    val flagCheckNavigationDrawer: LiveData<Boolean>
        get() = _flagCheckNavigationDrawer

    private val _flagRequestPermissions = MutableLiveData<Boolean>(false)
    val flagRequestPermission: LiveData<Boolean>
        get() = _flagRequestPermissions

    private val _syncSteps = MutableLiveData<LoginStepsLoadingDialog.LOGGING_PROGRESS>()
    val syncSteps: LiveData<LoginStepsLoadingDialog.LOGGING_PROGRESS>
        get() = _syncSteps


    fun flagRequestPermissionsActivated() = _flagRequestPermissions.postValue(true)
    fun flagRequestPermissionsDeactivated() = _flagRequestPermissions.postValue(false)

    fun modifyCriteria(newCriteria: String?) {
        _searchCriteria.postValue(newCriteria!!)
    }

    fun cleanseCriteria() {
        _flagCleanseSearchCriteria.postValue(true)
    }

    fun criteriaCleansed() {
        _flagCleanseSearchCriteria.postValue(false)
    }

    fun flagCheckNavigationDrawerDeactivate() {
        _flagCheckNavigationDrawer.postValue(false)
    }

    fun flagCheckNavigationDrawerActivate() {
        _flagCheckNavigationDrawer.postValue(true)
    }

    fun logoutUser(activity: MainActivity) {

        val options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(activity.getString(R.string.server_client_id))
            .requestEmail()
            .build()

        GoogleSignIn.getClient(activity, options).signOut().addOnCompleteListener {
            Timber.d("Finished")
        }

        val userInternalId = sessionManagement.userInternalId()
        viewModelScope.launch {
            devicesRepository.cleanUserLoggedOutInformation(userInternalId)
        }
        sessionManagement.logoutUser()
    }

    fun syncInformation() {
        if (sessionManagement.getStoredDeviceUUID() != null) {
            viewModelScope.launch {
                Timber.d("Syncing the user information...")
                _syncSteps.postValue(LoginStepsLoadingDialog.LOGGING_PROGRESS.SYNC)
                syncRepository.syncUserInformation()
                _syncSteps.postValue(LoginStepsLoadingDialog.LOGGING_PROGRESS.END)
            }
        }
    }
}