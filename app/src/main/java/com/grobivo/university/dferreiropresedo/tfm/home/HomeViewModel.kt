package com.grobivo.university.dferreiropresedo.tfm.home

import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.InventoryRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val inventoryRepository: InventoryRepository,
    private val productRepository: ProductRepository
) : ViewModel() {

    val categories = MediatorLiveData<List<ProductType>>()

    fun getDeviceProductsType(deviceUUID: String) {
        viewModelScope.launch {
            categories.addSource(productRepository.getStoredProductsType(deviceUUID)) {
                categories.postValue(it)
            }
        }
    }

    fun retrieveDeviceInventoryInfo(deviceUUID: String): DeviceInventoryMapping = runBlocking {
        inventoryRepository.getDeviceInventoryInfo(deviceUUID)
    }

}