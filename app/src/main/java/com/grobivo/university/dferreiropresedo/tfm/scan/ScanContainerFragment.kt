package com.grobivo.university.dferreiropresedo.tfm.scan

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentScanContainerBinding
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanCameraStep
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanPreviewStep
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.ScanStepperAdapter
import com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep.ScanProcessStep
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ScanContainerFragment : Fragment() {

    private val viewModel: ScanContainerViewModel by activityViewModels()

    private lateinit var viewDataBinding: FragmentScanContainerBinding

    @Inject
    lateinit var scanCameraStep: ScanCameraStep

    @Inject
    lateinit var scanPreviewStep: ScanPreviewStep

    @Inject
    lateinit var scanProcessStep: ScanProcessStep

    @Inject
    lateinit var sessionManagement: SessionManagement


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("Initializing the scan container fragment...")

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_scan_container, container, false)

        val scanStepperAdapter = ScanStepperAdapter(
            childFragmentManager, requireContext(), scanCameraStep, scanPreviewStep, scanProcessStep
        )

        viewDataBinding.scanStepperLayout.setAdapter(scanStepperAdapter, 0)
        viewDataBinding.scanStepperLayout.setListener(configureStepperListener())

        scanProcessStep.stepperLayout = viewDataBinding.scanStepperLayout
        scanCameraStep.stepperLayout = viewDataBinding.scanStepperLayout

        return viewDataBinding.root
    }

    private fun configureStepperListener(): StepperLayout.StepperListener =
        object : StepperLayout.StepperListener {
            override fun onCompleted(completeButton: View?) {
                viewModel.completeScanningProcess()
                findNavController().navigate(
                    ScanContainerFragmentDirections.actionScanFragmentToHomeFragment(
                        Device(
                            sessionManagement.getStoredDeviceUUID()!!,
                            sessionManagement.getStoredDeviceName()!!
                        )
                    )
                )
            }

            override fun onError(verificationError: VerificationError?) {
                // Do nothing
            }

            override fun onStepSelected(newStepPosition: Int) {
                // Do nothing
            }

            override fun onReturn() {
                // Do nothing
            }

        }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    override fun onDestroy() {
        Timber.d("destroyed")
        super.onDestroy()
    }
}