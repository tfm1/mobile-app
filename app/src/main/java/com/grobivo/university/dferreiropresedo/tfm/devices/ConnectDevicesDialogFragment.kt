package com.grobivo.university.dferreiropresedo.tfm.devices

import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import com.grobivo.university.dferreiropresedo.tfm.R

class ConnectDevicesDialogFragment constructor(
    private val devices: List<BluetoothDevice>,
    private val host: Fragment
) :
    DialogFragment() {

    interface ConnectDevicesDialogListener {
        fun onConnectDevicesDialogConfirm(dialog: DialogFragment, bluetoothDevice: BluetoothDevice)
    }

    private lateinit var listener: ConnectDevicesDialogListener
    private var deviceSelected: BluetoothDevice? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)

        // checking if the host implements the corresponding interface
        try {
            listener = host as ConnectDevicesDialogListener
        } catch (e: ClassCastException) {
            throw ClassCastException(context.toString() + "must implement ConnectDevicesDialogListener")
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return activity?.let {

            val devicesToChoose = devices.toTypedArray()
            val devicesData =
                devices.map { device -> device.name }.toTypedArray()

            val builder = AlertDialog.Builder(it)


            if (devicesToChoose.isNullOrEmpty()) {
                builder.setTitle(getString(R.string.devices_dialog_connect_device_title))
                builder.setMessage(getString(R.string.devices_dialog_connect_device_empty_message))


            } else {
                builder.setTitle(getString(R.string.devices_dialog_connect_device_title))

                builder.setSingleChoiceItems(devicesData, -1) { _, which ->
                    if (which < devicesToChoose.size) {
                        deviceSelected = devicesToChoose[which]
                    }
                }

                builder.setNegativeButton(R.string.generic_text_cancel, null)
            }

            builder.setPositiveButton(R.string.generic_text_ok) { _, _ ->
                if (deviceSelected != null) {
                    listener.onConnectDevicesDialogConfirm(this, deviceSelected!!)
                }
            }

            return builder.create()

        } ?: throw IllegalStateException("Activity cannot be null")
    }
}