package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local

import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface InventoryDatabaseSource {

    suspend fun updateInventoryInfo(newData: List<Register>, storedInventory: Long)

    suspend fun addScannedRegisters(newData: List<Register>, storedInventory: Long): List<Register>

    suspend fun getDeviceInventoryInfo(userId: String, deviceUUID: String): DeviceInventoryMapping

}