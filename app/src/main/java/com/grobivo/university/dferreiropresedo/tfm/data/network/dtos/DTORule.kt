package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule
import com.squareup.moshi.Json

data class DTORule(
    @Json(name = "client_rule_id") val clientRuleId: Long = 0L,
    @Json(name = "server_rule_id") val serverRuleId: Long? = 0L,
    @Json(name = "quantity") val quantity: Float,
    @Json(name = "inventory_id") val inventoryId: Long,
    @Json(name = "product") val product: DTOProduct
)

fun DTORule.asDomainModel(): Rule =
    Rule(clientRuleId, serverRuleId, quantity, inventoryId, product.asDomainModel())

fun List<DTORule>.asDomainModel(): List<Rule> = map { it.asDomainModel() }