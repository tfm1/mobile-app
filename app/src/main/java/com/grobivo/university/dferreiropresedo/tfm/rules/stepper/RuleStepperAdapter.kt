package com.grobivo.university.dferreiropresedo.tfm.rules.stepper

import android.content.Context
import androidx.fragment.app.FragmentManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep.RuleStepperProduct
import com.grobivo.university.dferreiropresedo.tfm.rules.stepper.quantitystep.RuleStepperQuantity
import com.stepstone.stepper.Step
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter
import com.stepstone.stepper.viewmodel.StepViewModel

class RuleStepperAdapter(
    fm: FragmentManager,
    context: Context,
    private val ruleStepperProduct: RuleStepperProduct,
    private val ruleStepperQuantity: RuleStepperQuantity
) :
    AbstractFragmentStepAdapter(fm, context) {

    override fun getCount(): Int = 2

    override fun createStep(position: Int): Step {

        return when (position) {
            0 -> ruleStepperProduct
            1 -> ruleStepperQuantity
            else -> TODO("Something went wrong")
        }
    }

    override fun getViewModel(position: Int): StepViewModel {

        return when (position) {
            0 -> StepViewModel.Builder(context)
                .setBackButtonVisible(false)
                .setEndButtonLabel(R.string.generic_text_next)
                .create()
            1 -> StepViewModel.Builder(context)
                .setBackButtonVisible(true)
                .setBackButtonLabel(R.string.generic_text_back)
                .setEndButtonLabel(R.string.generic_text_complete)
                .create()
            else -> TODO("Another thing wrong")
        }
    }
}