package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.processstep

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.custom.DoublePickerDialog
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.ProductsItemlistViewBinding
import com.grobivo.university.dferreiropresedo.tfm.products.RegisterItemListener
import com.grobivo.university.dferreiropresedo.tfm.products.RegistersDiffItemCallback

class ScanProcessAdapter(
    private val childFragmentManager: FragmentManager,
    private val viewLifecycleOwner: LifecycleOwner
) :
    ListAdapter<Register, ScanProcessAdapter.ScannedProductViewHolder>(RegistersDiffItemCallback()) {

    var selectionTracker: SelectionTracker<Register>? = null

    class ScannedProductViewHolder private constructor(
        private val binding: ProductsItemlistViewBinding,
        private val childFragmentManager: FragmentManager
    ) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(
                view: ViewGroup,
                childFragmentManager: FragmentManager
            ): ScannedProductViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = ProductsItemlistViewBinding.inflate(from, view, false)
                return ScannedProductViewHolder(binding, childFragmentManager)
            }
        }

        fun bind(register: Register, viewLifecycleOwner: LifecycleOwner, selected: Boolean) {

            binding.registerInfo = register
            binding.listener = RegisterItemListener(binding)

            binding.root.isActivated = selected
            binding.productCard.isChecked = selected

            binding.productQuantity.setOnClickListener {
                val doublePicker =
                    DoublePickerDialog(register.quantity, register.product.productType.measure)
                doublePicker
                    .show(childFragmentManager, DoublePickerDialog::class.qualifiedName)
                doublePicker.valueSelected.observe(viewLifecycleOwner) {
                    binding.productQuantity.text = it.toString()
                    register.quantity = it
                }
            }

            binding.executePendingBindings()
        }


        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Register> =
            object : ItemDetailsLookup.ItemDetails<Register>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): Register? = binding.registerInfo
            }
    }


    fun getItemByPosition(position: Int): Register = getItem(position)

    fun getItemPosition(registerProductId: Long) =
        currentList.indexOfFirst { it.product.productId == registerProductId }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScannedProductViewHolder {
        return ScannedProductViewHolder.from(parent, childFragmentManager)
    }

    override fun onBindViewHolder(holder: ScannedProductViewHolder, position: Int) {
        val register = getItem(position)
        selectionTracker?.let {
            holder.bind(register!!, viewLifecycleOwner, it.isSelected(register))
        }
    }

}