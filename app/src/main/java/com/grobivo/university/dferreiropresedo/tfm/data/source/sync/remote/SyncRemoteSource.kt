package com.grobivo.university.dferreiropresedo.tfm.data.source.sync.remote

import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTODeviceInformation
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTOUserDeviceInformation

interface SyncRemoteSource {

    suspend fun syncUserInformation(): List<DTOUserDeviceInformation>

}