package com.grobivo.university.dferreiropresedo.tfm.groceries

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentGroceriesListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class GroceriesFragment : Fragment() {

    private val viewModel: GroceriesViewModel by viewModels()
    private val activityViewModel: MainViewModel by activityViewModels()

    private lateinit var viewDataBinding: FragmentGroceriesListBinding
    private lateinit var groceryAdapter: GroceryItemAdapter


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_groceries_list, container, false)

        configureRecyclerView()
        configureObservers()

        viewModel.requestGroceriesList()

        return viewDataBinding.root
    }

    private fun configureObservers() {
        viewModel.flagErrorRequestingGroceries.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(requireContext(), R.string.groceries_error, Toast.LENGTH_SHORT)
                    .show()
                viewModel.flagErrorRequestingGroceriesDeactivate()
            }
        }
    }

    private fun configureRecyclerView() {
        groceryAdapter = GroceryItemAdapter()

        viewModel.productsToBuy.observe(viewLifecycleOwner) { groceries ->
            if (groceries.isNullOrEmpty()) {
                viewDataBinding.noGroceriesText.visibility = View.VISIBLE
                viewDataBinding.groceriesList.visibility = View.INVISIBLE
            } else {
                viewDataBinding.noGroceriesText.visibility = View.INVISIBLE
                viewDataBinding.groceriesList.visibility = View.VISIBLE
                groceryAdapter.submitList(groceries)
            }
        }

        viewDataBinding.groceriesList.adapter = groceryAdapter
        val gridLayoutManager = GridLayoutManager(activity, 1)
        viewDataBinding.groceriesList.layoutManager = gridLayoutManager


        activityViewModel.searchCriteria.observe(viewLifecycleOwner) { searchCriteria ->

            val listToShow = if (!searchCriteria.isNullOrBlank()) {
                viewModel.productsToBuy.value?.filter { grocery ->
                    grocery.product.name.contains(searchCriteria, true)
                }
            } else {
                viewModel.productsToBuy.value
            }

            groceryAdapter.submitList(listToShow)
            viewDataBinding.groceriesList.smoothScrollToPosition(0)
        }
    }

}