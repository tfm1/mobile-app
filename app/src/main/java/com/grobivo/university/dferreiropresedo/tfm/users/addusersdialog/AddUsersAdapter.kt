package com.grobivo.university.dferreiropresedo.tfm.users.addusersdialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.UserSelected
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDialogAddUsersItemviewBinding

class AddUsersAdapter :
    ListAdapter<UserSelected, AddUsersAdapter.AddUsersViewHolder>(AddUsersManagementDiffCallback()) {

    class AddUsersViewHolder private constructor(val binding: CustomDialogAddUsersItemviewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        companion object {
            fun from(parent: ViewGroup): AddUsersViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding =
                    CustomDialogAddUsersItemviewBinding.inflate(layoutInflater, parent, false)
                return AddUsersViewHolder(binding)
            }
        }

        fun bind(userInfo: UserSelected) {
            binding.userInfo = userInfo
            binding.userManagementListener = OnUserSelectedListener(binding)
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AddUsersViewHolder {
        return AddUsersViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: AddUsersViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class AddUsersManagementDiffCallback : DiffUtil.ItemCallback<UserSelected>() {
    override fun areItemsTheSame(oldItem: UserSelected, newItem: UserSelected): Boolean {
        return oldItem.user.internalId == newItem.user.internalId
    }

    override fun areContentsTheSame(oldItem: UserSelected, newItem: UserSelected): Boolean {
        return oldItem == newItem
    }
}

class OnUserSelectedListener(private val binding: CustomDialogAddUsersItemviewBinding) {
    fun userSelected() {
        binding.userInfo?.selected = binding.userManagedSelected.isChecked
    }
}