package com.grobivo.university.dferreiropresedo.tfm.di

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.grobivo.university.dferreiropresedo.tfm.data.database.AppDatabase
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityProduct
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityProductType
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local.ProductTypeDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.local.ProductDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import timber.log.Timber
import java.util.concurrent.Executors
import javax.inject.Provider
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DatabaseModule {

    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context, productTypeDao: Provider<ProductTypeDao>,
        productDao: Provider<ProductDao>
    ): AppDatabase {

        Timber.d("database creation")

        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "grobivo"
        )
            .addCallback(object : RoomDatabase.Callback() {
                override fun onCreate(db: SupportSQLiteDatabase) {
                    super.onCreate(db)
                    populateDatabase(productTypeDao.get(), productDao.get())
                }
            })
            .build()
    }

    @Provides
    fun provideDeviceDao(database: AppDatabase): DeviceDao {
        return database.deviceDao()
    }

    @Provides
    fun provideUserDao(database: AppDatabase): UserDao {
        return database.userDao()
    }

    @Provides
    fun provideInventoryDao(database: AppDatabase): InventoryDao {
        return database.inventoryDao()
    }

    @Provides
    fun provideProductDao(database: AppDatabase): ProductDao {
        return database.productDao()
    }

    @Provides
    fun provideProductTypeDao(database: AppDatabase): ProductTypeDao {
        return database.productTypeDao()
    }

    @Provides
    fun provideRegisterDao(database: AppDatabase): RegisterDao {
        return database.registerDao()
    }

    @Provides
    fun provideRuleDao(database: AppDatabase): RuleDao {
        return database.ruleDao()
    }


    private fun populateDatabase(
        productTypeDao: ProductTypeDao, productDao: ProductDao
    ) {
        Executors.newSingleThreadExecutor().execute {
            val entityProductType = EntityProductType(1, "Beverage", "L")
            val entityProductType2 = EntityProductType(2, "Food", "Kg")
            val entityProductType3 = EntityProductType(3, "Misc", "Units")
            val beverage1 = EntityProduct(1, "Water", "AGUA", entityProductType.productTypeId)
            val beverage2 = EntityProduct(2, "Coke", "COCA", entityProductType.productTypeId)
            val food1 = EntityProduct(3, "Chicken", "POLLO", entityProductType2.productTypeId)
            val food2 = EntityProduct(4, "Cow", "TERNERA", entityProductType2.productTypeId)
            val trashBag =
                EntityProduct(5, "Trash bags", "BASURA", entityProductType3.productTypeId)

            productTypeDao.insertEntitySync(entityProductType.name, entityProductType.measure)
            productTypeDao.insertEntitySync(entityProductType2.name, entityProductType2.measure)
            productTypeDao.insertEntitySync(entityProductType3.name, entityProductType3.measure)

            productDao.syncInsert(beverage1)
            productDao.syncInsert(beverage2)
            productDao.syncInsert(food1)
            productDao.syncInsert(food2)
            productDao.syncInsert(trashBag)
        }
    }

}