package com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class ProductTypeLocalDataSource @Inject constructor(private val productTypeDao: ProductTypeDao) :
    ProductTypeDatabaseSource {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

}