package com.grobivo.university.dferreiropresedo.tfm.data.source.products.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import javax.inject.Inject

class ProductRemoteDataSource @Inject internal constructor(
    private val endpointService: EndpointService,
    private val sessionManagement: SessionManagement,
    private val inventoryLocalDataSource: InventoryLocalDataSource
) : ProductRemoteSource {

    override suspend fun getAllDeviceProducts(deviceUUID: String) {

        val remoteRegisters = endpointService.getAllDeviceProducts(deviceUUID).asDomainModel()
        val registersWithLocalInventoryId = remoteRegisters.map {
            Register(
                it.registerId, it.sharedRegisterId, it.quantity,
                sessionManagement.getStoredInventoryInternalId(), it.product
            )
        }

        inventoryLocalDataSource.updateInventoryInfo(
            registersWithLocalInventoryId,
            sessionManagement.getStoredInventoryInternalId()
        )
    }
}