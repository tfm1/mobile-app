package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.dialog

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.LiveData
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDialogScanSelectProductBinding
import timber.log.Timber

class ProductSelectorDialogFragment constructor(
    private val productsToSelect: LiveData<List<Product>>,
    private val listener: ProductsToRegisterSelected
) : DialogFragment() {

    interface ProductsToRegisterSelected {
        fun onProductsSelected(products: List<Product>?)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {

        val binding: CustomDialogScanSelectProductBinding = DataBindingUtil.inflate(
            LayoutInflater.from(context), R.layout.custom_dialog_scan_select_product, null, false
        )

        val productSelectorAdapter = ProductSelectorAdapter()
        binding.productsToScanList.layoutManager = GridLayoutManager(context, 2)
        binding.productsToScanList.adapter = productSelectorAdapter
        generateSelectionTracker(binding, productSelectorAdapter)

        productsToSelect.observe(this) { products ->
            productSelectorAdapter.submitList(products)
        }

        return AlertDialog.Builder(activity)
            .setTitle(R.string.scan_product_selection_header)
            .setPositiveButton(R.string.generic_text_ok) { _, _ ->
                Timber.d("Selected items %s", productSelectorAdapter.selectionTracker?.selection)
                listener.onProductsSelected(productSelectorAdapter.selectionTracker?.selection?.toList())
            }
            .setNegativeButton(R.string.generic_text_cancel) { _, _ ->
                // Do nothing
            }
            .setView(binding.root)
            .create()
    }

    private fun generateSelectionTracker(
        binding: CustomDialogScanSelectProductBinding,
        adapter: ProductSelectorAdapter
    ): SelectionTracker<Product> {

        val selectionTracker = SelectionTracker.Builder(
            "scanProductSelect",
            binding.productsToScanList,
            ProductSelectionItemKeyProvider(adapter),
            ProductSelectionItemLookup(binding.productsToScanList),
            StorageStrategy.createParcelableStorage(Product::class.java)
        )
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        adapter.selectionTracker = selectionTracker

        return selectionTracker
    }

}