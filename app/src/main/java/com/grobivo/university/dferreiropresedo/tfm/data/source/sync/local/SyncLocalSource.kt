package com.grobivo.university.dferreiropresedo.tfm.data.source.sync.local

import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTOUserDeviceInformation

interface SyncLocalSource {

    suspend fun syncUserInformation(userInformation: List<DTOUserDeviceInformation>)
}