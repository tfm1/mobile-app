package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Inventory
import com.squareup.moshi.Json

data class DTOInventory(
    @Json(name = "device_UUID") val deviceUUID: String,
    @Json(name = "server_inventory_id") val serverInventoryId: Long? = 0L,
    @Json(name = "client_inventory_id") val clientInventoryId: Long = 0L,
    @Json(name = "registers") val registers: List<DTORegister>,
    @Json(name = "rules") val rules: List<DTORule>
)

fun DTOInventory.asDomainModel(): Inventory =
    Inventory(deviceUUID, serverInventoryId, clientInventoryId)

