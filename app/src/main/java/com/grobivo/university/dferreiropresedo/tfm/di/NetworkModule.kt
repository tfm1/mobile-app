package com.grobivo.university.dferreiropresedo.tfm.di

import com.grobivo.university.dferreiropresedo.tfm.data.network.AppService
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import timber.log.Timber
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NetworkModule {

    @Singleton
    @Provides
    fun provideService(sessionManagement: SessionManagement): EndpointService {
        Timber.d("Providing the Network Service")
        val endpointService: EndpointService by lazy {
            AppService.getEndpointService(sessionManagement)
        }
        return endpointService
    }
}