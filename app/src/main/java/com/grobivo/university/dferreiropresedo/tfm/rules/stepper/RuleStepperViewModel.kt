package com.grobivo.university.dferreiropresedo.tfm.rules.stepper

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.RuleRepository
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RuleStepperViewModel @Inject constructor(
    private val ruleRepository: RuleRepository,
    private val sessionManagement: SessionManagement
) :
    ViewModel() {

    private val _products = MutableLiveData<List<Register>>()
    val products: LiveData<List<Register>>
        get() = _products


    var productSelected: Register? = null
    var quantitySelected: Float = 0F

    fun cleanData() {
        productSelected = null
        quantitySelected = 0F
    }

    fun createRule(product: Product, quantity: Float) {
        viewModelScope.launch {
            ruleRepository.createRule(
                sessionManagement.getStoredInventoryInternalId(), product, quantity
            )
        }
    }


    fun retrieveProductsInfo() {
        viewModelScope.launch {
            _products.postValue(ruleRepository.getAllProductsWithoutRule(sessionManagement.getStoredInventoryInternalId()))
        }
    }

}