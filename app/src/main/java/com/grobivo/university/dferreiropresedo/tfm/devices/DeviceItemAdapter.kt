package com.grobivo.university.dferreiropresedo.tfm.devices

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.databinding.DeviceItemlistViewBinding

class DeviceItemAdapter(private val clickListener: DeviceItemListener) :
    ListAdapter<Device, DeviceItemAdapter.ViewHolder>(DeviceItemDiffCallback()) {

    var selectionTracker: SelectionTracker<Device>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val device = getItem(position)
        selectionTracker?.let {
            holder.bind(device!!, clickListener, it.isSelected(device))
        }
    }

    fun getItemByPosition(position: Int): Device = getItem(position)

    fun getItemPosition(deviceUUID: String) = currentList.indexOfFirst { it.UUID == deviceUUID }

    class ViewHolder private constructor(val binding: DeviceItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)

                val binding = DeviceItemlistViewBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }

        fun bind(
            device: Device,
            clickListener: DeviceItemListener,
            isItemSelected: Boolean
        ) {
            binding.deviceInfo = device
            binding.clickListener = clickListener

            binding.root.isActivated = isItemSelected
            binding.deviceItemView.isChecked = isItemSelected

            binding.executePendingBindings()
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Device> =
            object : ItemDetailsLookup.ItemDetails<Device>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): Device? = binding.deviceInfo
            }

    }
}


class DeviceItemDiffCallback : DiffUtil.ItemCallback<Device>() {

    override fun areItemsTheSame(oldItem: Device, newItem: Device): Boolean {
        return oldItem.UUID == newItem.UUID
    }

    override fun areContentsTheSame(oldItem: Device, newItem: Device): Boolean {
        return oldItem == newItem
    }
}


class DeviceItemListener(val clickListener: (device: Device) -> Unit) {
    fun onClick(device: Device) = clickListener(device)
}