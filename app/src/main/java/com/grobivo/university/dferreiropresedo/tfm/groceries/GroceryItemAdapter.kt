package com.grobivo.university.dferreiropresedo.tfm.groceries

import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.GroceryItemlistViewBinding

class GroceryItemAdapter :
    ListAdapter<Register, GroceryItemAdapter.GroceryViewHolder>(GroceryItemDiffCallback()) {

    class GroceryViewHolder private constructor(val binding: GroceryItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(parent: ViewGroup): GroceryViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = GroceryItemlistViewBinding.inflate(layoutInflater, parent, false)
                return GroceryViewHolder(binding)
            }
        }

        fun bind(register: Register) {
            binding.register = register
            binding.groceryCheckbox.setOnCheckedChangeListener { _, isChecked ->
                if (isChecked) {
                    binding.groceryName.setTypeface(
                        binding.groceryCheckbox.typeface, Typeface.ITALIC
                    )
                    binding.crossEverything.visibility = View.VISIBLE

                } else {
                    binding.groceryName.setTypeface(
                        binding.groceryCheckbox.typeface, Typeface.NORMAL
                    )
                    binding.crossEverything.visibility = View.GONE
                }
            }

            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GroceryViewHolder {
        return GroceryViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: GroceryViewHolder, position: Int) {
        holder.bind(getItem(position))
    }


}


class GroceryItemDiffCallback : DiffUtil.ItemCallback<Register>() {

    override fun areItemsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem.registerId == newItem.registerId
    }

    override fun areContentsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem == newItem
    }
}