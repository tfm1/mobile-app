package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.squareup.moshi.Json

data class DTODeviceRules(
    @Json(name = "device") val deviceInfo: DTODevice,
    @Json(name = "rules") val rules: List<DTORule>
)

