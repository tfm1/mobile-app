package com.grobivo.university.dferreiropresedo.tfm.products

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.InventoryRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.ProductRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject


@HiltViewModel
class ProductsViewModel @Inject constructor(
    private val productRepository: ProductRepository,
    private val inventoryRepository: InventoryRepository
) :
    ViewModel() {

    private val _flagChangesCompleted = MutableLiveData(false)
    val flagChangesCompleted: LiveData<Boolean>
        get() = _flagChangesCompleted

    private val _productRegisters = MutableLiveData<List<Register>>()
    val productRegisters: LiveData<List<Register>>
        get() = _productRegisters


    fun findProducts(deviceUUID: String) {
        viewModelScope.launch {
            Timber.d("Searching all the products stocked")
            val allProducts = productRepository.getAllDeviceProducts(deviceUUID)
            _productRegisters.postValue(allProducts)
        }
    }

    fun findProducts(deviceUUID: String, productTypeId: Long) {
        viewModelScope.launch {
            Timber.d("Searching the products stocked by product type id")
            _productRegisters.postValue(
                productRepository.getStoredProductsByTypeId(deviceUUID, productTypeId)
            )
        }
    }

    fun confirmChanges() {
        viewModelScope.launch {
            Timber.d("Confirming the changes in the stock")
            inventoryRepository.updateInventoryInfo(productRegisters.value!!)
            flagChangesCompletedActivate()
        }
    }

    fun flagChangesCompletedActivate() = _flagChangesCompleted.postValue(true)
    fun flagChangesCompletedDeactivate() = _flagChangesCompleted.postValue(false)

}