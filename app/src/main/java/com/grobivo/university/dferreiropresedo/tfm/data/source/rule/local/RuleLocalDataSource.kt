package com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityRule
import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class RuleLocalDataSource @Inject constructor(
    private val ruleDao: RuleDao
) : RuleDatabaseSource {

    private val ioDispatcher = Dispatchers.IO

    override suspend fun getAmountOfProductsWithoutRule(inventoryId: Long): LiveData<Int> =
        withContext(ioDispatcher) {
            ruleDao.getAmountOfProductsWithoutRule(inventoryId)
        }

    override suspend fun getAllProductsWithoutRule(inventoryId: Long): List<Register> =
        withContext(ioDispatcher) {
            ruleDao.getAllProductsWithoutRule(inventoryId).parseEntityToRegister()
        }

    override suspend fun createRule(inventoryId: Long, product: Product, quantity: Float): Long =
        withContext(ioDispatcher) {
            ruleDao.createRule(EntityRule(0L, null, quantity, inventoryId, product.productId))
        }

    override suspend fun createRules(rules: List<Rule>) = withContext(ioDispatcher) {
        ruleDao.createRules(rules.asRuleDatabaseModel())
    }

    override suspend fun retrieveInventoryRules(inventoryId: Long): LiveData<List<Rule>> =
        withContext(ioDispatcher) {
            return@withContext Transformations.map(ruleDao.retrieveInventoryRules(inventoryId)) {
                it.parseProductRuleMapping()
            }
        }

    override suspend fun storeRuleChanges(rules: List<Rule>) = withContext(ioDispatcher) {
        ruleDao.updateRules(rules.asRuleDatabaseModel())
    }

    override suspend fun removeRules(rulesToDelete: Set<Rule>) = withContext(ioDispatcher) {
        val entityRulesToDelete = rulesToDelete.map { it.asRuleDatabaseModel() }
        ruleDao.removeRules(entityRulesToDelete)
    }


}