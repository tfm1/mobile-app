package com.grobivo.university.dferreiropresedo.tfm.rules

import android.view.MotionEvent
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.ItemKeyProvider
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule

class RuleItemKeyProvider(private val ruleAdapter: RulesAdapter) :
    ItemKeyProvider<Rule>(
        SCOPE_CACHED
    ) {

    override fun getKey(position: Int): Rule? = ruleAdapter.getItemByPosition(position)

    override fun getPosition(key: Rule): Int = ruleAdapter.getItemPosition(key.ruleId)

}

class RuleItemLookup(private val recyclerView: RecyclerView) : ItemDetailsLookup<Rule>() {

    override fun getItemDetails(e: MotionEvent): ItemDetails<Rule>? {
        val viewSelected = recyclerView.findChildViewUnder(e.x, e.y)

        if (viewSelected != null) {
            val viewSelectedHolder =
                recyclerView.getChildViewHolder(viewSelected) as RulesAdapter.RuleViewHolder

            return viewSelectedHolder.getItemDetails()
        }

        return null
    }

}