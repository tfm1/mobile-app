package com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local

import androidx.room.*
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityDevice

@Dao
interface DeviceDao {

    @Insert
    fun insertSync(device: EntityDevice)

    @Insert
    suspend fun insert(device: EntityDevice)

    @Delete
    suspend fun delete(device: List<EntityDevice>)

    @Query("""DELETE FROM device WHERE UUID IN (:deviceUUID)""")
    suspend fun deleteDevices(deviceUUID: List<String>)

    @Query("SELECT * FROM device WHERE UUID LIKE :UUID")
    suspend fun getDeviceByUUID(UUID: String): EntityDevice?

    @Query("INSERT OR IGNORE INTO device_users(user_internal_id, device_UUID) VALUES (:userInternalId, :deviceUUID)")
    suspend fun insertDeviceUser(userInternalId: String, deviceUUID: String)

    @Query("INSERT OR IGNORE INTO device_users(user_internal_id, device_UUID) VALUES (:userInternalId, :deviceUUID)")
    fun insertDeviceUserSync(userInternalId: String, deviceUUID: String)

    @Query("DELETE FROM device_users WHERE user_internal_id LIKE :userInternalId AND device_UUID IN (:UUID)")
    suspend fun deleteUserDevices(UUID: List<String>, userInternalId: String)

    @Transaction
    suspend fun connectDeviceToUser(device: EntityDevice, userInternalId: String) {
        insert(device)
        insertDeviceUser(userInternalId, device.UUID)
    }

    @Transaction
    suspend fun disconnectDevicesToUser(
        device: List<EntityDevice>,
        UUIDs: List<String>,
        userInternalId: String
    ) {
        delete(device)
        deleteUserDevices(UUIDs, userInternalId)
    }
}