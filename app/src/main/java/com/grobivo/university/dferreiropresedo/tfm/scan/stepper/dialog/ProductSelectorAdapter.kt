package com.grobivo.university.dferreiropresedo.tfm.scan.stepper.dialog

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.databinding.CustomDialogScanSelectProductItemviewBinding

class ProductSelectorAdapter(

) : ListAdapter<Product, ProductSelectorAdapter.ProductSelectorViewHolder>(ProductDiffItemCallback()) {

    var selectionTracker: SelectionTracker<Product>? = null

    class ProductSelectorViewHolder private constructor(
        private val binding: CustomDialogScanSelectProductItemviewBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup): ProductSelectorViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding =
                    CustomDialogScanSelectProductItemviewBinding.inflate(from, view, false)
                return ProductSelectorViewHolder(binding)
            }
        }

        fun bind(product: Product, selected: Boolean) {
            binding.product = product
            binding.root.isActivated = selected
            binding.productToScanView.isChecked = selected
            binding.executePendingBindings()
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Product> =
            object : ItemDetailsLookup.ItemDetails<Product>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): Product? = binding.product
            }

    }

    fun getItemByPosition(position: Int): Product = getItem(position)

    fun getItemPosition(productId: Long) = currentList.indexOfFirst { it.productId == productId }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductSelectorViewHolder {
        return ProductSelectorViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ProductSelectorViewHolder, position: Int) {
        val product = getItemByPosition(position)
        selectionTracker?.let {
            holder.bind(product, it.isSelected(product))
        }
    }
}


class ProductDiffItemCallback : DiffUtil.ItemCallback<Product>() {
    override fun areItemsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem.productId == newItem.productId

    override fun areContentsTheSame(oldItem: Product, newItem: Product): Boolean =
        oldItem == newItem
}