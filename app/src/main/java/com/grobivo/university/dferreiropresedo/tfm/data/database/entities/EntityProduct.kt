package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product

@Entity(
    tableName = "product"
)
data class EntityProduct constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "product_id") var productId: Long,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "ticket_code") val ticketCode: String,
    @ColumnInfo(name = "product_type_id") val productTypeId: Long
)

fun EntityProduct.asDomainModel(entityProductType: EntityProductType) =
    Product(productId, name, ticketCode, entityProductType.asDomainModel())