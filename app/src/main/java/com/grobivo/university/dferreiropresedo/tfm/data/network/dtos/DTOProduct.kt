package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.squareup.moshi.Json

data class DTOProduct(
    @Json(name = "product_id") val productId: Long,
    @Json(name = "name") val name: String,
    @Json(name = "productType") val productType: DTOProductType
)

fun DTOProduct.asDomainModel(): Product = Product(productId, name, "", productType.asDomainModel())