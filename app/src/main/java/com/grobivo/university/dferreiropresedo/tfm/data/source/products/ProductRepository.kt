package com.grobivo.university.dferreiropresedo.tfm.data.source.products

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface ProductRepository {

    suspend fun getAllProductsToScan(): List<Product>

    suspend fun getAllProductsSkipping(productsToSkip: List<Product>): List<Product>

    suspend fun getAllDeviceProducts(deviceUUID: String): List<Register>

    suspend fun getStoredProductsByTypeId(
        deviceUUID: String, productTypeId: Long
    ): List<Register>

    suspend fun getStoredProductsType(deviceUUID: String): LiveData<List<ProductType>>

}