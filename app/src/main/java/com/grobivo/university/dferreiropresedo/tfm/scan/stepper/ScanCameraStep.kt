package com.grobivo.university.dferreiropresedo.tfm.scan.stepper

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageCapture
import androidx.camera.core.ImageCaptureException
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentScanCameraStepBinding
import com.grobivo.university.dferreiropresedo.tfm.scan.ScanContainerViewModel
import com.stepstone.stepper.Step
import com.stepstone.stepper.StepperLayout
import com.stepstone.stepper.VerificationError
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import javax.inject.Inject

class ScanCameraStep @Inject constructor() : Fragment(), Step {

    lateinit var stepperLayout: StepperLayout

    private lateinit var viewDataBinding: FragmentScanCameraStepBinding
    private lateinit var cameraExecutor: ExecutorService
    private var imageCapture: ImageCapture? = null

    private val containerViewModel: ScanContainerViewModel by activityViewModels()
    private val mainViewModel: MainViewModel by activityViewModels()

    companion object {
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
        private const val FILENAME_PREFIX = "tmpGrobivo"
        const val REQUEST_CODE_PERMISSIONS = 10
        val REQUIRED_PERMISSIONS = arrayOf(Manifest.permission.CAMERA)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("Initializing the camera step...")

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_scan_camera_step, container, false)


        configureListeners()
        configureObservers()
        cameraExecutor = Executors.newSingleThreadExecutor()

        return viewDataBinding.root
    }

    private fun configureObservers() {
        mainViewModel.flagRequestPermission.observe(viewLifecycleOwner) {
            if (it) {
                if (allPermissionsGranted()) {
                    startCamera()
                } else {
                    Toast.makeText(
                        requireContext(), "Permissions not granted by the user.", Toast.LENGTH_SHORT
                    ).show()
                    childFragmentManager.popBackStack()
                }
                mainViewModel.flagRequestPermissionsDeactivated()
            }
        }
    }

    private fun configureListeners() {
        viewDataBinding.takePhoto.setOnClickListener {
            takePhoto()
        }
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(requireContext(), it) == PackageManager.PERMISSION_GRANTED
    }

    private fun takePhoto() {
        Timber.d("Taking the photo...")
        val imageCapture = imageCapture ?: return

        val tmpFile = File.createTempFile(
            FILENAME_PREFIX,
            SimpleDateFormat(FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis())
        )

        // Create output options object which contains file + metadata
        val outputOptions = ImageCapture.OutputFileOptions.Builder(tmpFile).build()

        // Set up image capture listener, which is triggered after photo has
        // been taken
        imageCapture.takePicture(outputOptions, ContextCompat.getMainExecutor(requireContext()),
            object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Timber.e(exc, "Photo capture failed")
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    Timber.d("Photo taken")
                    containerViewModel.photoResult = tmpFile
                    stepperLayout.currentStepPosition += 1
                }
            })
    }

    private fun startCamera() {
        Timber.d("Opening the camera...")
        val cameraProviderFuture = ProcessCameraProvider.getInstance(requireContext())

        cameraProviderFuture.addListener({
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder().build()
                .also {
                    it.setSurfaceProvider(viewDataBinding.viewFinder.surfaceProvider)
                }
            imageCapture = ImageCapture.Builder().build()

            // Select back camera as a default
            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                cameraProvider.bindToLifecycle(
                    viewLifecycleOwner, cameraSelector, preview, imageCapture
                )

            } catch (exc: Exception) {
                Timber.e(exc, "Use case binding failed")
            }
        }, ContextCompat.getMainExecutor(requireContext()))
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraExecutor.shutdown()
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    override fun verifyStep(): VerificationError? = null

    override fun onSelected() {

        if (allPermissionsGranted()) {
            startCamera()
        } else {
            Timber.d("Asking for the needed permissions...")
            ActivityCompat.requestPermissions(
                requireActivity(), REQUIRED_PERMISSIONS, REQUEST_CODE_PERMISSIONS
            )
        }

    }

    override fun onError(error: VerificationError) {
        TODO("Not yet implemented")
    }
}