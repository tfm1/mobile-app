package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule


@Entity(
    tableName = "rule",
    indices = [
        Index(value = ["inventory_id", "product_id"], unique = true)
    ]
)
class EntityRule constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "rule_id") val ruleId: Long,
    @ColumnInfo(name = "shared_rule_id") val sharedRuleId: Long?,
    @ColumnInfo(name = "quantity") val quantity: Float,
    @ColumnInfo(name = "inventory_id") val inventoryId: Long,
    @ColumnInfo(name = "product_id") val productId: Long
)

fun EntityRule.asDomainModel(product: Product): Rule =
    Rule(ruleId, sharedRuleId, quantity, inventoryId, product)