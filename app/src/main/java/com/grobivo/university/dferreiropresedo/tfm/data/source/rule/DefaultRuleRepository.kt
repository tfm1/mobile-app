package com.grobivo.university.dferreiropresedo.tfm.data.source.rule

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.remote.RuleRemoteDataSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject


class DefaultRuleRepository @Inject constructor(
    private val ruleLocalDataSource: RuleDatabaseSource,
    private val ruleRemoteDataSource: RuleRemoteDataSource,
    private val sessionManagement: SessionManagement
) : RuleRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun getAmountOfProductsWithoutRule(inventoryId: Long): LiveData<Int> =
        withContext(ioDispatcher) {
            ruleLocalDataSource.getAmountOfProductsWithoutRule(inventoryId)
        }

    override suspend fun getAllProductsWithoutRule(inventoryId: Long): List<Register> =
        withContext(ioDispatcher) {
            ruleLocalDataSource.getAllProductsWithoutRule(inventoryId)
        }

    override suspend fun createRule(inventoryId: Long, product: Product, quantity: Float) =
        withContext(ioDispatcher) {

            val resultWrapper = safeApiCall(ioDispatcher) {
                ruleRemoteDataSource.createRule(
                    listOf(Rule(0L, null, quantity, inventoryId, product))
                )
            }

            when (resultWrapper) {
                is ResultWrapper.Success -> {
                    val rulesCreated = resultWrapper.value.map {
                        Rule(
                            0L, it.sharedRuleId, it.quantity,
                            sessionManagement.getStoredInventoryInternalId(), it.product
                        )
                    }
                    ruleLocalDataSource.createRules(rulesCreated)
                }
                else -> {
                    ruleLocalDataSource.createRule(inventoryId, product, quantity)
                }
            }
            return@withContext
        }


    override suspend fun retrieveInventoryRules(): LiveData<List<Rule>> =
        withContext(ioDispatcher) {

            safeApiCall(ioDispatcher) {
                ruleRemoteDataSource.retrieveInventoryRules(sessionManagement.getStoredInventorySharedId())
            }

            ruleLocalDataSource.retrieveInventoryRules(sessionManagement.getStoredInventoryInternalId())
        }

    override suspend fun saveChanges(rules: List<Rule>) = withContext(ioDispatcher) {

        safeApiCall(ioDispatcher) {
            ruleRemoteDataSource.saveChanges(rules)
        }

        ruleLocalDataSource.storeRuleChanges(rules)
    }

    override suspend fun removeRules(rulesToDelete: Set<Rule>) = withContext(ioDispatcher) {
        ruleRemoteDataSource.removeRule(rulesToDelete.toList())
        ruleLocalDataSource.removeRules(rulesToDelete)
    }

}