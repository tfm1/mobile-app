package com.grobivo.university.dferreiropresedo.tfm.users

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.domain.UserSelected
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.DevicesRepository
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.UserRepository
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class UsersViewModel @Inject constructor(
    private val devicesRepository: DevicesRepository,
    private val usersRepository: UserRepository,
    private val sessionManagement: SessionManagement
) :
    ViewModel() {

    private val _deviceUsers = MutableLiveData<List<User>>()
    val deviceUsers: LiveData<List<User>>
        get() = _deviceUsers

    private val _flagShowNetworkError = MutableLiveData(false)
    val flagShowNetworkError: LiveData<Boolean>
        get() = _flagShowNetworkError

    private val _flagShowServerError = MutableLiveData(false)
    val flagShowServerError: LiveData<Boolean>
        get() = _flagShowServerError

    private val _flagOperationsCompleted = MutableLiveData(false)
    val flagOperationsCompleted: LiveData<Boolean>
        get() = _flagOperationsCompleted

    private val _usersToDisassociate = ArrayList<User>()
    private val _usersToUpdate = ArrayList<User>()

    fun retrieveDeviceUsers(deviceUUID: String) {
        viewModelScope.launch {
            manageRepositoryRemoteCall(_deviceUsers) {
                devicesRepository.getAllUsersFromDevice(deviceUUID)
            }
        }
    }

    fun addUserToDisassociate(user: User) {
        _usersToDisassociate.removeAll { userRemove -> userRemove.internalId == user.internalId }
        _usersToDisassociate.add(user)
        _deviceUsers.value?.let {
            val mutableList = it.toMutableList()
            mutableList.remove(user)
            _deviceUsers.postValue(mutableList)
        }
    }

    fun addUserToUpdate(user: User) {
        _usersToUpdate.removeAll { userRemove -> userRemove.internalId == user.internalId }
        _usersToUpdate.add(user)
    }

    fun completeOperations() {
        viewModelScope.launch {
            var activateFlagShowError = false
            if (_usersToDisassociate.isNotEmpty()) {
                when (
                    devicesRepository.removeUsersToSelectedDevice(
                        _usersToDisassociate, sessionManagement.getStoredDeviceUUID()!!
                    )
                ) {
                    is ResultWrapper.Success -> {

                    }
                    else -> {
                        activateFlagShowError = true
                    }
                }
            }
            if (_usersToUpdate.isNotEmpty()) {
                when (usersRepository.upgradeUsers(_usersToUpdate)) {
                    is ResultWrapper.Success -> {

                    }
                    else -> {
                        activateFlagShowError = true
                    }
                }
            }

            if (activateFlagShowError) {
                _flagShowServerError.postValue(true)
            } else {
                _flagOperationsCompleted.postValue(true)
            }
        }
    }

    fun retrievePossibleUsersToAddToDevice(): LiveData<List<User>> {
        val usersLiveData = MutableLiveData<List<User>>()
        viewModelScope.launch {
            manageRepositoryRemoteCall(usersLiveData) {
                devicesRepository.getAllPossibleUsers(
                    _deviceUsers.value!!
                )
            }
        }
        return usersLiveData
    }

    fun addUsersToDevice(usersSelected: List<UserSelected>) {
        viewModelScope.launch {
            val usersToAdd = usersSelected.filter { usersSelected -> usersSelected.selected }
                .map { userSelected -> userSelected.user }.toList()

            if (usersToAdd.isNotEmpty()) {
                val result = devicesRepository.addUsersToSelectedDevice(
                    usersToAdd, sessionManagement.getStoredDeviceUUID()!!
                )

                when (result) {
                    is ResultWrapper.GenericError, ResultWrapper.NetworkError -> {
                        _flagShowServerError.postValue(true)
                    }
                    else -> {
                        _deviceUsers.value?.let {
                            val previousUsers = it.toMutableList()
                            previousUsers.addAll(usersToAdd)
                            _deviceUsers.postValue(previousUsers.sortedBy { user -> user.name })
                        }
                    }
                }
            }
        }
    }

    fun flagShowNetworkErrorProcessed() = _flagShowNetworkError.postValue(false)
    fun flagShowServerErrorProcessed() = _flagShowServerError.postValue(false)
    fun flagCompletedOperationsProcessed() = _flagOperationsCompleted.postValue(false)

    suspend fun <LiveDataType> manageRepositoryRemoteCall(
        liveData: MutableLiveData<List<LiveDataType>>,
        call: suspend () -> ResultWrapper<List<LiveDataType>>
    ) {
        when (val invoke = call.invoke()) {
            is ResultWrapper.Success -> {
                liveData.postValue(invoke.value!!)
            }
            is ResultWrapper.NetworkError -> {
                _flagShowNetworkError.postValue(true)
                liveData.postValue(emptyList())
            }
            is ResultWrapper.GenericError -> {
                _flagShowServerError.postValue(true)
                liveData.postValue(emptyList())
            }
        }
    }
}
