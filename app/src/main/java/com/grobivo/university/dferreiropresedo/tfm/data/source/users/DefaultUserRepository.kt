package com.grobivo.university.dferreiropresedo.tfm.data.source.users

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper
import com.grobivo.university.dferreiropresedo.tfm.data.network.safeApiCall
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDatabaseSource
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.remote.UserRemoteSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class DefaultUserRepository @Inject internal constructor(
    private val localUserDataSource: UserDatabaseSource,
    private val remoteUserDataSource: UserRemoteSource,
    private val sessionManagement: SessionManagement
) : UserRepository {

    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO

    override suspend fun saveUser(userPhotoUrl: String, userIdToken: String): User? =
        withContext(ioDispatcher) {

            val registerUserWrapper = safeApiCall(ioDispatcher) {
                remoteUserDataSource.registerUser(userIdToken, userPhotoUrl)
            }
            when (registerUserWrapper) {
                is ResultWrapper.Success -> {
                    val userRegistered = registerUserWrapper.value
                    localUserDataSource.insert(userRegistered)
                    sessionManagement.loginUser(userRegistered, userIdToken)
                    return@withContext userRegistered
                }
                else -> {
                    Timber.w("It was not possible to login the user")
                    return@withContext null
                }
            }

        }

    override suspend fun upgradeUsers(users: List<User>) = safeApiCall(ioDispatcher) {
        remoteUserDataSource.upgradeUsers(users)
    }

}