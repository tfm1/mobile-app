package com.grobivo.university.dferreiropresedo.tfm.data.source.products.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityProduct
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityProductType
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.RegisterMapping

@Dao
interface ProductDao {

    @Insert
    fun syncInsert(entityProduct: EntityProduct): Long

    @Query("SELECT * FROM product WHERE product_id = :productId")
    suspend fun getById(productId: Long): EntityProduct

    @Query(
        """
        SELECT p.product_id as productId, p.name, p.ticket_code as ticketCode, 
            p.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id
    """
    )
    suspend fun getProductsToScan(): List<ProductMapping>

    @Query(
        """
        SELECT p.product_id as productId, p.name, p.ticket_code as ticketCode, 
            p.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id
        WHERE p.product_id NOT IN (:productsIds)
    """
    )
    suspend fun getProductsSkipping(productsIds: List<Long>): List<ProductMapping>

    @Query(
        """
        SELECT r.register_id as registerId, r.shared_register_id as sharedRegisterId, i.inventory_id as inventoryId, p.product_id as productId, p.name, p.ticket_code as ticketCode,
            pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure, COALESCE(r.quantity, 0) as quantity
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id
            LEFT JOIN register r ON r.product_id = p.product_id
            LEFT JOIN inventory i ON r.inventory_id = i.inventory_id AND i.device_UUID LIKE :deviceUUID
            LEFT JOIN device_users du ON du.device_UUID = i.device_UUID AND du.user_internal_id LIKE :userInternalId
        """
    )
    suspend fun getAllDeviceProductsInfo(
        userInternalId: String, deviceUUID: String
    ): List<RegisterMapping>

    @Query(
        """
        SELECT r.register_id as registerId, r.shared_register_id as sharedRegisterId, i.inventory_id as inventoryId, p.product_id as productId, p.name, p.ticket_code as ticketCode,
            pt.product_type_id as productTypeId, pt.name as productTypeName, pt.measure as productTypeMeasure, r.quantity as quantity
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id AND pt.product_type_id = :productTypeId
        JOIN register r ON r.product_id = p.product_id AND r.quantity > 0
        JOIN inventory i ON r.inventory_id = i.inventory_id AND i.device_UUID LIKE :deviceUUID
        JOIN device_users du ON i.device_UUID LIKE du.device_UUID AND du.user_internal_id LIKE :userInternalId
    """
    )
    suspend fun getStoredProductsByTypeId(
        userInternalId: String, deviceUUID: String, productTypeId: Long
    ): List<RegisterMapping>

    @Query(
        """
        SELECT pt.product_type_id, pt.name, pt.measure
        FROM product p JOIN product_type pt ON p.product_type_id = pt.product_type_id
        JOIN register r ON r.product_id = p.product_id AND r.quantity > 0
        JOIN inventory i ON r.inventory_id = i.inventory_id AND i.device_UUID LIKE :deviceUUID
        JOIN device_users du ON i.device_UUID LIKE du.device_UUID AND du.user_internal_id LIKE :userInternalId
        GROUP BY pt.product_type_id, pt.name, pt.measure
    """
    )
    fun getStoredProductsType(
        deviceUUID: String, userInternalId: String
    ): LiveData<List<EntityProductType>>

}