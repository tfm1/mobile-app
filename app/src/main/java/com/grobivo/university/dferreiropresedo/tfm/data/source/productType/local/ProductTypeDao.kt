package com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local

import androidx.room.Dao
import androidx.room.Query
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityProductType

@Dao
interface ProductTypeDao {

    @Query("INSERT INTO product_type(name,measure) VALUES (:name,:measure)")
    suspend fun insertEntity(name: String, measure: String)

    @Query("INSERT INTO product_type(name,measure) VALUES (:name,:measure)")
    fun insertEntitySync(name: String, measure: String)

    @Query("SELECT product_type_id, name, measure FROM product_type WHERE name LIKE :name")
    fun findByName(name: String): EntityProductType
}