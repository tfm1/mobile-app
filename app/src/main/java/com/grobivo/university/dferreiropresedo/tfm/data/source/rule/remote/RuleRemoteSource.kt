package com.grobivo.university.dferreiropresedo.tfm.data.source.rule.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule

interface RuleRemoteSource {

    suspend fun createRule(rule: List<Rule>): List<Rule>

    suspend fun removeRule(rule: List<Rule>)

    suspend fun retrieveInventoryRules(storedInventorySharedId: Long)

    suspend fun saveChanges(rules: List<Rule>)

}