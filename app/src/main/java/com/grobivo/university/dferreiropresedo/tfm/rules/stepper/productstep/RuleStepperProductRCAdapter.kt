package com.grobivo.university.dferreiropresedo.tfm.rules.stepper.productstep

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.recyclerview.selection.ItemDetailsLookup
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.ProducttypeHeaderItemlistViewBinding
import com.grobivo.university.dferreiropresedo.tfm.databinding.RuleStepperProductItemviewBinding
import com.grobivo.university.dferreiropresedo.tfm.products.RegistersDiffItemCallback
import kotlin.math.min

class RuleStepperProductAdapter :
    ListAdapter<Register, RecyclerView.ViewHolder>(RegistersDiffItemCallback()) {

    var selectionTracker: SelectionTracker<Register>? = null

    class RuleStepperProductHeaderViewHolder private constructor(private val binding: ProducttypeHeaderItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup): RuleStepperProductHeaderViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = ProducttypeHeaderItemlistViewBinding.inflate(from, view, false)
                return RuleStepperProductHeaderViewHolder(binding)
            }
        }

        fun bind(register: Register) {
            binding.productTypeHeader.text = register.product.productType.name
            binding.numberOfElements.text = register.quantity.toInt().toString()
            binding.root.isClickable = false
        }
    }

    class RuleStepperProductViewHolder private constructor(private val binding: RuleStepperProductItemviewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup): RuleStepperProductViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = RuleStepperProductItemviewBinding.inflate(from, view, false)
                return RuleStepperProductViewHolder(binding)
            }
        }

        fun bind(register: Register, selected: Boolean) {
            binding.register = register
            binding.ruleProductCard.isChecked = selected
            binding.executePendingBindings()
        }

        fun getItemDetails(): ItemDetailsLookup.ItemDetails<Register> =
            object : ItemDetailsLookup.ItemDetails<Register>() {
                override fun getPosition(): Int = bindingAdapterPosition

                override fun getSelectionKey(): Register? = binding.register

                override fun inSelectionHotspot(e: MotionEvent): Boolean = true
            }
    }


    fun getItemByPosition(position: Int): Register = getItem(position)

    fun getItemPosition(productId: Long) =
        currentList.indexOfFirst { it.product.productId == productId }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item.product.productId == -1L) {
            AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER
        } else {
            super.getItemViewType(position)
        }
    }

    override fun submitList(list: List<Register>?) {
        super.submitList(addHeadersToList(list))
    }

    private fun addHeadersToList(list: List<Register>?): List<Register> {
        val data = ArrayList<Register>()

        list?.let {
            val headersWithAmountOfProducts = checkHeadersAndAmount(it)
            var position = 0
            headersWithAmountOfProducts.forEach { header ->
                val quantity = header.quantity
                data.add(header)
                data.addAll(list.subList(position, min(position + quantity.toInt(), list.size)))
                position += quantity.toInt()
            }
        }

        return data
    }

    private fun checkHeadersAndAmount(listElements: List<Register>): List<Register> {

        val list = ArrayList<Register>()
        val listIterator = listElements.listIterator()
        var previous: Register
        var element = listIterator.next()

        list.add(createListHeader(element.product.productType))

        while (listIterator.hasNext()) {
            previous = element
            element = listIterator.next()

            if (element.product.productType.productTypeId == previous.product.productType.productTypeId) {
                list.last().quantity += 1
            } else {
                list.add(createListHeader(element.product.productType))
            }
        }

        return list
    }


    private fun createListHeader(productType: ProductType): Register {
        return Register(-1L, -1L, 1F, -1L, Product(-1L, "", "", productType))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER == viewType) {
            return RuleStepperProductHeaderViewHolder.from(parent)
        }
        return RuleStepperProductViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        when (holder) {
            is RuleStepperProductHeaderViewHolder -> holder.bind(getItem(position))
            is RuleStepperProductViewHolder -> {
                val product = getItem(position)
                selectionTracker?.let {
                    holder.bind(product, it.isSelected(product))
                }
            }
            else -> TODO("Something went wrong")
        }

    }
}