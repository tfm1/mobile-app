package com.grobivo.university.dferreiropresedo.tfm.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.*
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.productType.local.ProductTypeDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.products.local.ProductDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDao

@Database(
    entities = [EntityDevice::class, EntityUser::class, EntityInventory::class, EntityProduct::class, EntityProductType::class, EntityRegister::class, EntityRule::class,
        DeviceUsersRelation::class],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {

    abstract fun deviceDao(): DeviceDao

    abstract fun userDao(): UserDao

    abstract fun inventoryDao(): InventoryDao

    abstract fun productDao(): ProductDao

    abstract fun productTypeDao(): ProductTypeDao

    abstract fun registerDao(): RegisterDao

    abstract fun ruleDao(): RuleDao
}