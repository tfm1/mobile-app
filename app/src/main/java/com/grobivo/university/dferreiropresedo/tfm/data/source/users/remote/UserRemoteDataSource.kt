package com.grobivo.university.dferreiropresedo.tfm.data.source.users.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.domain.asUserDTO
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import javax.inject.Inject

class UserRemoteDataSource @Inject constructor(
    private val endpointService: EndpointService
) :
    UserRemoteSource {

    override suspend fun registerUser(idToken: String, userPhotoUrl: String?): User {

        val userParsed = endpointService.registerUser("Bearer $idToken")
        return userParsed.asDomainModel(userPhotoUrl)
    }

    override suspend fun upgradeUsers(users: List<User>) {
        endpointService.upgradeUsers(users.asUserDTO())
    }

}