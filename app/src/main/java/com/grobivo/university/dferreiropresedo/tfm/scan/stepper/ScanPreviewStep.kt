package com.grobivo.university.dferreiropresedo.tfm.scan.stepper

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentScanPreviewStepBinding
import com.grobivo.university.dferreiropresedo.tfm.scan.ScanContainerViewModel
import com.stepstone.stepper.Step
import com.stepstone.stepper.VerificationError
import timber.log.Timber
import javax.inject.Inject

class ScanPreviewStep @Inject constructor() : Fragment(), Step {

    private lateinit var viewDataBinding: FragmentScanPreviewStepBinding

    private val containerViewModel: ScanContainerViewModel by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("Initializing the preview step...")

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_scan_preview_step, container, false)

        return viewDataBinding.root
    }

    override fun verifyStep(): VerificationError? {
        return if (containerViewModel.photoResult == null) {
            VerificationError("A photo must have been taken")
        } else {
            null
        }
    }

    override fun onSelected() {
        containerViewModel.cleanData()
        Glide.with(viewDataBinding.scanPreviewCapture.context)
            .load(containerViewModel.photoResult!!)
            .apply(
                RequestOptions()
                    .placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image)
                    .override(
                        viewDataBinding.scanPreviewCapture.width,
                        viewDataBinding.scanPreviewCapture.height
                    )
            )
            .into(viewDataBinding.scanPreviewCapture)
    }

    override fun onError(error: VerificationError) {
        TODO("Not yet implemented")
    }
}