package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType

@Entity(
    tableName = "product_type"
)
class EntityProductType constructor(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "product_type_id") val productTypeId: Long,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "measure") val measure: String
)

fun EntityProductType.asDomainModel() = ProductType(productTypeId, name, measure)
fun List<EntityProductType>.asDomainModel(): List<ProductType> = map { it.asDomainModel() }