package com.grobivo.university.dferreiropresedo.tfm.data.source.users

import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.network.ResultWrapper

interface UserRepository {

    suspend fun saveUser(userPhotoUrl: String, userIdToken: String): User?

    suspend fun upgradeUsers(users: List<User>): ResultWrapper<Unit>

}