package com.grobivo.university.dferreiropresedo.tfm.data.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User

@Entity(tableName = "user")
data class EntityUser constructor(
    @PrimaryKey @ColumnInfo(name = "internal_id") var internalId: String,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "email") var email: String,
    @ColumnInfo(name = "admin") var isAdmin: Boolean
)

fun EntityUser.asDomainModel() = User(internalId, name, email, isAdmin, null)
fun List<EntityUser>.asDomainModel(): List<User> = map { it.asDomainModel() }