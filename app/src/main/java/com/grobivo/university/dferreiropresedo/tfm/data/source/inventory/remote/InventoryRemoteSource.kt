package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface InventoryRemoteSource {

    suspend fun updateInventoryInfo(newData: List<Register>, inventoryId: Long)

}