package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory

import com.grobivo.university.dferreiropresedo.tfm.data.domain.DeviceInventoryMapping
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface InventoryRepository {

    suspend fun updateInventoryInfo(newData: List<Register>)

    suspend fun addScannedRegisters(newData: List<Register>)

    suspend fun getDeviceInventoryInfo(deviceUUID: String): DeviceInventoryMapping

}