package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.squareup.moshi.Json

data class DTORegister(
    @Json(name = "client_register_id") val clientRegisterId: Long = -1L,
    @Json(name = "server_register_id") val serverRegisterId: Long? = -1L,
    @Json(name = "inventory_id") val inventoryId: Long,
    @Json(name = "quantity") val quantity: Float,
    @Json(name = "product") val product: DTOProduct
)

fun DTORegister.asDomainModel(): Register =
    Register(clientRegisterId, serverRegisterId, quantity, inventoryId, product.asDomainModel())

fun List<DTORegister>.asDomainModel(): List<Register> = map { it.asDomainModel() }
