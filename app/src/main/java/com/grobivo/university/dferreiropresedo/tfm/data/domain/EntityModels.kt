package com.grobivo.university.dferreiropresedo.tfm.data.domain

import android.bluetooth.BluetoothDevice
import android.os.Parcelable
import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.*
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.*
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Device(
    val UUID: String,
    val name: String
) : Parcelable

fun BluetoothDevice.asDomainDevice(): Device = Device(address, name)
fun Device.asDeviceDatabaseModel(): EntityDevice = EntityDevice(UUID, name)
fun List<Device>.asDeviceDatabaseModel(): List<EntityDevice> = map { it.asDeviceDatabaseModel() }
fun Device.asDeviceDTO(): DTODevice = DTODevice(UUID, name)
fun List<Device>.asDeviceDTO(): List<DTODevice> = map { it.asDeviceDTO() }


data class User(
    val internalId: String,
    val name: String,
    val email: String,
    var isAdmin: Boolean,
    val imageUrl: String?
)

fun User.asUserDatabaseModel(): EntityUser = EntityUser(internalId, name, email, isAdmin)
fun List<User>.asUserDatabaseModel(): List<EntityUser> = map { it.asUserDatabaseModel() }
fun User.asUserDTO(): DTOUser = DTOUser(internalId, name, email, isAdmin, null)
fun List<User>.asUserDTO(): List<DTOUser> = map { it.asUserDTO() }


data class Inventory(
    val deviceUUID: String,
    val sharedInventoryId: Long?,
    val inventoryId: Long
)

fun Inventory.asInventoryDatabaseModel(): EntityInventory =
    EntityInventory(inventoryId, sharedInventoryId, deviceUUID)

fun List<Inventory>.asInventoryDatabaseModel(): List<EntityInventory> =
    map { it.asInventoryDatabaseModel() }


@Parcelize
data class Register(
    val registerId: Long,
    val sharedRegisterId: Long?,
    var quantity: Float,
    val inventoryId: Long,
    val product: Product
) : Parcelable

fun Register.asRegisterDatabaseModel(): EntityRegister =
    EntityRegister(registerId, sharedRegisterId, quantity, inventoryId, product.productId)

fun List<Register>.asRegisterDatabaseModel(): List<EntityRegister> =
    map { it.asRegisterDatabaseModel() }

fun Register.asRegisterDTO(): DTORegister =
    DTORegister(registerId, sharedRegisterId, inventoryId, quantity, product.asDTO())

fun List<Register>.asRegisterDTO(): List<DTORegister> = map { it.asRegisterDTO() }

@Parcelize
data class Rule(
    val ruleId: Long,
    val sharedRuleId: Long?,
    var quantity: Float,
    val inventoryId: Long,
    val product: Product
) : Parcelable

fun Rule.asRuleDatabaseModel(): EntityRule =
    EntityRule(ruleId, sharedRuleId, quantity, inventoryId, product.productId)

fun List<Rule>.asRuleDatabaseModel(): List<EntityRule> = map { it.asRuleDatabaseModel() }

fun Rule.asRuleDTO(): DTORule =
    DTORule(ruleId, sharedRuleId, quantity, inventoryId, product.asDTO())

fun List<Rule>.asRuleDTO(): List<DTORule> = map { it.asRuleDTO() }


@Parcelize
data class Product(
    val productId: Long,
    val name: String,
    val ticketCode: String,
    val productType: ProductType
) : Parcelable

fun Product.asProductDatabaseModel(): EntityProduct =
    EntityProduct(productId, name, ticketCode, productType.productTypeId)

fun List<Product>.asProductDatabaseModel(): List<EntityProduct> =
    map { it.asProductDatabaseModel() }

fun Product.asDTO(): DTOProduct = DTOProduct(productId, name, productType.asDTO())


@Parcelize
data class ProductType(
    val productTypeId: Long,
    val name: String,
    val measure: String
) : Parcelable

fun ProductType.asProductTypeDatabaseModel(): EntityProductType =
    EntityProductType(productTypeId, name, measure)

fun List<ProductType>.asProductTypeDatabaseModel(): List<EntityProductType> =
    map { it.asProductTypeDatabaseModel() }

fun ProductType.asDTO(): DTOProductType = DTOProductType(productTypeId, name, measure)

