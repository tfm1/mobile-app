package com.grobivo.university.dferreiropresedo.tfm.data.source.products.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register

interface ProductRemoteSource {

    suspend fun getAllDeviceProducts(deviceUUID: String)

}