package com.grobivo.university.dferreiropresedo.tfm.data.source.register.local

import javax.inject.Inject

class RegisterLocalDataSource @Inject constructor(
    private val registerDao: RegisterDao
) : RegisterDatabaseSource {
}