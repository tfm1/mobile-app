package com.grobivo.university.dferreiropresedo.tfm.rules

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.view.ActionMode
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.selection.SelectionPredicates
import androidx.recyclerview.selection.SelectionTracker
import androidx.recyclerview.selection.StorageStrategy
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentRulesBinding
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class RulesFragment : Fragment(), ActionMode.Callback, RulesAdapter.RuleModifiedListener {

    @Inject
    lateinit var sessionManagement: SessionManagement

    private lateinit var viewDataBinding: FragmentRulesBinding
    private lateinit var rulesAdapter: RulesAdapter

    private val viewModel: RulesViewModel by viewModels()
    private var actionMode: ActionMode? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_rules, container, false)

        parentFragmentManager.popBackStack(R.id.devicesFragment, 0)

        configureRecyclerView()
        configureObservers()
        configureListeners()
        configureViewAccordingToPermissions()

        viewModel.retrieveAllInventoryRules()
        return viewDataBinding.root
    }

    private fun configureViewAccordingToPermissions() {

        if (sessionManagement.isAdminUser()) {
            viewDataBinding.confirmRulesChanges.visibility = View.VISIBLE
            viewDataBinding.createAdditionalRuleAdmin.visibility = View.VISIBLE

        } else {
            viewDataBinding.confirmRulesChanges.visibility = View.VISIBLE
            viewDataBinding.createAdditionalRuleAdmin.visibility = View.GONE
        }

    }

    private fun configureListeners() {
        viewDataBinding.confirmRulesChanges.setOnClickListener {
            viewModel.confirmChanges()
        }

        viewDataBinding.createAdditionalRuleAdmin.setOnClickListener {
            findNavController().navigate(RulesFragmentDirections.actionRulesFragmentToRuleStepperFragment())
        }
    }

    private fun configureObservers() {

        viewModel.flagChangesFinished.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(
                    RulesFragmentDirections.actionRulesFragmentToHomeFragment(
                        Device(
                            sessionManagement.getStoredDeviceUUID()!!,
                            sessionManagement.getStoredDeviceName()!!
                        )
                    )
                )
            }
            viewModel.flagChangesFinishedDeactivate()
        }

        viewModel.flagDisableButton.observe(viewLifecycleOwner) { amount ->
            viewDataBinding.createAdditionalRuleAdmin.isEnabled = amount > 0
        }

        viewModel.inventoryRules.observe(viewLifecycleOwner) { ruleList ->
            rulesAdapter.submitList(ruleList.sortedBy { rule -> rule.product.name })
        }
    }

    private fun configureRecyclerView() {
        rulesAdapter = RulesAdapter(childFragmentManager, viewLifecycleOwner, this, sessionManagement.isAdminUser())
        viewDataBinding.rulesList.adapter = rulesAdapter
        viewDataBinding.rulesList.layoutManager = GridLayoutManager(activity, 1)
        if (sessionManagement.isAdminUser()) {
            generateSelectionTracker()
        }
    }

    private fun generateSelectionTracker(): SelectionTracker<Rule> {

        val selectionTracker = SelectionTracker.Builder(
            "ruleItemTracker",
            viewDataBinding.rulesList,
            RuleItemKeyProvider(rulesAdapter),
            RuleItemLookup(viewDataBinding.rulesList),
            StorageStrategy.createParcelableStorage(Rule::class.java)
        )
            .withSelectionPredicate(SelectionPredicates.createSelectAnything())
            .build()

        selectionTracker.addObserver(object : SelectionTracker.SelectionObserver<Rule>() {

            override fun onSelectionChanged() {
                super.onSelectionChanged()
                selectionTracker.let {
                    val selectedItems = it.selection.toList()
                    viewModel.setRulesToDelete(selectedItems)

                    if (selectedItems.isEmpty()) {
                        actionMode?.finish()
                    } else {

                        if (actionMode == null) {
                            val requireActivity: AppCompatActivity =
                                requireActivity() as AppCompatActivity

                            actionMode =
                                requireActivity.startSupportActionMode(this@RulesFragment)
                        }
                        actionMode?.title = "${selectedItems.size}"
                    }
                }
            }
        })

        rulesAdapter.selectionTracker = selectionTracker

        return selectionTracker
    }


    override fun onCreateActionMode(mode: ActionMode?, menu: Menu?): Boolean {

        mode?.let {
            it.menuInflater.inflate(R.menu.action_mode_menu, menu)
            return true
        }

        return false
    }

    override fun onPrepareActionMode(mode: ActionMode?, menu: Menu?): Boolean {
        (requireActivity() as AppCompatActivity).supportActionBar?.hide()
        return true
    }

    override fun onDestroyActionMode(mode: ActionMode?) {
        rulesAdapter.selectionTracker?.clearSelection()
        actionMode = null
        (requireActivity() as AppCompatActivity).supportActionBar?.show()

        rulesAdapter.notifyDataSetChanged()
    }

    override fun onActionItemClicked(mode: ActionMode?, item: MenuItem?): Boolean {

        when (item?.itemId) {
            R.id.action_mode_delete_view -> {

                Timber.d("Action mode - Delete clicked")

                val confirmDialog = AlertDialog.Builder(requireContext())

                confirmDialog.apply {
                    setTitle(R.string.generic_text_confirmation_question)
                    setMessage(R.string.rules_dialog_confirm_delete_body)

                    setPositiveButton(R.string.generic_text_yes) { _, _ ->
                        Timber.d("Action mode - Removing selected devices")
                        val numberOfRulesRemoved = viewModel.removeRules()

                        Toast.makeText(
                            requireContext(),
                            resources.getQuantityString(
                                R.plurals.rules_confirm_delete_reply,
                                numberOfRulesRemoved, numberOfRulesRemoved
                            ),
                            Toast.LENGTH_SHORT
                        ).show()

                        actionMode?.finish()
                    }

                    setNegativeButton(R.string.generic_text_no) { _, _ ->
                        Timber.d("Action mode - Reverted delete action")
                        // We just dismiss the alert and do nothing
                    }

                    create()
                }.show()
            }
        }

        return true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    override fun ruleModified(rule: Rule) {
        viewModel.addRuleToUpdate(rule)
    }
}