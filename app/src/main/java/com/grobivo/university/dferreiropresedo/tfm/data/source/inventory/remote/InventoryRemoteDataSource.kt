package com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.remote

import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.domain.asRegisterDTO
import com.grobivo.university.dferreiropresedo.tfm.data.network.EndpointService
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTORegister
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryLocalDataSource
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import timber.log.Timber
import javax.inject.Inject

class InventoryRemoteDataSource @Inject constructor(
    private val endpointService: EndpointService,
    private val inventoryLocalDataSource: InventoryLocalDataSource,
    private val sessionManagement: SessionManagement
) : InventoryRemoteSource {

    override suspend fun updateInventoryInfo(newData: List<Register>, inventoryId: Long) {
        Timber.d("Updating the remote inventory")
        val registersToSend = newData.map {
            Register(
                it.registerId, it.sharedRegisterId, it.quantity, inventoryId, it.product
            )
        }
        val registersReceived = endpointService.updateRegistersInfo(registersToSend.asRegisterDTO())
        val registersToUpdate = registersReceived.asDomainModel()

        val registersDeleted = newData.filter { it.quantity == 0F }

        inventoryLocalDataSource.updateInventoryInfo(
            registersToUpdate.plus(registersDeleted),
            sessionManagement.getStoredInventoryInternalId()
        )
    }
}