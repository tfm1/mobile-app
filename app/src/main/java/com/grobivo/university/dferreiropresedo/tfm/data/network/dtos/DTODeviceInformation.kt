package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.InventoryInfo
import com.squareup.moshi.Json

data class DTODeviceInformation(
    @Json(name = "device") val deviceInfo: DTODevice,
    @Json(name = "inventory") val inventoryInfo: DTOInventory
)

fun DTODeviceInformation.asDomainModel(): InventoryInfo = InventoryInfo(
    deviceInfo.asDomainModel(),
    inventoryInfo.asDomainModel(),
    inventoryInfo.registers.asDomainModel(),
    inventoryInfo.rules.asDomainModel()
)
