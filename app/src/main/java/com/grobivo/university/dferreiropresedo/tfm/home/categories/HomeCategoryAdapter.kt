package com.grobivo.university.dferreiropresedo.tfm.home.categories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.databinding.HomeCategoryItemlistViewBinding

class HomeCategoryAdapter(private val clickListener: HomeCategoryListener) :
    ListAdapter<ProductType, HomeCategoryAdapter.ViewHolder>(HomeCategoryDiffItemCallback()) {

    class ViewHolder private constructor(private val binding: HomeCategoryItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup): ViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = HomeCategoryItemlistViewBinding.inflate(from, view, false)
                return ViewHolder(binding)
            }
        }

        fun bind(productType: ProductType, clickListener: HomeCategoryListener) {

            binding.productType = productType
            binding.productTypeListener = clickListener

            binding.executePendingBindings()
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(getItem(position), clickListener)
    }
}

class HomeCategoryDiffItemCallback : DiffUtil.ItemCallback<ProductType>() {

    override fun areItemsTheSame(oldItem: ProductType, newItem: ProductType): Boolean {
        return oldItem.productTypeId == newItem.productTypeId
    }

    override fun areContentsTheSame(oldItem: ProductType, newItem: ProductType): Boolean {
        return oldItem == newItem
    }

}

class HomeCategoryListener(val clickListener: (product: ProductType) -> Unit) {
    fun categoryClicked(product: ProductType) = clickListener(product)
}