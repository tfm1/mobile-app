package com.grobivo.university.dferreiropresedo.tfm.users

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.grobivo.university.dferreiropresedo.tfm.MainViewModel
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Device
import com.grobivo.university.dferreiropresedo.tfm.data.domain.User
import com.grobivo.university.dferreiropresedo.tfm.data.domain.UserSelected
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentUsersBinding
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import com.grobivo.university.dferreiropresedo.tfm.users.addusersdialog.AddUsersToDeviceDialog
import dagger.hilt.android.AndroidEntryPoint
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class UsersFragment : Fragment(), AddUsersToDeviceDialog.AddUsersToDeviceListener {

    private lateinit var viewDataBinding: FragmentUsersBinding
    private lateinit var recyclerViewAdapter: UsersAdapter
    private val viewModel: UsersViewModel by viewModels()
    private val activityViewModel: MainViewModel by activityViewModels()

    @Inject
    lateinit var sessionManagement: SessionManagement


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_users, container, false)

        viewDataBinding.userManagementHeader.text = resources.getString(
            R.string.user_management_header,
            sessionManagement.getStoredDeviceName()
        )
        insertRecyclerView()

        chargeData()
        configureListeners()
        configureObservables()

        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewDataBinding.lifecycleOwner = this.viewLifecycleOwner
    }

    private fun chargeData() {
        viewModel.retrieveDeviceUsers(sessionManagement.getStoredDeviceUUID()!!)
    }

    private fun configureListeners() {
        viewDataBinding.addUsers.setOnClickListener {
            val dialog =
                AddUsersToDeviceDialog(viewModel.retrievePossibleUsersToAddToDevice(), this)
            if (dialog.showsDialog) {
                dialog.show(childFragmentManager, AddUsersToDeviceDialog::class.toString())
            }
        }

        viewDataBinding.confirmUsersChanges.setOnClickListener {
            viewModel.completeOperations()
        }
    }

    private fun configureObservables() {

        viewModel.deviceUsers.observe(viewLifecycleOwner) {
            recyclerViewAdapter.submitList(it)
        }

        viewModel.flagShowNetworkError.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.user_management_network_error_toast),
                    Toast.LENGTH_LONG
                ).show()
            }
            viewModel.flagShowNetworkErrorProcessed()
        }

        viewModel.flagShowServerError.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(
                    requireContext(),
                    getString(R.string.user_management_server_error_toast),
                    Toast.LENGTH_LONG
                ).show()
            }
            viewModel.flagShowServerErrorProcessed()
        }

        viewModel.flagOperationsCompleted.observe(viewLifecycleOwner) {
            if (it) {
                findNavController().navigate(
                    UsersFragmentDirections.actionUsersFragmentToHomeFragment(
                        Device(
                            sessionManagement.getStoredDeviceUUID()!!,
                            sessionManagement.getStoredDeviceName()!!
                        )
                    )
                )
            }
            viewModel.flagCompletedOperationsProcessed()
        }

        activityViewModel.searchCriteria.observe(viewLifecycleOwner) { criteria ->
            val allUsersShown =
                recyclerViewAdapter.currentList.size == viewModel.deviceUsers.value?.size

            if (!criteria.isNullOrBlank() || !allUsersShown) {
                val listToShow = if (!criteria.isNullOrBlank()) {
                    viewModel.deviceUsers.value?.filter { userToFilter ->
                        userToFilter.name.contains(criteria, true)
                    }
                } else {
                    viewModel.deviceUsers.value
                }?.sortedBy { deviceUser -> deviceUser.name.capitalize(Locale.ROOT) }

                recyclerViewAdapter.submitList(listToShow)
                viewDataBinding.usersListManagement.smoothScrollToPosition(0)
            }
        }
    }

    private fun insertRecyclerView() {
        val userManagementListener = object : UsersDataManagementListener {
            override fun onDeleteUser(user: User) {
                viewModel.addUserToDisassociate(user)
            }

            override fun onModifyUser(user: User) {
                viewModel.addUserToUpdate(user)
            }
        }

        recyclerViewAdapter = UsersAdapter(userManagementListener)
        val gridLayoutManager = GridLayoutManager(activity, 1)
        viewDataBinding.usersListManagement.adapter = recyclerViewAdapter
        viewDataBinding.usersListManagement.layoutManager = gridLayoutManager
    }

    override fun onUsersSelectedToAdd(usersSelected: List<UserSelected>) {
        viewModel.addUsersToDevice(usersSelected)
    }


}