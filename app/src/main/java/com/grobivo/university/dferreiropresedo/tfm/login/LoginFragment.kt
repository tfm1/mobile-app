package com.grobivo.university.dferreiropresedo.tfm.login

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.grobivo.university.dferreiropresedo.tfm.R
import com.grobivo.university.dferreiropresedo.tfm.databinding.FragmentLoginBinding
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class LoginFragment : Fragment() {

    private lateinit var viewDataBinding: FragmentLoginBinding

    @Inject
    lateinit var sessionManagement: SessionManagement

    private val viewModel: LoginViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        if (sessionManagement.isUserLoggedIn()) {
            findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToDevicesFragment())
        }

        Timber.d("onCreateView initialized")
        viewDataBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_login, container, false
        )

        Timber.d("Configuring things...")
        viewModel.configureGoogleSignIn(this)

        configureListeners()
        configureObservers()

        return viewDataBinding.root
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        Timber.d("Intent received.")

        if (requestCode == viewModel.INTENT_CODE_SIGN_IN) {
            Timber.d("The received intent was a GSI intent")
            viewModel.handleSignInResult(data)
        }
    }


    private fun configureListeners() {
        viewDataBinding.signInButton.setOnClickListener {
            viewModel.googleSignInTrigger()
        }
    }

    private fun configureObservers() {

        viewModel.triggerErrorTryingToLogin.observe(viewLifecycleOwner) {
            if (it) {
                Toast.makeText(context, R.string.login_error_logging_in, Toast.LENGTH_LONG).show()
                viewModel.flagTriggerErrorTryingToLoginDeactivate()
            }
        }

        viewModel.triggerSignInActivity.observe(viewLifecycleOwner, {
            it?.apply {
                viewModel.googleSignInTriggered()
                fireSignInIntent(it)
                viewModel.flagTriggerNavigateAfterSignInDeactivate()
            }
        })

        viewModel.triggerNavigateAfterSignIn.observe(viewLifecycleOwner) {
            if (it) {
                this.findNavController()
                    .navigate(LoginFragmentDirections.actionLoginFragmentToDevicesFragment())
                viewModel.flagTriggerSignInActivityDeactivate()
            }
        }

        viewModel.triggerShowLoggingDialog.observe(viewLifecycleOwner) {
            if (it) {
                LoginStepsLoadingDialog(viewModel.loggingSteps).show(
                    childFragmentManager, LoginStepsLoadingDialog::class.toString()
                )
                viewModel.flagTriggerShowLoggingDialogDeactivate()
            }
        }
    }

    /**
     *  Launches the Sign-In Intent so as to try to connect with Google
     */
    private fun fireSignInIntent(signInIntent: Intent) {
        Timber.d("Firing the Sign-In intent")
        startActivityForResult(signInIntent, viewModel.INTENT_CODE_SIGN_IN)
    }
}