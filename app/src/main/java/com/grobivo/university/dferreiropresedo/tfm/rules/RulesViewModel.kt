package com.grobivo.university.dferreiropresedo.tfm.rules

import androidx.lifecycle.*
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.RuleRepository
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class RulesViewModel @Inject constructor(
    private val sessionManagement: SessionManagement,
    private val ruleRepository: RuleRepository
) : ViewModel() {

    val inventoryRules = MediatorLiveData<List<Rule>>()
    val flagDisableButton = MediatorLiveData<Int>()

    private val _flagChangesFinished = MutableLiveData(false)
    val flagChangesFinished: LiveData<Boolean>
        get() = _flagChangesFinished

    private val rulesToDelete = HashSet<Rule>()
    private val rulesToUpdate = HashSet<Rule>()

    fun flagChangesFinishedActivate() = _flagChangesFinished.postValue(true)
    fun flagChangesFinishedDeactivate() = _flagChangesFinished.postValue(false)

    fun retrieveAllInventoryRules() {
        Timber.d("Retrieving all inventory rules")
        viewModelScope.launch {
            flagDisableButton.addSource(
                ruleRepository.getAmountOfProductsWithoutRule(sessionManagement.getStoredInventoryInternalId())
            ) {
                flagDisableButton.postValue(it)
            }
            inventoryRules.addSource(ruleRepository.retrieveInventoryRules()) {
                inventoryRules.postValue(it)
            }
        }
    }

    fun setRulesToDelete(rules: List<Rule>) {
        rulesToDelete.clear()
        rulesToDelete.addAll(rules)
    }

    fun removeRules(): Int {
        Timber.d("Removing the selected rules")
        val numberOfRulesDeleted = rulesToDelete.size

        viewModelScope.launch {
            ruleRepository.removeRules(HashSet(rulesToDelete))
            rulesToDelete.clear()
        }

        return numberOfRulesDeleted
    }

    fun addRuleToUpdate(rule: Rule) {
        rulesToUpdate.find { it.ruleId == rule.ruleId }?.let {
            rulesToUpdate.remove(it)
        }
        rulesToUpdate.add(rule)
    }

    fun confirmChanges() {
        Timber.d("Confirming the changes applied to the rules")
        viewModelScope.launch {
            if (rulesToUpdate.isNotEmpty()) {
                ruleRepository.saveChanges(rulesToUpdate.toList())
                rulesToUpdate.clear()
            }
            flagChangesFinishedActivate()
        }
    }
}
