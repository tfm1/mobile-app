package com.grobivo.university.dferreiropresedo.tfm.data.source.rule

import androidx.lifecycle.LiveData
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Rule

interface RuleRepository {

    suspend fun getAmountOfProductsWithoutRule(inventoryId: Long): LiveData<Int>

    suspend fun getAllProductsWithoutRule(inventoryId: Long): List<Register>

    suspend fun createRule(inventoryId: Long, product: Product, quantity: Float)

    suspend fun retrieveInventoryRules(): LiveData<List<Rule>>

    suspend fun saveChanges(rules: List<Rule>)

    suspend fun removeRules(rulesToDelete: Set<Rule>)

}