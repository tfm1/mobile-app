package com.grobivo.university.dferreiropresedo.tfm.products

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.grobivo.university.dferreiropresedo.tfm.custom.DoublePickerDialog
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Product
import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.grobivo.university.dferreiropresedo.tfm.data.domain.Register
import com.grobivo.university.dferreiropresedo.tfm.databinding.ProductsItemlistViewBinding
import com.grobivo.university.dferreiropresedo.tfm.databinding.ProducttypeHeaderItemlistViewBinding
import kotlin.math.min


class ProductsAdapter(
    private val childFragmentManager: FragmentManager,
    private val viewLifecycleOwner: LifecycleOwner
) :
    ListAdapter<Register, RecyclerView.ViewHolder>(RegistersDiffItemCallback()) {

    class RegisterHeaderViewHolder private constructor(private val binding: ProducttypeHeaderItemlistViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup): RegisterHeaderViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = ProducttypeHeaderItemlistViewBinding.inflate(from, view, false)
                return RegisterHeaderViewHolder(binding)
            }
        }

        fun bind(product: Register) {
            binding.productTypeHeader.text = product.product.productType.name
            binding.numberOfElements.text = product.quantity.toInt().toString()
        }
    }

    class RegisterViewHolder private constructor(
        private val binding: ProductsItemlistViewBinding,
        private val childFragmentManager: FragmentManager
    ) :
        RecyclerView.ViewHolder(binding.root) {

        companion object {
            fun from(view: ViewGroup, childFragmentManager: FragmentManager): RegisterViewHolder {
                val from = LayoutInflater.from(view.context)
                val binding = ProductsItemlistViewBinding.inflate(from, view, false)
                return RegisterViewHolder(binding, childFragmentManager)
            }
        }

        fun bind(register: Register, viewLifecycleOwner: LifecycleOwner) {

            binding.registerInfo = register
            binding.listener = RegisterItemListener(binding)

            binding.productQuantity.setOnClickListener {
                val doublePicker =
                    DoublePickerDialog(register.quantity, register.product.productType.measure)
                doublePicker
                    .show(childFragmentManager, DoublePickerDialog::class.qualifiedName)
                doublePicker.valueSelected.observe(viewLifecycleOwner) {
                    binding.productQuantity.text = it.toString()
                    register.quantity = it
                }
            }

            binding.executePendingBindings()
        }
    }

    override fun getItemViewType(position: Int): Int {
        val item = getItem(position)
        return if (item.product.productId == -1L) {
            AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER
        } else {
            super.getItemViewType(position)
        }
    }

    override fun submitList(list: List<Register>?) {
        super.submitList(addHeadersToList(list))
    }

    private fun addHeadersToList(list: List<Register>?): List<Register> {
        val data = ArrayList<Register>()

        list?.let {
            val headersWithAmountOfProducts = checkHeadersAndAmount(it)
            var position = 0
            headersWithAmountOfProducts.forEach { header ->
                val quantity = header.quantity
                data.add(header)
                data.addAll(list.subList(position, min(position + quantity.toInt(), list.size)))
                position += quantity.toInt()
            }
        }

        return data
    }

    private fun checkHeadersAndAmount(listElements: List<Register>): List<Register> {

        val list = ArrayList<Register>()
        val listIterator = listElements.listIterator()
        var previous: Register
        var element = listIterator.next()

        list.add(createListHeader(element.product.productType))

        while (listIterator.hasNext()) {
            previous = element
            element = listIterator.next()

            if (element.product.productType.productTypeId == previous.product.productType.productTypeId) {
                list.last().quantity += 1
            } else {
                list.add(createListHeader(element.product.productType))
            }
        }

        return list
    }


    private fun createListHeader(productType: ProductType): Register {
        return Register(-1L, -1L, 1F, -1L, Product(-1L, "", "", productType))
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (AdapterView.ITEM_VIEW_TYPE_HEADER_OR_FOOTER == viewType) {
            return RegisterHeaderViewHolder.from(parent)
        }
        return RegisterViewHolder.from(parent, childFragmentManager)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is RegisterHeaderViewHolder -> holder.bind(getItem(position))
            is RegisterViewHolder -> holder.bind(getItem(position), viewLifecycleOwner)
            else -> TODO("Something went wrong")
        }
    }

}


class RegistersDiffItemCallback : DiffUtil.ItemCallback<Register>() {

    override fun areItemsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem.product.productId == newItem.product.productId
    }

    override fun areContentsTheSame(oldItem: Register, newItem: Register): Boolean {
        return oldItem == newItem
    }

}

class RegisterItemListener(private val binding: ProductsItemlistViewBinding) {

    fun onClickDecrease() {
        val value = binding.productQuantity.text.toString().replace(",", ".").toFloat()
        val formatter = if (value % 1.0 <= 0.001) {
            "%.0f"
        } else {
            "%.2f"
        }

        if (value >= 1) {
            val decreasedValue = formatter.format(value - 1).replace(",", ".")
            binding.productQuantity.text = decreasedValue
            binding.registerInfo?.quantity = decreasedValue.toFloat()
        } else {
            binding.productQuantity.text = 0F.toString()
            binding.registerInfo?.quantity = 0F
        }
    }

    fun onClickIncrease() {
        val value = binding.productQuantity.text.toString().replace(",", ".").toFloat()
        val formatter = if (value % 1.0 <= 0.001) {
            "%.0f"
        } else {
            "%.2f"
        }

        val increasedValue = formatter.format(value + 1).replace(",", ".")
        binding.productQuantity.text = increasedValue
        binding.registerInfo?.quantity = increasedValue.toFloat()
    }
}