package com.grobivo.university.dferreiropresedo.tfm.data.network

import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.*
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

interface EndpointService {

    @GET("user/register")
    @Headers("No-Authentication: true")
    suspend fun registerUser(@Header("Authorization") authorization: String): DTOUser

    @GET("user/users")
    suspend fun retrieveAllPossibleUsers(
        @Query(value = "users", encoded = true) usersAlreadyAdded: List<String>
    ): List<DTOUser>

    @POST("user/users")
    suspend fun upgradeUsers(@Body asUserDTO: List<DTOUser>)

    @POST("user/devices")
    suspend fun connectUserDevice(@Body selectedDevices: DTODevice): DTODeviceInformation

    @HTTP(method = "DELETE", path = "user/devices", hasBody = true)
    suspend fun disconnectUserDevice(@Body selectedDevices: List<DTODevice>)


    @GET("devices/user")
    suspend fun getAllDeviceUsers(
        @Query(value = "device", encoded = true) device: String
    ): List<DTOUser>

    @POST("devices/user")
    suspend fun addUsersToDevice(
        @Query(value = "device", encoded = true) device: String,
        @Body usersToAdd: List<String>
    )

    @HTTP(method = "DELETE", path = "devices/user", hasBody = true)
    suspend fun removeUsersFromDevice(
        @Query(value = "device", encoded = true) device: String,
        @Body usersToRemove: List<DTOUser>
    )

    @GET("inventory/register")
    suspend fun getAllDeviceProducts(
        @Query(value = "device", encoded = true) device: String
    ): List<DTORegister>

    @POST("inventory/register")
    suspend fun updateRegistersInfo(@Body register: List<DTORegister>): List<DTORegister>

    @GET("rules/rule")
    suspend fun retrieveInventoryRules(
        @Query(value = "inventory", encoded = true) inventoryId: Long
    ): List<DTORule>

    @POST("rules/rule")
    suspend fun createRules(@Body rules: DTODeviceRules): List<DTORule>

    @PUT("rules/rule")
    suspend fun saveRuleChanges(@Body rules: DTODeviceRules)

    @HTTP(method = "DELETE", path = "rules/rule", hasBody = true)
    suspend fun removeRules(@Body rules: List<Long>)

    @GET("sync/devices")
    suspend fun syncUserInformation(): List<DTOUserDeviceInformation>

    @GET("groceries/list")
    suspend fun requestGroceriesList(
        @Query(value = "deviceUUID", encoded = true) deviceUUID: String,
        @Query(value = "deviceName", encoded = true) deviceName: String
    ): List<DTORegister>

}


object AppService {
    const val REMOTE_SERVER_URL = "http://193.144.50.243:39393/"
    const val LOCAL_SERVER_URL = "http://192.168.0.18:43121/"
    const val SERVER_URL = REMOTE_SERVER_URL

    private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

    private lateinit var RETRO_INSTANCE: Retrofit
    private lateinit var SERVICE_INSTANCE: EndpointService

    fun getEndpointService(
        sessionManagement: SessionManagement
    ): EndpointService {

        if (!::SERVICE_INSTANCE.isInitialized) {
            SERVICE_INSTANCE =
                getRetrofit(sessionManagement).create(EndpointService::class.java)
        }

        return SERVICE_INSTANCE
    }

    private fun getRetrofit(sessionManagement: SessionManagement): Retrofit {

        if (!::RETRO_INSTANCE.isInitialized) {

            val httpLoggingInterceptor = HttpLoggingInterceptor()
            httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            val client = OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .callTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor {
                    var request = it.request()
                    if (request.header("No-Authentication") == null) {
                        request = request.newBuilder()
                            .addHeader(
                                "Authorization", "Bearer " + sessionManagement.userAccessToken()
                            )
                            .build()
                    }
                    it.proceed(request)
                }
                .addInterceptor(httpLoggingInterceptor)
                .build()

            RETRO_INSTANCE = Retrofit.Builder()
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .baseUrl(SERVER_URL)
                .client(client)
                .build()
        }

        return RETRO_INSTANCE
    }
}