package com.grobivo.university.dferreiropresedo.tfm.data.network.dtos

import com.grobivo.university.dferreiropresedo.tfm.data.domain.ProductType
import com.squareup.moshi.Json

data class DTOProductType(
    @Json(name = "product_type_id") val productTypeId: Long,
    @Json(name = "name") val name: String,
    @Json(name = "measure") val measure: String
)

fun DTOProductType.asDomainModel(): ProductType = ProductType(productTypeId, name, measure)
