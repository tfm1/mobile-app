package com.grobivo.university.dferreiropresedo.tfm.data.source.sync.local

import com.grobivo.university.dferreiropresedo.tfm.data.database.entities.EntityRegister
import com.grobivo.university.dferreiropresedo.tfm.data.domain.*
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.DTOUserDeviceInformation
import com.grobivo.university.dferreiropresedo.tfm.data.network.dtos.asDomainModel
import com.grobivo.university.dferreiropresedo.tfm.data.source.devices.local.DeviceDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.inventory.local.InventoryDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.register.local.RegisterDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.rule.local.RuleDao
import com.grobivo.university.dferreiropresedo.tfm.data.source.users.local.UserDao
import com.grobivo.university.dferreiropresedo.tfm.session.SessionManagement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

class SyncLocalDataSource @Inject constructor(
    private val userDao: UserDao,
    private val deviceDao: DeviceDao,
    private val inventoryDao: InventoryDao,
    private val ruleDao: RuleDao,
    private val registerDao: RegisterDao,
    private val sessionManagement: SessionManagement
) : SyncLocalSource {

    private val ioDispatcher = Dispatchers.IO

    override suspend fun syncUserInformation(userInformation: List<DTOUserDeviceInformation>) =
        withContext(ioDispatcher) {
            userInformation.map { userDeviceInformation ->
                val device = userDeviceInformation.deviceInformation.deviceInfo.asDomainModel()
                val inventory =
                    userDeviceInformation.deviceInformation.inventoryInfo.asDomainModel()
                val rules =
                    userDeviceInformation.deviceInformation.inventoryInfo.rules.asDomainModel()
                val registers =
                    userDeviceInformation.deviceInformation.inventoryInfo.registers.asDomainModel()

                syncUserPrivileges(userDeviceInformation.isAdmin)
                val deviceSynced = syncDevice(device)
                val inventorySynced = syncInventory(deviceSynced, inventory)
                syncRules(inventorySynced, rules)
                syncRegisters(inventorySynced, registers)
            }
            return@withContext
        }

    private suspend fun syncUserPrivileges(admin: Boolean) = withContext(ioDispatcher) {
        Timber.d("Updating the user information")
        val userFound = userDao.findUserInfo(sessionManagement.userInternalId())
        userFound?.let {
            sessionManagement.updateUserAdmin(admin)
            userFound.isAdmin = admin
            userDao.update(userFound)
        }
    }

    private suspend fun syncDevice(remoteDevice: Device): Device =
        withContext(ioDispatcher) {
            Timber.d("Syncing the device information")
            val deviceFound = deviceDao.getDeviceByUUID(remoteDevice.UUID)
            if (deviceFound == null) {
                deviceDao.connectDeviceToUser(
                    remoteDevice.asDeviceDatabaseModel(),
                    sessionManagement.userInternalId()
                )
            }
            return@withContext remoteDevice
        }

    private suspend fun syncInventory(device: Device, remoteInventory: Inventory): Inventory =
        withContext(ioDispatcher) {
            Timber.d("Syncing the inventory information")
            val inventoryFound = inventoryDao.getDeviceInventoryInfo(
                sessionManagement.userInternalId(), device.UUID
            )

            return@withContext if (inventoryFound == null) {
                val inventoryIdInserted =
                    inventoryDao.insertInventory(remoteInventory.asInventoryDatabaseModel())
                Inventory(
                    remoteInventory.deviceUUID,
                    remoteInventory.sharedInventoryId,
                    inventoryIdInserted
                )
            } else {
                inventoryFound.asInventoryInfo()
            }
        }

    private suspend fun syncRules(inventory: Inventory, remoteRules: List<Rule>) =
        withContext(ioDispatcher) {
            Timber.d("Syncing the rules information")

            val rulesWithLocalInventoryId = remoteRules.map {
                Rule(
                    it.ruleId, it.sharedRuleId, it.quantity, inventory.inventoryId, it.product
                )
            }
            val rulesAlreadyCreated = ruleDao.retrieveInventoryRulesSync(inventory.inventoryId)

            val rulesToCreate = ArrayList<Rule>()
            val rulesToUpdate = ArrayList<Rule>()

            if (rulesAlreadyCreated.isNullOrEmpty()) {
                // if there are no rules on the database, everything should be inserted
                rulesToCreate.addAll(rulesWithLocalInventoryId)
            } else {
                rulesWithLocalInventoryId.map { ruleToCheck ->
                    val ruleFound =
                        rulesAlreadyCreated.find { it.productId == ruleToCheck.product.productId }
                    if (ruleFound == null) {
                        // if we don't found a rule among the already created, we mark it as to create
                        rulesToCreate.add(ruleToCheck)
                    } else {
                        rulesToUpdate.add(
                            Rule(
                                ruleFound.ruleId, ruleToCheck.sharedRuleId, ruleToCheck.quantity,
                                inventory.inventoryId, ruleToCheck.product
                            )
                        )
                    }
                }
            }

            ruleDao.updateRules(rulesToUpdate.map { it.asRuleDatabaseModel() })
            ruleDao.createRules(rulesToCreate.map { it.asRuleDatabaseModel() })
        }

    private suspend fun syncRegisters(inventory: Inventory, remoteRegisters: List<Register>) =
        withContext(ioDispatcher) {
            Timber.d("Syncing the registers information")
            val remoteRegistersWithLocalInventory = remoteRegisters.map {
                Register(
                    it.registerId, it.sharedRegisterId, it.quantity,
                    inventory.inventoryId, it.product
                )
            }
            val registersAlreadyCreated =
                registerDao.getAllInventoryRegisters(inventory.inventoryId)

            val registersToCreate = ArrayList<EntityRegister>()
            val registersToUpdate = ArrayList<EntityRegister>()

            remoteRegistersWithLocalInventory.map { registerToCheck ->
                val registerFound =
                    registersAlreadyCreated.find { it.productId == registerToCheck.product.productId }

                if (registerFound == null) {
                    registersToCreate.add(registerToCheck.asRegisterDatabaseModel())
                } else {
                    registersToUpdate.add(
                        EntityRegister(
                            registerFound.registerId, registerToCheck.sharedRegisterId,
                            registerToCheck.quantity, inventory.inventoryId,
                            registerToCheck.product.productId
                        )
                    )
                }
            }

            registerDao.updateUserInventoryStock(
                registersToCreate, emptyList(), registersToUpdate
            )
        }
}