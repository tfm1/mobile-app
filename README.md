# Grobivo - Aplicación móvil

Este proyecto forma parte del Trabajo de Fin de Máster de Daniel Ferreiro Presedo.

## Qué es

Grobivo trata de una aplicación móvil que permite conectarse a un dispositivo Google Home y asociarle un inventario al cual podremos asociar una serie de productos. A mayores, podremos crearles reglas a estos productos para que cuando solicitemos la lista de la compra nos comunique cuáles son los productos que nos faltan en casa. Una vez hayamos hecho la compra, podremos actualizar nuestro inventario a través del escaneo del ticket.

Gracias al servidor de Grobivo, también podremos sincronizarnos con el resto de los usuarios asociados al dispositivo, e incluso gestionarlos siempre que dispongamos de los permisos necesarios.

## Funcionalidades

- Autenticación a través de Google.
- Gestionar los dispositivos y sus correspondientes inventarios.
- Posibilidad de crear reglas sobre los productos del inventario.
- Solicitar la lista de la compra.
- Gestionar los usuarios asociados al dispositivo.
- Escanear los tickets de la compra.

## Instalación

Se recomienda el uso de Android Studio para la instalación de Grobivo.

Una vez importado el proyecto, seleccionamos el dispositivo físico o emulado en el que queremos ejecutar el proyecto, y lo ejecutamos.

## Licencia

MIT
